package com.hyperstudio.hyper.file.complete.model

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.hyperstudio.hyper.file.main.model.TodoItemBean

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/10/21 18:29
 */
class FunctionSuccessBean(override var itemType: Int) : MultiItemEntity {
    companion object {
        const val head = 1
        const val dangerApp = 2
        const val ad = 3
        const val foot = 4
    }

    var functionTypeText = ""
    var functionSuccessItem: TodoItemBean? = null
    var functionDangerAppItem: DangerAppItem? = null
}
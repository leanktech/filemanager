package com.hyperstudio.hyper.file.clean.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.showInterstitialAd
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.base_lib.tool.rotateAnim
import com.hyperstudio.hyper.file.base_lib.tool.setVisibilityAnimation
import com.hyperstudio.hyper.file.clean.viewmodel.FirstStartAppCleanViewModel
import com.hyperstudio.hyper.file.databinding.FirstStartAppCleanLayoutBinding
import com.hyperstudio.hyper.file.splash.activity.MemoryUsageActivity
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/5/9 16:11
 */
class FirstStartAppCleanActivity : BaseActivity<FirstStartAppCleanLayoutBinding>() {

    private val firstCleanViewModel by viewModel<FirstStartAppCleanViewModel>()

    override val showToolBar: Boolean
        get() = false

    override fun getLayoutId() = R.layout.first_start_app_clean_layout

    override fun initView() {
        bindingView.vm = firstCleanViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        //设置顶部导航栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.color_152BA1).init()
        firstCleanViewModel.firstCleanLoadingIcon.value = R.mipmap.first_clean_cpu
        bindingView.ivRoundBg.rotateAnim()
    }

    override fun loadData(isRefresh: Boolean) {
        firstCleanViewModel.launchUI {

//            AdUtils.loadInterstitialAd(
//                this@FirstStartAppCleanActivity,
//                GlobalInfo.FIRST_START_CLEAN_COLD_BOOT_ID_h
//            )

            startFun {
                firstCleanViewModel.launchUI {
//                    bindingView.clCleaningUp.setVisibilityAnimation(View.GONE, 1000)
//                    delay(1000)
//                    bindingView.clCleaningSuccess.visibility = View.VISIBLE
//                    bindingView.dhvCleanSuccess.startAnim(500)
//                    delay(500)
//                    bindingView.dhvCleanSuccess.stopAnim()
//                    bindingView.clCleaningSuccess.visibility = View.VISIBLE
//                    tickAnim()
//                    loadDownToUpAnimFun()

                    startActivity(
                        Intent(
                            this@FirstStartAppCleanActivity,
                            MemoryUsageActivity::class.java
                        )
                    )
                    finish()
                }
            }
        }
    }

    private fun startFun(callBack: () -> Unit) {
        firstCleanViewModel.launchUI {
            delay(1500)
            firstCleanViewModel.firstCleanLoadingIcon.value = R.mipmap.first_clean_file
            delay(1500)
            firstCleanViewModel.firstCleanLoadingIcon.value = R.mipmap.first_clean_battery

            //clean another app
            var scanAppSuccess = false
            firstCleanViewModel.cleanAnotherApp {
                scanAppSuccess = true
            }
            firstCleanViewModel.asyncSimpleAsync {
                var loading = true

                var maxTime = 0

                var loadAdB = false

                var loadAdBTime = 0

                while (loading) {
                    val loadingSuccess = scanAppSuccess
//                        AdUtils.getAdObjectSuccess() &&
                    if (loadingSuccess || maxTime > 6000) {
                        loading = false
                        callBack()
                    } else {
//                        if (AdUtils.interstitialErrorMessage != null && AdUtils.interstitialErrorMessage?.isNotEmpty() == true) {
//                            if (!loadAdB) {
//                                AdUtils.loadInterstitialAd(
//                                    this@FirstStartAppCleanActivity,
//                                    GlobalInfo.FIRST_START_CLEAN_COLD_BOOT_ID_b
//                                )
//
//                                loadAdBTime = maxTime
//                                loadAdB = true
//                            } else {
//                                if (maxTime > 30000 || (maxTime - loadAdBTime > 6000 && AdUtils.interstitialErrorMessage != null)) {
//                                    if (scanAppSuccess) {
//                                        loading = false
//                                        callBack()
//                                    }
//                                }
//                            }
//                        }
                        delay(10)
                        maxTime += 10
                    }
                }
            }.run { }

        }
    }

    /**
     * 执行对勾动画（放大、翻转）
     * */
    private fun tickAnim() {
        val rotationYAnimator =
            ObjectAnimator.ofFloat(bindingView.ivCleanSuccess, "rotationY", 0f, 360f)
        val scaleXanim = ObjectAnimator.ofFloat(bindingView.ivCleanSuccess, "scaleX", 0f, 1.1f)
        val scaleYanim = ObjectAnimator.ofFloat(bindingView.ivCleanSuccess, "scaleY", 0f, 1.1f)
        val animatorSet = AnimatorSet()
        animatorSet.addListener(object : AnimatorListenerAdapter() {
            //便利类，只要实现需要的方法
            override fun onAnimationEnd(animation: Animator) {
                //在动画结束后，执行对勾动画（缩小到原始大小）
                tickScaleAnim()
            }
        })
        animatorSet.duration = 800
        animatorSet.playTogether(scaleXanim, scaleYanim, rotationYAnimator)
        animatorSet.start()
    }

    /**
     * 执行对勾动画（缩小到原始大小）
     * */
    private fun tickScaleAnim() {
        val scaleXanim = ObjectAnimator.ofFloat(bindingView.ivCleanSuccess, "scaleX", 1.1f, 1f)
        val scaleYanim = ObjectAnimator.ofFloat(bindingView.ivCleanSuccess, "scaleY", 1.1f, 1f)
        val animatorSet = AnimatorSet()
        animatorSet.addListener(object : AnimatorListenerAdapter() {
            //便利类，只要实现需要的方法
            override fun onAnimationEnd(animation: Animator) {
                //在动画结束后
                loadDownToUpAnimFun()
            }
        })
        animatorSet.duration = 600
        animatorSet.playTogether(scaleXanim, scaleYanim)
        animatorSet.start()
    }

    fun loadDownToUpAnimFun() {
        firstCleanViewModel.launchUI {
            //由下向上的渐入动画
//            val moveAnim: Animation = AnimationUtils.loadAnimation(
//                this@FirstStartAppCleanActivity,
//                R.anim.move_anim_down_to_up
//            ).apply {
//                duration = 650
//            }
//            //展示按钮
//            bindingView.tvFirstCleanContinue.visibility = View.VISIBLE
//            bindingView.tvFirstCleanContinue.startAnimation(moveAnim)
//            delay(650)
            val moveAnim1: Animation = AnimationUtils.loadAnimation(
                this@FirstStartAppCleanActivity,
                R.anim.move_anim_down_to_up
            ).apply {
                duration = 650
            }
            bindingView.tvCleanInfo.visibility = View.VISIBLE
            bindingView.tvCleanInfo.startAnimation(moveAnim1)
            delay(650)

//            showInterstitialAd(this@FirstStartAppCleanActivity) {
                firstCleanViewModel.launchUI {
//                    bindingView.ivCleanSuccess.setVisibilityAnimation(
//                        View.INVISIBLE,
//                        1500
//                    )
//                    delay(1500)
//                    bindingView.ivCleanSuccess.setVisibilityAnimation(
//                        View.VISIBLE,
//                        1500
//                    )
//                    delay(1500)
                    startActivity(
                        Intent(
                            this@FirstStartAppCleanActivity,
                            MemoryUsageActivity::class.java
                        )
                    )
                    finish()
                }
//            }
        }
    }


    override fun onClick(v: View?) {
        super.onClick(v)
        v?.let {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.tv_first_clean_continue -> {
                        showInterstitialAd(this@FirstStartAppCleanActivity) {
                            firstCleanViewModel.launchUI {
                                bindingView.tvFirstCleanContinue.setVisibilityAnimation(
                                    View.GONE,
                                    1000
                                )
                                bindingView.ivCleanSuccess.setVisibilityAnimation(
                                    View.INVISIBLE,
                                    1500
                                )
                                delay(1500)
                                bindingView.ivCleanSuccess.setVisibilityAnimation(
                                    View.VISIBLE,
                                    1500
                                )
                                delay(1500)
                                startActivity(
                                    Intent(
                                        this@FirstStartAppCleanActivity,
                                        MemoryUsageActivity::class.java
                                    )
                                )
                                finish()
                            }
                        }
                    }
                }
            }

        }
    }

}
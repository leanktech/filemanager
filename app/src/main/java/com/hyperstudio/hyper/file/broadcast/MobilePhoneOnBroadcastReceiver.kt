package com.hyperstudio.hyper.file.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.push.notification.initSystemResidentNotification
import com.hyperstudio.hyper.file.unit.startAliveService

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/1/20 18:09
 */
class MobilePhoneOnBroadcastReceiver: BroadcastReceiver() {

    private val actionBoot = "android.intent.action.BOOT_COMPLETED"

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals(actionBoot)) {

            initSystemResidentNotification()

            //启动亮屏监控服务和广播
            startAliveService(ActivityMgr.getContext())
        }
    }
}
package com.hyperstudio.hyper.file.push.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.push.notification.initSecurityPushNotification

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/12/3 20:13
 */
class SecurityDailyPushWorker(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
        if(!AppBean.firstStartApp) {
            initSecurityPushNotification()
        }
        return Result.success()
    }

}
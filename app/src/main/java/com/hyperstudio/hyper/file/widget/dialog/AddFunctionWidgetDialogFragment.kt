package com.hyperstudio.hyper.file.widget.dialog

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.Build
import android.view.View
import android.view.ViewGroup
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.fragment.BaseDialogFragment
import com.hyperstudio.hyper.file.databinding.AddWidgetFunctionDialogBinding
import com.hyperstudio.hyper.file.widget.FunctionAppWidget

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/4/26 17:47
 */
class AddFunctionWidgetDialogFragment: BaseDialogFragment<AddWidgetFunctionDialogBinding>() {

    override var dialogHeight: Int = ViewGroup.LayoutParams.MATCH_PARENT

    override fun getLayoutId() = R.layout.add_widget_function_dialog

    override fun initView() {
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.tv_widget_dialog_cancel -> {
                dismiss()
            }
            R.id.tv_widget_dialog_add -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    val appWidgetManager = context?.getSystemService(AppWidgetManager::class.java)
                    val myProvider = context?.let { ComponentName(it, FunctionAppWidget::class.java) }
                    val pinnedWidgetCallbackIntent =
                        Intent(context, FunctionAppWidget::class.java)
                    val successCallback = PendingIntent.getBroadcast(
                        context,
                        105,
                        pinnedWidgetCallbackIntent,
                        PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                    )
                    //如果不需要创建成功回调，requestPinAppWidget的第三个参数可以为null
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        myProvider?.let { appWidgetManager?.requestPinAppWidget(it, null, successCallback) }
                    }
                }
                dismiss()
            }
        }
    }

}
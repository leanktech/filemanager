package com.hyperstudio.hyper.file.push.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.push.notification.initTargetedPushNotification
import com.hyperstudio.hyper.file.unit.getPhoneCountry
import com.hyperstudio.hyper.file.unit.getTargetedApp

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/3/29 17:47
 */
class TargetedDailyPushWorker(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
        if (!AppBean.firstStartApp) {
            getTargetedApp()
            if (AppBean.tiktokAlive || AppBean.whatsAppAlive || AppBean.googlePlayAlive) {
                pushNotification()
            }
        }

        return Result.success()
    }

    private var count = 0//当前递归次数
    private fun pushNotification() {
        if (!pushTargetedNotification(AppBean.targetedPushType)) {
            count++
            if (count < 5) {
                pushNotification()
            } else {//递归五次后跳出循环
                count = 0
            }
        }
    }

    private fun pushTargetedNotification(index: Int): Boolean {
        var pushSuccess = false
        when (index) {
            0 -> {
                if (AppBean.tiktokAlive) {
                    initTargetedPushNotification(
                        ActivityMgr.getContext()
                            .getString(R.string.tiktok_notification_cleaning_up_text),
                        R.mipmap.tiktok_clean_up,
                        GlobalInfo.targetedTiktokFunction
                    )
                    pushSuccess = true
                }
                AppBean.targetedPushType++
            }
            1 -> {
                if (getPhoneCountry(context, "JP", true)) {
                    if (AppBean.whatsAppAlive) {
                        initTargetedPushNotification(
                            ActivityMgr.getContext()
                                .getString(R.string.whats_app_notification_cleaning_up_text),
                            R.mipmap.whatsapp_clean_up,
                            GlobalInfo.targetedWhatsAppFunction
                        )
                        pushSuccess = true
                    }
                }
                AppBean.targetedPushType++
            }
            2 -> {
                if (AppBean.googlePlayAlive) {
                    initTargetedPushNotification(
                        ActivityMgr.getContext()
                            .getString(R.string.google_play_notification_cleaning_up_text),
                        R.mipmap.google_play_clean_up,
                        GlobalInfo.targetedGooglePlayFunction
                    )
                    pushSuccess = true
                }
                AppBean.targetedPushType++
            }
            3 -> {
                if (AppBean.youTubeAlive) {
                    initTargetedPushNotification(
                        ActivityMgr.getContext()
                            .getString(R.string.youtube_notification_cleaning_up_text),
                        R.mipmap.youtube_clean_up,
                        GlobalInfo.targetedYouTubeFunction
                    )
                    pushSuccess = true
                }
                AppBean.targetedPushType++
            }
            4 -> {
                if (getPhoneCountry(context, "JP") || getPhoneCountry(
                        context,
                        "ID"
                    ) || getPhoneCountry(context, "MY") || getPhoneCountry(
                        context,
                        "PH"
                    ) || getPhoneCountry(context, "SG") || getPhoneCountry(context, "TH")
                ) {
                    if (AppBean.lineAlive) {
                        initTargetedPushNotification(
                            ActivityMgr.getContext()
                                .getString(R.string.line_notification_cleaning_up_text),
                            R.mipmap.line_clean_up,
                            GlobalInfo.targetedLineFunction
                        )
                        pushSuccess = true
                    }
                }
                AppBean.targetedPushType = 0
            }
        }
        return pushSuccess
    }

}
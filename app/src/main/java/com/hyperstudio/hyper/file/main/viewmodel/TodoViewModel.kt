package com.hyperstudio.hyper.file.main.viewmodel

import androidx.databinding.ObservableArrayList
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.main.model.TodoItemBean
import com.hyperstudio.hyper.file.main.model.TodoListBean
import com.hyperstudio.hyper.file.unit.getExternalUsedPercentage


/**
 * @Name: CleanViewModel
 * @Description: Clean Vm
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */

class TodoViewModel : BaseViewMode() {
    var todoList = ObservableArrayList<TodoListBean>()

    /**
     * 加载todo list数据
     */
    fun getTodoListData() {
        todoList.add(TodoListBean(TodoListBean.head).apply {
            todoHeadText = ActivityMgr.getContext().getString(R.string.clean_now_text)
            memoryUsageNum = AppBean.memoryNumber
            memoryNum = getExternalUsedPercentage() + "%"
        })
        todoList.add(TodoListBean(TodoListBean.foot).apply {
            todoFootItem = TodoItemBean().apply {
                itemName = GlobalInfo.cleanFunction
                itemImage = R.mipmap.icon_clean
                itemIntro = ActivityMgr.getContext().getString(R.string.clean_item_intro)
                itemButtonText = ActivityMgr.getContext().getString(R.string.clean_item_button_text)
            }
        })
        todoList.add(TodoListBean(TodoListBean.foot).apply {
            todoFootItem = TodoItemBean().apply {
                itemName = GlobalInfo.securityFunction
                itemImage = R.mipmap.icon_safe
                itemIntro = ActivityMgr.getContext().getString(R.string.security_item_intro)
                itemButtonText = ActivityMgr.getContext().getString(R.string.security_item_button_text)
            }
        })
//        todoList.add(TodoListBean(TodoListBean.foot).apply {
//            todoFootItem = TodoItemBean().apply {
//                itemName = GlobalInfo.boostFunction
//                itemImage = R.mipmap.icon_boost
//                itemIntro = ActivityMgr.getContext().getString(R.string.boost_item_intro)
//                itemButtonText = ActivityMgr.getContext().getString(R.string.boost_item_button_text)
//            }
//        })
//        todoList.add(TodoListBean(TodoListBean.foot).apply {
//            todoFootItem = TodoItemBean().apply {
//                itemName = GlobalInfo.saverFunction
//                itemImage = R.mipmap.icon_battery
//                itemIntro = ActivityMgr.getContext().getString(R.string.saver_item_intro)
//                itemButtonText = ActivityMgr.getContext().getString(R.string.saver_item_button_text)
//            }
//        })
//        todoList.add(TodoListBean(TodoListBean.foot).apply {
//            todoFootItem = TodoItemBean().apply {
//                itemName = GlobalInfo.coolerFunction
//                itemImage = R.mipmap.icon_cpu
//                itemIntro = ActivityMgr.getContext().getString(R.string.cooler_item_intro)
//                itemButtonText = ActivityMgr.getContext().getString(R.string.cooler_item_button_text)
//            }
//        })
        todoList.add((TodoListBean(TodoListBean.banner).apply {  }))
    }

}
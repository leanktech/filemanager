package com.hyperstudio.hyper.file.push.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.push.notification.initCleanPushNotification
import com.hyperstudio.hyper.file.unit.AnalyticsUtils

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/12/3 19:31
 */
class CleanDailyPushWorker(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
        if (!AppBean.firstStartApp) {
            initCleanPushNotification(
                ActivityMgr.getContext().getString(R.string.notification_clean),
                embeddingPoint = AnalyticsUtils.CARD_CLEAN_JUNK
            )
        }
        return Result.success()
    }

}
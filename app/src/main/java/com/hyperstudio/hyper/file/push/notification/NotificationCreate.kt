package com.hyperstudio.hyper.file.push.notification

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.view.View
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.splash.activity.SplashActivity
import com.hyperstudio.hyper.file.unit.AnalyticsUtils

/**
 * 需要设置通知的FullScreenIntent实现横幅通知悬停
 * 必须设置空的Intent 否则会有高频率推送自动跳转的问题
 */
//val fullScreenIntent: PendingIntent = PendingIntent.getActivity(
//    ActivityMgr.getContext(),
//    -1,
//    Intent(),
//    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
//)
private fun fullScreenIntent(): PendingIntent {
    val fullScreenIntent = PendingIntent.getActivities(
        ActivityMgr.getContext(),
        -2,
        arrayOf(Intent().apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }),
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )
    return fullScreenIntent
}

/**
 * 创建系统常驻通知
 */
fun initSystemResidentNotification() {

    val notificationContentView =
        RemoteViews(ActivityMgr.getContext().packageName, R.layout.system_resident_notification_bar)

    AppBean.memoryNumber.toInt().let { percent ->

        when {
            percent <= 7 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_01
                )
            }
            percent <= 12 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_02
                )
            }
            percent <= 17 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_03
                )
            }
            percent <= 22 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_04
                )
            }
            percent <= 27 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_05
                )
            }
            percent <= 32 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_06
                )
            }
            percent <= 37 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_07
                )
            }
            percent <= 42 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_08
                )
            }
            percent <= 47 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_09
                )
            }
            percent <= 52 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_10
                )
            }
            percent <= 57 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_11
                )
            }
            percent <= 62 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_12
                )
            }
            percent <= 67 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_13
                )
            }
            percent <= 72 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_14
                )
            }
            percent <= 77 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_15
                )
            }
            percent <= 82 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_16
                )
            }
            percent <= 87 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_17
                )
            }
            percent <= 92 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_18
                )
            }
            percent <= 100 -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_19
                )
            }
            else -> {
                notificationContentView.setImageViewResource(
                    R.id.iv_memory_bg,
                    R.mipmap.notification_memory_progress_20
                )
            }
        }

        notificationContentView.setTextViewText(R.id.tv_memory_num, "$percent%")

    }

    val country = DeviceTool.getCurrentCountry(ActivityMgr.getContext())
    /**美国地区温度单位℉（华氏度）**/
    if (country.trim() == "US") {
        notificationContentView.setTextViewText(R.id.tv_cpu_temperature, AppBean.temperature + "°F")
    } else {
        notificationContentView.setTextViewText(R.id.tv_cpu_temperature, AppBean.temperature + "°C")
    }
    notificationContentView.setViewVisibility(
        R.id.iv_clean_cache_today,
        if (AppBean.startBoostToday == getTodayDateStr()) View.GONE else View.VISIBLE
    )
    notificationContentView.setViewVisibility(
        R.id.iv_safe_today,
        if (AppBean.startSecurityToday == getTodayDateStr()) View.GONE else View.VISIBLE
    )

    val jumpIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)

    jumpIntent.flags =
        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP

    jumpIntent.putExtra("functionType", GlobalInfo.boostFunction)
    jumpIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_SYSTEM_RESIDENT)//通知类型
    jumpIntent.putExtra("contentType", "PhoneBoost")
    jumpIntent.putExtra("itemCategory", "Permanent")
    val pendingBoostIntent = PendingIntent.getActivity(
        ActivityMgr.getContext(),
        0,
        jumpIntent,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )
    notificationContentView.setOnClickPendingIntent(R.id.ll_tools_boost, pendingBoostIntent)

    jumpIntent.putExtra("functionType", GlobalInfo.saverFunction)
    jumpIntent.putExtra("contentType", "Battery")
    jumpIntent.putExtra("itemCategory", "Permanent")
    val pendingSaverIntent = PendingIntent.getActivity(
        ActivityMgr.getContext(),
        1,
        jumpIntent,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )
    notificationContentView.setOnClickPendingIntent(R.id.ll_tools_saver, pendingSaverIntent)

    jumpIntent.putExtra("functionType", GlobalInfo.coolerFunction)
    jumpIntent.putExtra("contentType", "CPU")
    jumpIntent.putExtra("itemCategory", "Permanent")
    val pendingCpuIntent = PendingIntent.getActivity(
        ActivityMgr.getContext(),
        2,
        jumpIntent,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )
    notificationContentView.setOnClickPendingIntent(R.id.ll_tools_cpu, pendingCpuIntent)

    jumpIntent.putExtra("functionType", GlobalInfo.securityFunction)
    jumpIntent.putExtra("contentType", "Virus")
    jumpIntent.putExtra("itemCategory", "Permanent")
    val pendingSafeIntent = PendingIntent.getActivity(
        ActivityMgr.getContext(),
        3,
        jumpIntent,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )
    notificationContentView.setOnClickPendingIntent(R.id.ll_tools_safe, pendingSafeIntent)

    jumpIntent.putExtra("functionType", GlobalInfo.cleanFunction)
    jumpIntent.putExtra("contentType", "CleanJunk")
    jumpIntent.putExtra("itemCategory", "Permanent")
    val pendingCleanIntent = PendingIntent.getActivity(
        ActivityMgr.getContext(),
        4,
        jumpIntent,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )
    notificationContentView.setOnClickPendingIntent(R.id.ll_tools_clean, pendingCleanIntent)

    //更多
    jumpIntent.putExtra("functionType", GlobalInfo.moreFunction)
    jumpIntent.putExtra("contentType", "MoreFunction")
    jumpIntent.putExtra("itemCategory", "Permanent")
    val pendingMoreIntent = PendingIntent.getActivity(
        ActivityMgr.getContext(),
        17,
        jumpIntent,
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )
    notificationContentView.setOnClickPendingIntent(R.id.ll_tools_more, pendingMoreIntent)

    val residentNotification =
        NotificationCompat.Builder(ActivityMgr.getContext(), "systemResident").apply {
            setWhen(System.currentTimeMillis())
            setSmallIcon(R.mipmap.notification_icon)
            setLargeIcon(
                BitmapFactory.decodeResource(
                    ActivityMgr.getContext().resources,
                    R.mipmap.app_icon
                )
            )
            setCustomContentView(notificationContentView)
            setCustomBigContentView(notificationContentView)
            setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
            setCategory(NotificationCompat.CATEGORY_SERVICE)
            priority = NotificationCompat.PRIORITY_HIGH
            setSilent(true)
            setVibrate(null)
            setSound(null)
            setOngoing(true)
        }.build()

    //设置通知栏常驻
    residentNotification.flags =
        Notification.FLAG_ONGOING_EVENT or Notification.FLAG_FOREGROUND_SERVICE or Notification.FLAG_NO_CLEAR

    val notificationManager =
        ActivityMgr.getContext()
            .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager

    notificationManager?.notify(1, residentNotification)
}

/**
 * 创建Boost需要重复推送的拉活通知
 */
//fun initBoostPushNotification(viewText: CharSequence) {
//    val notificationBoostView =
//        RemoteViews(ActivityMgr.getContext().packageName, R.layout.notification_boost_card)
//    notificationBoostView.setTextViewText(
//        R.id.tv_notification_boost,
//        viewText
//    )
//
//    val boostIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
//    boostIntent.flags =
//        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
//    boostIntent.putExtra("functionType", GlobalInfo.boostFunction)
//    boostIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
//    boostIntent.putExtra("notificationBoostTurn", true)//通知类型
//    boostIntent.putExtra("embeddingPoint", AnalyticsUtils.CARD_PHONE_BOOST)//埋点
//    boostIntent.putExtra("contentType", "PhoneBoost")
//    boostIntent.putExtra("itemCategory", "Card")
//    AppBean.residentNotificationChoiceType
//    val pendingBoostIntent = PendingIntent.getActivity(
//        ActivityMgr.getContext(),
//        5,
//        boostIntent,
//        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
//    )
//
//    // Dismiss Action
//    val dismissIntent = Intent(ActivityMgr.getContext(), NotificationIntentService::class.java)
//    dismissIntent.action = "dismiss"
//    dismissIntent.putExtra("notificationId", 2)//通知类型
//    val dismissPendingIntent =
//        PendingIntent.getService(
//            ActivityMgr.getContext(),
//            6,
//            dismissIntent,
//            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
//        )
//    notificationBoostView.setOnClickPendingIntent(R.id.iv_notification_close, dismissPendingIntent)
//    notificationBoostView.setOnClickPendingIntent(R.id.tv_later, dismissPendingIntent)
//
//    val notification =
//        NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush")
//            .setContent(notificationBoostView)
//            .setCustomBigContentView(notificationBoostView)
//            .setAutoCancel(true)
//            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//            .setPriority(NotificationCompat.PRIORITY_HIGH)
//            .setSmallIcon(R.mipmap.notification_icon)
//            .setLargeIcon(
//                BitmapFactory.decodeResource(
//                    ActivityMgr.getContext().resources,
//                    R.mipmap.app_icon
//                )
//            )
//            .setContentIntent(pendingBoostIntent)
//            .setFullScreenIntent(fullScreenIntent(), true)
//            .setVibrate(longArrayOf(0, 200, 300, 200, 300))
//            .setDefaults(Notification.DEFAULT_VIBRATE)
//            .setDefaults(Notification.DEFAULT_SOUND)
//            .build()
//
//    val notificationManager =
//        ActivityMgr.getContext()
//            .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
//
//    notificationManager?.notify(2, notification)
//}

/**
 * 创建Clean需要重复推送的拉活通知
 */
@SuppressLint("RestrictedApi")
fun initCleanPushNotification(
    viewText: CharSequence,
    notificationIcon: Int = R.mipmap.function_notification_clean,
    embeddingPoint: String = ""
) {
    try {
        var notificationCleanView =
            RemoteViews(ActivityMgr.getContext().packageName, R.layout.notification_clean_card)
        val notificationSmallView =
            RemoteViews(
                ActivityMgr.getContext().packageName,
                R.layout.notification_small_clean_card_layout
            )
        //android12以上版本
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            notificationCleanView = RemoteViews(ActivityMgr.getContext().packageName, R.layout.notification_large_clean_card)
        }
        notificationCleanView.setTextViewText(
            R.id.tv_notification_clean,
            Html.fromHtml(viewText.toString())
        )
        notificationSmallView.setTextViewText(
            R.id.tv_notification_clean,
            Html.fromHtml(viewText.toString())
        )
        notificationCleanView.setImageViewResource(R.id.iv_notification_icon, notificationIcon)
        val cleanIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
        cleanIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
        cleanIntent.putExtra("functionType", GlobalInfo.cleanFunction)
        cleanIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
        cleanIntent.putExtra("contentType", "CleanJunk")
        cleanIntent.putExtra("itemCategory", "Card")
        if (embeddingPoint.isNotEmpty()) {
            cleanIntent.putExtra("embeddingPoint", embeddingPoint)//埋点
        }
        val pendingCleanIntent = PendingIntent.getActivity(
            ActivityMgr.getContext(),
            7,
            cleanIntent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
        )
        notificationCleanView.setOnClickPendingIntent(R.id.ll_root, pendingCleanIntent)
        notificationSmallView.setOnClickPendingIntent(R.id.ll_root, pendingCleanIntent)

        // Dismiss Action
        val dismissIntent = Intent(ActivityMgr.getContext(), NotificationIntentService::class.java)
        dismissIntent.action = "dismiss"
        dismissIntent.putExtra("notificationId", 2)//通知类型
        val dismissPendingIntent =
            PendingIntent.getService(
                ActivityMgr.getContext(),
                8,
                dismissIntent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
            )
        notificationCleanView.setOnClickPendingIntent(
            R.id.iv_notification_close,
            dismissPendingIntent
        )
        notificationCleanView.setOnClickPendingIntent(R.id.tv_later, dismissPendingIntent)

//    val notification =
//        NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush")
//            .setContent(notificationCleanView)
//            .setCustomContentView(contentSmallViews)
//            .setCustomBigContentView(notificationCleanView)
//            .setAutoCancel(true)
//            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//            .setPriority(NotificationCompat.PRIORITY_MAX)
//            .setWhen(System.currentTimeMillis())
//            .setSmallIcon(R.mipmap.notification_icon)
//            .setLargeIcon(
//                BitmapFactory.decodeResource(
//                    ActivityMgr.getContext().resources,
//                    R.mipmap.app_icon
//                )
//            )
//            .setContentIntent(pendingCleanIntent)
//            .setFullScreenIntent(fullScreenIntent(), true)
//            .setVibrate(longArrayOf(0, 200, 300, 200, 300))
//            .setDefaults(Notification.DEFAULT_VIBRATE)
//            .setDefaults(Notification.DEFAULT_SOUND)
//            .build()
        val notificationBuilder =
            NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush").apply {
                setSmallIcon(R.mipmap.notification_icon)
//                setColor(Color.parseColor("#10AC53"))
                setAutoCancel(true)
                setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                setPriority(NotificationCompat.PRIORITY_MAX)
                setWhen(System.currentTimeMillis())
                setVibrate(longArrayOf(0, 200, 300, 200, 300))
                setLights(Color.BLUE, 2000, 1000)
                setFullScreenIntent(fullScreenIntent(), true)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        setCustomContentView(notificationSmallView)
                    } else {
                        setCustomContentView(notificationCleanView)
                    }
                } else {
                    setContent(notificationCleanView)
                }
                setCustomBigContentView(notificationCleanView)
                setLargeIcon(
                    BitmapFactory.decodeResource(
                        ActivityMgr.getContext().resources,
                        R.mipmap.app_icon
                    )
                )
            }

        val notification = notificationBuilder.build()
        notification.flags = Notification.FLAG_AUTO_CANCEL

        val notificationManager =
            ActivityMgr.getContext()
                .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager

        notificationManager?.notify(2, notification)
    }catch (e: Exception){
        e.printStackTrace()
        println("Exception ========================= "+e.message)
    }
}

/**
 * 创建Battery需要重复推送的拉活通知
 */
//fun initBatteryPushNotification(viewText: CharSequence, batteryCharge: Boolean = false) {
//    val notificationBatteryView =
//        RemoteViews(ActivityMgr.getContext().packageName, R.layout.notification_battery_card)
//    notificationBatteryView.setTextViewText(
//        R.id.tv_notification_battery,
//        viewText
//    )
//    var embeddingPoint = ""
//    embeddingPoint = if (batteryCharge) {
//        notificationBatteryView.setImageViewResource(
//            R.id.iv_notification_icon,
//            R.mipmap.function_notification_battery_push
//        )
//        AnalyticsUtils.CARD_CHARGE
//    }else{
//        AnalyticsUtils.CARD_BATTERY
//    }
//
//    val batteryIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
//    batteryIntent.flags =
//        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
//    batteryIntent.putExtra("functionType", GlobalInfo.saverFunction)
//    batteryIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
//    batteryIntent.putExtra("contentType", "Battery")
//    batteryIntent.putExtra("itemCategory", "Card")
//    batteryIntent.putExtra("embeddingPoint", embeddingPoint)//埋点
//    val pendingBatteryIntent = PendingIntent.getActivity(
//        ActivityMgr.getContext(),
//        9,
//        batteryIntent,
//        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
//    )
//
//    // Dismiss Action
//    val dismissIntent = Intent(ActivityMgr.getContext(), NotificationIntentService::class.java)
//    dismissIntent.action = "dismiss"
//    dismissIntent.putExtra("notificationId", 2)//通知类型
//    val dismissPendingIntent =
//        PendingIntent.getService(
//            ActivityMgr.getContext(),
//            10,
//            dismissIntent,
//            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
//        )
//    notificationBatteryView.setOnClickPendingIntent(
//        R.id.iv_notification_close,
//        dismissPendingIntent
//    )
//    notificationBatteryView.setOnClickPendingIntent(R.id.tv_later, dismissPendingIntent)
//
//    val notification =
//        NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush")
//            .setContent(notificationBatteryView)
//            .setCustomBigContentView(notificationBatteryView)
//            .setAutoCancel(true)
//            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//            .setPriority(NotificationCompat.PRIORITY_MAX)
//            .setWhen(System.currentTimeMillis())
//            .setSmallIcon(R.mipmap.notification_icon)
//            .setLargeIcon(
//                BitmapFactory.decodeResource(
//                    ActivityMgr.getContext().resources,
//                    R.mipmap.app_icon
//                )
//            )
//            .setContentIntent(pendingBatteryIntent)
//            .setFullScreenIntent(fullScreenIntent(), true)
//            .setVibrate(longArrayOf(0, 200, 300, 200, 300))
//            .setDefaults(Notification.DEFAULT_VIBRATE)
//            .setDefaults(Notification.DEFAULT_SOUND)
//            .build()
//
//    val notificationManager =
//        ActivityMgr.getContext()
//            .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
//
//    notificationManager?.notify(2, notification)
//}

/**
 * 创建Cooler需要重复推送的拉活通知
 */
//fun initCoolerPushNotification() {
//    val notificationCoolerView =
//        RemoteViews(ActivityMgr.getContext().packageName, R.layout.notification_cooler_card)
//
//    val country = DeviceTool.getCurrentCountry(ActivityMgr.getContext())
//    /**美国地区温度单位℉（华氏度）**/
//    if (country.trim() == "US") {
//        notificationCoolerView.setTextViewText(
//            R.id.tv_notification_cooler,
//            Html.fromHtml(
//                ActivityMgr.getContext()
//                    .getString(R.string.html_notification_cooler, AppBean.temperature + "°F")
//            )
//        )
//    } else {
//        notificationCoolerView.setTextViewText(
//            R.id.tv_notification_cooler,
//            Html.fromHtml(
//                ActivityMgr.getContext()
//                    .getString(R.string.html_notification_cooler, AppBean.temperature + "°C")
//            )
//        )
//    }
//
//    val coolerIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
//    coolerIntent.flags =
//        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
//    coolerIntent.putExtra("functionType", GlobalInfo.coolerFunction)
//    coolerIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
//    coolerIntent.putExtra("contentType", "CPU")
//    coolerIntent.putExtra("itemCategory", "Card")
//    coolerIntent.putExtra("embeddingPoint", AnalyticsUtils.CARD_CPU)//埋点
//    val pendingCoolerIntent = PendingIntent.getActivity(
//        ActivityMgr.getContext(),
//        11,
//        coolerIntent,
//        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
//    )
//
//    // Dismiss Action
//    val dismissIntent = Intent(ActivityMgr.getContext(), NotificationIntentService::class.java)
//    dismissIntent.action = "dismiss"
//    dismissIntent.putExtra("notificationId", 2)//通知类型
//    val dismissPendingIntent =
//        PendingIntent.getService(
//            ActivityMgr.getContext(),
//            12,
//            dismissIntent,
//            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
//        )
//    notificationCoolerView.setOnClickPendingIntent(R.id.iv_notification_close, dismissPendingIntent)
//    notificationCoolerView.setOnClickPendingIntent(R.id.tv_later, dismissPendingIntent)
//
//    val notification =
//        NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush")
//            .setContent(notificationCoolerView)
//            .setCustomBigContentView(notificationCoolerView)
//            .setAutoCancel(true)
//            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//            .setPriority(NotificationCompat.PRIORITY_MAX)
//            .setWhen(System.currentTimeMillis())
//            .setSmallIcon(R.mipmap.notification_icon)
//            .setLargeIcon(
//                BitmapFactory.decodeResource(
//                    ActivityMgr.getContext().resources,
//                    R.mipmap.app_icon
//                )
//            )
//            .setContentIntent(pendingCoolerIntent)
//            .setFullScreenIntent(fullScreenIntent(), true)
//            .setVibrate(longArrayOf(0, 200, 300, 200, 300))
//            .setDefaults(Notification.DEFAULT_VIBRATE)
//            .setDefaults(Notification.DEFAULT_SOUND)
//            .build()
//
//    val notificationManager =
//        ActivityMgr.getContext()
//            .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
//
//    notificationManager?.notify(2, notification)
//}

/**
 * 创建Security需要重复推送的拉活通知
 */
fun initSecurityPushNotification() {
    try {
        var notificationSecurityView =
            RemoteViews(ActivityMgr.getContext().packageName, R.layout.notification_security_card)
        val notificationSmallView =
            RemoteViews(
                ActivityMgr.getContext().packageName,
                R.layout.notification_small_security_card_layout
            )
        //android12以上版本
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            notificationSecurityView = RemoteViews(
                ActivityMgr.getContext().packageName,
                R.layout.notification_large_security_card
            )
        }
        notificationSecurityView.setTextViewText(
            R.id.tv_notification_security,
            ActivityMgr.getContext().getString(R.string.notification_security)
        )
        notificationSmallView.setTextViewText(
            R.id.tv_notification_security,
            ActivityMgr.getContext().getString(R.string.notification_security)
        )

        val securityIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
        securityIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
        securityIntent.putExtra("functionType", GlobalInfo.securityFunction)
        securityIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
        securityIntent.putExtra("contentType", "Virus")
        securityIntent.putExtra("itemCategory", "Card")
        securityIntent.putExtra("embeddingPoint", AnalyticsUtils.CARD_SECURITY)//埋点
        val pendingSecurityIntent = PendingIntent.getActivity(
            ActivityMgr.getContext(),
            13,
            securityIntent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
        )
        notificationSecurityView.setOnClickPendingIntent(R.id.ll_root, pendingSecurityIntent)
        notificationSmallView.setOnClickPendingIntent(R.id.ll_root, pendingSecurityIntent)

        // Dismiss Action
        val dismissIntent = Intent(ActivityMgr.getContext(), NotificationIntentService::class.java)
        dismissIntent.action = "dismiss"
        dismissIntent.putExtra("notificationId", 2)//通知类型
        val dismissPendingIntent =
            PendingIntent.getService(
                ActivityMgr.getContext(),
                14,
                dismissIntent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
            )
        notificationSecurityView.setOnClickPendingIntent(
            R.id.iv_notification_close,
            dismissPendingIntent
        )
        notificationSecurityView.setOnClickPendingIntent(R.id.tv_later, dismissPendingIntent)

//    val notification =
//        NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush")
//            .setContent(notificationSecurityView)
//            .setCustomBigContentView(notificationSecurityView)
//            .setAutoCancel(true)
//            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//            .setPriority(NotificationCompat.PRIORITY_MAX)
//            .setWhen(System.currentTimeMillis())
//            .setSmallIcon(R.mipmap.notification_icon)
//            .setLargeIcon(
//                BitmapFactory.decodeResource(
//                    ActivityMgr.getContext().resources,
//                    R.mipmap.app_icon
//                )
//            )
//            .setContentIntent(pendingSecurityIntent)
//            .setFullScreenIntent(fullScreenIntent(), true)
//            .setVibrate(longArrayOf(0, 200, 300, 200, 300))
//            .setDefaults(Notification.DEFAULT_VIBRATE)
//            .setDefaults(Notification.DEFAULT_SOUND)
//            .build()

        val notificationBuilder =
            NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush").apply {
                setSmallIcon(R.mipmap.notification_icon)
//                setColor(Color.parseColor("#10AC53"))
                setAutoCancel(true)
                setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                setPriority(NotificationCompat.PRIORITY_MAX)
                setWhen(System.currentTimeMillis())
                setVibrate(longArrayOf(0, 200, 300, 200, 300))
                setLights(Color.BLUE, 2000, 1000)
                setFullScreenIntent(fullScreenIntent(), true)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        setCustomContentView(notificationSmallView)
                    } else {
                        setCustomContentView(notificationSecurityView)
                    }
                } else {
                    setContent(notificationSecurityView)
                }
                setCustomBigContentView(notificationSecurityView)
                setLargeIcon(
                    BitmapFactory.decodeResource(
                        ActivityMgr.getContext().resources,
                        R.mipmap.app_icon
                    )
                )
            }

        val notification = notificationBuilder.build()
        notification.flags = Notification.FLAG_AUTO_CANCEL

        val notificationManager =
            ActivityMgr.getContext()
                .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager

        notificationManager?.notify(2, notification)
    }catch (e: Exception){
        e.printStackTrace()
    }
}

/**
 * 创建Clean需要重复推送的拉活通知
 */
fun initTargetedPushNotification(
    viewText: CharSequence,
    notificationIcon: Int = R.mipmap.tiktok_clean_up,
    functionType: String
) {
    try {
        var notificationCleanView =
            RemoteViews(
                ActivityMgr.getContext().packageName,
                R.layout.notification_targeted_clean_card
            )
        val notificationSmallView =
            RemoteViews(
                ActivityMgr.getContext().packageName,
                R.layout.notification_small_targeted_card_layout
            )
        //android12以上版本
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            notificationCleanView = RemoteViews(
                ActivityMgr.getContext().packageName,
                R.layout.notification_large_targeted_clean_card
            )
        }
        notificationCleanView.setTextViewText(
            R.id.tv_notification_clean,
            viewText
        )
        notificationSmallView.setTextViewText(
            R.id.tv_notification_clean,
            viewText
        )
        notificationCleanView.setImageViewResource(R.id.iv_notification_icon, notificationIcon)

        val cleanIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
        cleanIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
        cleanIntent.putExtra("functionType", functionType)
        cleanIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_TARGETED_CARD)//通知类型
        cleanIntent.putExtra("itemCategory", "Card")
        when (notificationIcon) {
            R.mipmap.tiktok_clean_up -> {
                cleanIntent.putExtra("contentType", "TikTok")
            }
            R.mipmap.whatsapp_clean_up -> {
                cleanIntent.putExtra("contentType", "WhatsApp")
            }
            R.mipmap.google_play_clean_up -> {
                cleanIntent.putExtra("contentType", "GooglePlay")
            }
            R.mipmap.youtube_clean_up -> {
                cleanIntent.putExtra("contentType", "Youtube")
            }
        }
        val pendingTargetedIntent = PendingIntent.getActivity(
            ActivityMgr.getContext(),
            15,
            cleanIntent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
        )
        notificationCleanView.setOnClickPendingIntent(R.id.ll_root, pendingTargetedIntent)
        notificationSmallView.setOnClickPendingIntent(R.id.ll_root, pendingTargetedIntent)

        // Dismiss Action
        val dismissIntent = Intent(ActivityMgr.getContext(), NotificationIntentService::class.java)
        dismissIntent.action = "dismiss"
        dismissIntent.putExtra("notificationId", 3)//通知类型
        val dismissPendingIntent =
            PendingIntent.getService(
                ActivityMgr.getContext(),
                16,
                dismissIntent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
            )
        notificationCleanView.setOnClickPendingIntent(
            R.id.iv_notification_close,
            dismissPendingIntent
        )
        notificationCleanView.setOnClickPendingIntent(R.id.tv_later, dismissPendingIntent)

//    val notification =
//        NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush")
//            .setContent(notificationCleanView)
//            .setCustomBigContentView(notificationCleanView)
//            .setAutoCancel(true)
//            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//            .setPriority(NotificationCompat.PRIORITY_MAX)
//            .setWhen(System.currentTimeMillis())
//            .setSmallIcon(R.mipmap.notification_icon)
//            .setLargeIcon(
//                BitmapFactory.decodeResource(
//                    ActivityMgr.getContext().resources,
//                    R.mipmap.app_icon
//                )
//            )
//            .setContentIntent(pendingTargetedIntent)
//            .setFullScreenIntent(fullScreenIntent(), true)
//            .setVibrate(longArrayOf(0, 200, 300, 200, 300))
//            .setDefaults(Notification.DEFAULT_VIBRATE)
//            .setDefaults(Notification.DEFAULT_SOUND)
//            .build()

        val notificationBuilder =
            NotificationCompat.Builder(ActivityMgr.getContext(), "functionPush").apply {
                setSmallIcon(R.mipmap.notification_icon)
//                setColor(Color.parseColor("#10AC53"))
                setAutoCancel(true)
                setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                setPriority(NotificationCompat.PRIORITY_MAX)
                setWhen(System.currentTimeMillis())
                setVibrate(longArrayOf(0, 200, 300, 200, 300))
                setLights(Color.BLUE, 2000, 1000)
                setFullScreenIntent(fullScreenIntent(), true)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        setCustomContentView(notificationSmallView)
                    } else {
                        setCustomContentView(notificationCleanView)
                    }
                } else {
                    setContent(notificationCleanView)
                }
                setCustomBigContentView(notificationCleanView)
                setLargeIcon(
                    BitmapFactory.decodeResource(
                        ActivityMgr.getContext().resources,
                        R.mipmap.app_icon
                    )
                )
            }

        val notification = notificationBuilder.build()
        notification.flags = Notification.FLAG_AUTO_CANCEL

        val notificationManager =
            ActivityMgr.getContext()
                .getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager

        notificationManager?.notify(3, notification)
    }catch (e: Exception){
        e.printStackTrace()
    }
}

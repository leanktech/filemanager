package com.hyperstudio.hyper.file.service

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.alive.worker.StickyServiceWork
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.initKeepAliveNotificationChannel
import com.hyperstudio.hyper.file.broadcast.ScreenOnBroadcastReceiver
import com.hyperstudio.hyper.file.unit.getAppAddRemoveReceiverIntentFilter
import com.hyperstudio.hyper.file.unit.getSimpleReceiverIntentFilter
import java.util.concurrent.TimeUnit

/**
 * @Description: 亮屏监控服务
 * author: Rhett
 * time: 2021/12/23
 */
class NotificationPushMonitorService : Service() {

    private val screenOnBroadcastReceiver = ScreenOnBroadcastReceiver

    override fun onCreate() {
        super.onCreate()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            initKeepAliveNotificationChannel()
            val keepServiceAliveNotification = NotificationCompat.Builder(this, "serviceLive").apply {
                setContentTitle("Hyper File Manager")
                setContentText("App is running")
                setWhen(System.currentTimeMillis())
                setSmallIcon(R.mipmap.keep_alive_notification_small_icon)
                setContentIntent(PendingIntent.getActivity(
                    ActivityMgr.getContext(),
                    -1,
                    Intent(),
                    PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                ))
            }.build()
            startForeground(4, keepServiceAliveNotification)
        }

        //注册广播
        registerReceiver(
            screenOnBroadcastReceiver,
            getSimpleReceiverIntentFilter()
        )
        registerReceiver(
            screenOnBroadcastReceiver,
            getAppAddRemoveReceiverIntentFilter()
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        startForeground(1, initSystemResidentNotification(true))// 开始前台服务
        return START_STICKY //START_REDELIVER_INTENT
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            stopForeground(true);// 停止前台服务--参数：表示是否移除之前的通知
            //注销广播
            unregisterReceiver(screenOnBroadcastReceiver)

            //Service被Kill后保证广播能正常接收
            val oneTimeWorkRequest = OneTimeWorkRequest.Builder(StickyServiceWork::class.java)
                .setInitialDelay(3, TimeUnit.SECONDS) //延迟3s执行
                .build()
            WorkManager.getInstance(ActivityMgr.getContext()).enqueue(oneTimeWorkRequest)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBind(p0: Intent?): IBinder? = null

}
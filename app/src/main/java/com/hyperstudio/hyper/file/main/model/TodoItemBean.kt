package com.hyperstudio.hyper.file.main.model

import androidx.databinding.BaseObservable
import java.io.Serializable

/**
 * @Name: CleanFootItemBean
 * @Description: 功能Item
 * @Author: even@leanktech.com
 * @Date: 2021/8/26 14:27
 */
class TodoItemBean: BaseObservable(), Serializable {
    var itemName: String = ""
    var itemImage: Int = 0
    var itemIntro: String = ""
    var itemButtonText: String = ""
}
package com.hyperstudio.hyper.file.unit

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.PowerManager
import android.text.Html
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.DeviceTool
import com.hyperstudio.hyper.file.base_lib.tool.DeviceTool.getTodayDate
import com.hyperstudio.hyper.file.base_lib.tool.isNumeric
import com.hyperstudio.hyper.file.push.notification.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.random.Random

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/6/21 17:38
 */
object SystemBroadcastModel {
    /**
     * 默认数据
     * */
    private const val DEFAULT_TEMPERATURE = 33 //默认CPU温度
    var INTERVAL_NOTIFY = 1 * 60 * 60 * 1000L //所有类型通知最小间隔1h
    var INTERVAL_CHARGING_PROTECT = 4 * 60 * 60 * 1000L //电量间隔4分钟
    var INTERVAL_BATTERY = 40 //最低电量40%
    var INTERVAL_CLEAN = 8 * 60 * 60 * 1000L //清理间隔时间8小时
    var INTERVAL_BOOST = 75 //内存低于75%时出发BOOST通知
    var INTERVAL_BOOST_TIME = 4 * 60 * 60 * 1000L //boost间隔时间4小时
    var INTERVAL_COOL_CPU = 1 * 60 * 60 * 1000L //屏幕使用时长1小时触发COOl通知
    var INTERVAL_COOL_CPU_COUNT = 2 //COOL通知每天最多触发2次
    var INTERVAL_WIFI_SPEED = 4 * 60 * 60 * 1000L //Wi-Fi Speed通知间隔4小时触发一次
    var INTERVAL_ADD_COUNT = 2 //安装 00：00-24：00 内最多触发两次
    var INTERVAL_REMOVE_COUNT = 2 //卸载 00：00-24：00 内最多触发两次
    var INTERVAL_WIFI_SPEED_COUNT = 1 //Wi-Fi Speed通知每天最多触发1次

    /**
     * 模拟远程控制数据
     * */
    /**远程控制的清理间隔时间（8小时）*/
    private const val REMOTE_CONFIG_INTERVAL_CLEAN_UP = 8

    /**远程控制的BOOST触发时机时的内存百分比（低于75%）*/
    private const val REMOTE_CONFIG_BOOST = 75

    /**远程控制的BOOST触发的间隔时间（4小时）*/
    private const val REMOTE_CONFIG_INTERVAL_BOOST_TIME = 4

    /**远程控制的最低电量40%（当前电量小等于40% 每60分钟提示一次）*/
    private const val REMOTE_CONFIG_BATTERY = 40

    /**battery通知的间隔时间4小时*/
    private const val REMOTE_CONFIG_INTERVAL_BATTERY_TIME = 4

    /**远程控制的屏幕使用时长（大于等于1小时时才触发COOL通知）*/
    private const val REMOTE_CONFIG_CPU_COOLER_LENGTH = 1

    /**远程控制的cpu加速每天执行次数（最多2次）*/
    private const val REMOTE_CONFIG_CPU_DAIL_TIMES = 2

    /**远程控制的安装每天执行次数（最多2次）*/
    private const val REMOTE_CONFIG_ADD_TIMES = 2

    /**远程控制的卸载每天执行次数（最多2次）*/
    private const val REMOTE_CONFIG_REMOVE_TIMES = 2

    /**远程控制的Wi-Fi Speed每天执行次数（最多1次）*/
    private const val REMOTE_CONFIG_WIFI_SPEED_TIMES = 1

    var lastTemperature = DEFAULT_TEMPERATURE

    /**
     * 通过RemoteConfig 获取通知周期、间隔
     * */
    fun getRemoteConfig2Net() {
        // 清理时间间隔 8h
        val cleanHour = REMOTE_CONFIG_INTERVAL_CLEAN_UP
        if (cleanHour > 0) INTERVAL_CLEAN = cleanHour * 3600 * 1000L
        // 运行内存使用占比 75%
        val boostHour = REMOTE_CONFIG_BOOST
        if (boostHour > 0) INTERVAL_BOOST = boostHour
        // boost 时间间隔
        val boostTime = REMOTE_CONFIG_INTERVAL_BOOST_TIME
        if (boostTime > 0) INTERVAL_BOOST_TIME = boostTime * 3600 * 1000L
        // 屏幕使用时长 1h
        val cpuHour = REMOTE_CONFIG_CPU_COOLER_LENGTH
        if (cpuHour > 0) INTERVAL_COOL_CPU = cpuHour * 3600 * 1000L
        // cpu加速每天次数 2
        val cpuCount = REMOTE_CONFIG_CPU_DAIL_TIMES
        if (cpuCount > 0) INTERVAL_COOL_CPU_COUNT = cpuCount
        // 最低电量 40%
        val battery = REMOTE_CONFIG_BATTERY
        if (battery > 0) INTERVAL_BATTERY = cpuCount
        // 电量 时间间隔
        val batteryTime = REMOTE_CONFIG_INTERVAL_BATTERY_TIME
        if (batteryTime > 0) INTERVAL_CHARGING_PROTECT = batteryTime * 3600 * 1000L
        // 安装每天触发次数 2
        val addCount = REMOTE_CONFIG_ADD_TIMES
        if (addCount > 0) INTERVAL_ADD_COUNT = addCount
        // 卸载每天触发次数 2
        val removeCount = REMOTE_CONFIG_REMOVE_TIMES
        if (removeCount > 0) INTERVAL_REMOVE_COUNT = removeCount
        // Wi-Fi Speed每天触发次数 1
        val wifiCount = REMOTE_CONFIG_WIFI_SPEED_TIMES
        if (wifiCount > 0) INTERVAL_WIFI_SPEED_COUNT = wifiCount
    }

    /**
     * 充电通知
     */
//    fun handleReceivedBatteryTemperature(
//        context: Context,
//        intent: Intent?,
//        isPower: Boolean = false
//    ) {
//        // 当前CPU温度
//        lastTemperature = if (intent == null) {
//            DEFAULT_TEMPERATURE
//        } else {
//            BatteryInfoUtil.getTemperature(intent)
//        }
//
//        //获取当前系统设置的语言所属国家
//        val country = DeviceTool.getCurrentCountry(context)
//        /**美国地区温度单位℉（华氏度）**/
//        if (country.trim() == "US") {
//            AppBean.temperature = BatteryInfoUtil.transformC2F(lastTemperature).toString()
//        } else {
//            AppBean.temperature = lastTemperature.toString()
//        }
//        /**更新常驻通知(电池温度有改变时)**/
//        //TODO 暂且屏蔽
////        updateSystemResidentNotification()
//
//        // 充电时
//        if (System.currentTimeMillis() - AppBean.lastPushPowerTime >= INTERVAL_CHARGING_PROTECT
//            && BatteryInfoUtil.getElectricQuantity(context) != 100
//            && isPower
//        ) {
//            AppBean.lastPushPowerTime = System.currentTimeMillis()
//            val randNum = Random.nextInt(10) + 20
//            initBatteryPushNotification(
//                Html.fromHtml(
//                    ActivityMgr.getContext()
//                        .getString(
//                            R.string.html_notification_battery_is_charging,
//                            randNum.toString()
//                        )
//                ), true
//            )
//        }
//    }

    /**
     * CPU冷却 通知
     */
    @SuppressLint("NewApi")
//    fun handlerReceiverCoolCpu(context: Context) {
//        val pm: PowerManager =
//            context.getSystemService(Service.POWER_SERVICE) as PowerManager
//        val isScreenOn = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
//            pm.isInteractive
//        }else{
//            pm.isScreenOn
//        }
//        if (isScreenOn) {
//            if (AppBean.screenOnTimer < 0) {
//                AppBean.screenOnTimer = System.currentTimeMillis()
//                return
//            }
//            // 用户没有在设置中关闭通知，手机解锁后持续亮屏时间大于等于1h时触发通知，间隔10h触发一次，每日最多2次
//            var str = AppBean.dayCoolCount
//            if (str.isEmpty()) str = "${getTodayDate()}:0"
//            val tempArr = str.split(":")
//            val day = tempArr[0]
//            var count = tempArr[1].toInt()
//            if (System.currentTimeMillis() - AppBean.screenOnTimer >= INTERVAL_COOL_CPU
//                && System.currentTimeMillis() - AppBean.lastCoolCpuNotifyTime > 10 * 3600 * 1000L
//            ) {
//                if (day == getTodayDate() && count <= INTERVAL_COOL_CPU_COUNT) {
//                    AppBean.dayCoolCount = "${getTodayDate()}:${count++}"
//                    initCoolerPushNotification()
//                    AppBean.lastCoolCpuNotifyTime = System.currentTimeMillis()
//                } else if (day != getTodayDate()) {
//                    AppBean.dayCoolCount = "${getTodayDate()}:1"
//                    initCoolerPushNotification()
//                    AppBean.lastCoolCpuNotifyTime = System.currentTimeMillis()
//                }
//            }
//        } else {
//            AppBean.screenOnTimer = System.currentTimeMillis()
//        }
//    }

    /**
     * 应用安装/卸载 清理通知
     * @param isAdd 是否是安装/更新
     */
    fun handleReceiverPackageAddUpdateRemoved(context: Context, intent: Intent?, isAdd: Boolean) {
        var str = AppBean.addCount
        if (!isAdd) {
            str = AppBean.removeCount
        }
        if (str.isNullOrEmpty()) str = "${getTodayDate()}:0"
        val tempArr = str.split(":")
        val day = tempArr[0]
        var count = tempArr[1].toInt()
        if (isAdd) {
            if (day == getTodayDate() && count < INTERVAL_ADD_COUNT) {
                AppBean.addCount = "${getTodayDate()}:${++count}"
            } else if (day != getTodayDate()) {
                AppBean.addCount = "${getTodayDate()}:1"
            } else {
                return
            }
        } else {
            if (day == getTodayDate() && count < INTERVAL_REMOVE_COUNT) {
                AppBean.removeCount = "${getTodayDate()}:${++count}"
            } else if (day != getTodayDate()) {
                AppBean.removeCount = "${getTodayDate()}:1"
            } else {
                return
            }
        }
        var appName = ""//app名称
        try {
            val packageName = intent!!.data!!.schemeSpecificPart//包名
            val packageManager = context.packageManager
            if (intent.action == Intent.ACTION_PACKAGE_ADDED ||
                intent.action == Intent.ACTION_PACKAGE_REPLACED
            ) {
                val applicationInfo = packageManager.getApplicationInfo(
                    packageName,
                    PackageManager.GET_META_DATA
                )
                appName = try {
                    if (applicationInfo != null) {
                        packageManager?.getApplicationLabel(applicationInfo)
                            .toString()  //加空格隔开
                    } else {
                        ""
                    }
                } catch (e: Exception) {
                    ""
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            appName = ""
        }
        if (isAdd) {
            initCleanPushNotification(
                Html.fromHtml(
                    appName +
                            ActivityMgr.getContext()
                                .getString(R.string.html_notification_clean_Installation)
                ), R.mipmap.function_notification_package_add,
                AnalyticsUtils.CARD_INSTALL
            )
        } else {
            initCleanPushNotification(
                Html.fromHtml(
                    ActivityMgr.getContext()
                        .getString(R.string.html_notification_clean_uninstall)
                ), R.mipmap.function_notification_remove,
                AnalyticsUtils.CARD_UNINSTALL
            )
        }
    }

    fun handleReceivedUserPresent(context: Context, intent: Intent?) {
        GlobalScope.launch {
            delay(3000)

            // 所有类型通知最小间隔1h
            val currentTime = System.currentTimeMillis()

//            // 以下通知 优先级：电池保护>清理文件>手机加速>CPU加速>Wi-Fi Speed
//            // 当前电量小等于  40% 每4小时提示一次
//            var currentLevel = BatteryInfoUtil.getElectricQuantity(context)
//            if (currentLevel == 0) currentLevel = 50
//            if (currentLevel <= INTERVAL_BATTERY) {
//                if (currentTime - AppBean.lastPushPowerTime >= INTERVAL_CHARGING_PROTECT) {
//                    /**推送Battery通知*/
//                    initBatteryPushNotification(
//                        Html.fromHtml(
//                        ActivityMgr.getContext()
//                            .getString(R.string.html_notification_battery, currentLevel.toString())
//                    ))
//                    AppBean.lastPushPowerTime = System.currentTimeMillis()
//                    return@launch
//                }
//            }

            // 当天8:00-22:00内，每8h展示一次清理通知
            val date: Date = Calendar.getInstance().time
            if (date.hours in 8..21
                && currentTime - AppBean.lastCleanNotifyTime >= INTERVAL_CLEAN
            ) {
                initCleanPushNotification(
                    ActivityMgr.getContext().getString(R.string.notification_clean),
                    embeddingPoint = AnalyticsUtils.CARD_CLEAN_JUNK
                )
                AppBean.lastCleanNotifyTime = System.currentTimeMillis()
                return@launch
            }

//            // 当手机运行内存（RAM）使用占比超过75%时触发通知，需要判断手机是否已经解锁，未解锁时不触发
//            AppBean.memoryNumber = SplashDataObject.getMemoryUsedPercentage(context)
//            val memoryPercentage = if (isNumeric(AppBean.memoryNumber))
//                AppBean.memoryNumber.toInt()
//            else 60
//
//            if (memoryPercentage >= INTERVAL_BOOST
//                && currentTime - AppBean.lastBoostNotifyTime >= INTERVAL_BOOST_TIME
//            ) {
//                initBoostPushNotification(
//                    Html.fromHtml(
//                        ActivityMgr.getContext()
//                            .getString(R.string.html_notification_boost, AppBean.memoryNumber + "%")
//                    ))
//                AppBean.lastBoostNotifyTime = System.currentTimeMillis()
//                return@launch
//            }

            var str = AppBean.securityNotifyCount
            if (str.isEmpty()) str = "${getTodayDate()}:0"
            val tempArr = str.split(":")
            val day = tempArr[0]
            var count = tempArr[1].toInt()
            // Wi-Fi Speed Tester使用量较少每4h展示一次
            if (currentTime - AppBean.lastSecurityNotifyTime >= INTERVAL_WIFI_SPEED) {
                if (day == getTodayDate() && count < INTERVAL_WIFI_SPEED_COUNT) {
                    AppBean.securityNotifyCount = "${getTodayDate()}:${++count}"
                    initSecurityPushNotification()
                    AppBean.lastSecurityNotifyTime = System.currentTimeMillis()
                } else if (day != getTodayDate()) {
                    AppBean.securityNotifyCount = "${getTodayDate()}:1"
                    initSecurityPushNotification()
                    AppBean.lastSecurityNotifyTime = System.currentTimeMillis()
                }
                return@launch
            }
        }
    }
}
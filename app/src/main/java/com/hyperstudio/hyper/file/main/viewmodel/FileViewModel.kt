package com.hyperstudio.hyper.file.main.viewmodel

import androidx.databinding.ObservableArrayList
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.file.defaultOrder
import com.hyperstudio.hyper.file.base_lib.tool.file.getFileSize
import com.hyperstudio.hyper.file.base_lib.tool.file.getSonNode
import com.hyperstudio.hyper.file.base_lib.tool.init
import com.hyperstudio.hyper.file.base_lib.tool.param.*
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.main.model.FileListItemBean
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

/**
 * @Name: FileViewModel
 * @Description: File Vm
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */

class FileViewModel : BaseViewMode() {

    var fileText = init("")

    var fileList = ObservableArrayList<FileListItemBean>()

    /**
     * 加载FileList方法
     */
    suspend fun loadFolderList(file: String, callBack:() -> Unit) {
        launchUI {
            fileText.value = file
            val list = getSonNode(file)
            if (list != null) {
                val dataList = mutableListOf<FileListItemBean>()
                withContext(Dispatchers.IO) {
                    Collections.sort(list, defaultOrder())
                    for (map in list) {
                        val fileType = map[FILE_INFO_TYPE] as String?
                        val gMap = FileListItemBean()
                        if (map[FILE_INFO_ISFOLDER] == true) {
                            gMap.fileIsDir = true
                            gMap.fileImg = R.mipmap.icon_filetype_folder
                            gMap.fileInfo = map[FILE_INFO_NUM_SONDIRS].toString() + ActivityMgr.getContext().getString(R.string.folder_text) + "  " +
                                    map[FILE_INFO_NUM_SONFILES] + ActivityMgr.getContext().getString(R.string.file_text)
                        } else {
                            gMap.fileIsDir = false
                            if (fileType == "txt" || fileType == "text") {
                                gMap.fileImg = R.mipmap.icon_filetype_text
                            } else {
                                gMap.fileImg = R.mipmap.icon_filetype_unknow
                            }
                            gMap.fileInfo = ActivityMgr.getContext().getString(R.string.file_size_text) + ":" + getFileSize(map[FILE_INFO_PATH].toString())
                        }
                        (map[FILE_INFO_NAME] as? String)?.let {
                            gMap.fName = it
                        }
                        gMap.fPath = map[FILE_INFO_PATH].toString()
                        dataList.add(gMap)
                    }
                }
                fileList.clear()
                fileList.addAll(dataList)
                callBack()
            } else {
                fileList.clear()
                callBack()
            }
        }
    }

}
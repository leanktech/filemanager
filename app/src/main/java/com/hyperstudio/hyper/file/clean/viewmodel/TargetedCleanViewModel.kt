package com.hyperstudio.hyper.file.clean.viewmodel

import android.os.Environment
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.base_lib.tool.print
import java.io.File

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/3/28 17:13
 */
class TargetedCleanViewModel : BaseViewMode() {

    fun targetedCleanUp(type: String){
        when(type){
            GlobalInfo.targetedTiktokFunction -> {
                val tiktokCache = File(Environment.getExternalStorageState(), "Android/data/" + GlobalInfo.tiktokPackageName)
                clearCacheFolder(tiktokCache)
            }
            GlobalInfo.targetedWhatsAppFunction -> {
                val whatsAppCache = File(Environment.getExternalStorageState(), "Android/data/" + GlobalInfo.whatsAppPackageName)
                clearCacheFolder(whatsAppCache)
            }
            GlobalInfo.targetedGooglePlayFunction -> {
                val googlePlayCache = File(Environment.getExternalStorageState(), "Android/data/" + GlobalInfo.googlePlayPackageName)
                clearCacheFolder(googlePlayCache)
            }
            GlobalInfo.targetedYouTubeFunction -> {
                val youTubeCache = File(Environment.getExternalStorageState(), "Android/data/" + GlobalInfo.youTubePackageName)
                clearCacheFolder(youTubeCache)
            }
            GlobalInfo.targetedLineFunction -> {
                val lineCache = File(Environment.getExternalStorageState(), "Android/data/" + GlobalInfo.linePackageName)
                clearCacheFolder(lineCache)
            }
        }
    }

    /**
     * 清除缓存目录
     * @param dir 目录
     * @param curTime 当前系统时间
     */
    private fun clearCacheFolder(dir: File?, curTime: Long = System.currentTimeMillis()): Int {
        var deletedFiles = 0
        if (dir != null && dir.isDirectory) {
            try {
                dir.listFiles()?.let { fileList ->
                    for (child in fileList) {
                        if (child.isDirectory) {
                            deletedFiles += clearCacheFolder(child, curTime)
                        }
                        if (child.lastModified() < curTime) {
                            if (child.delete()) {
                                deletedFiles++
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        "clearCacheFolder: 清除目录: " + dir?.absolutePath?.print()
        return deletedFiles
    }


}
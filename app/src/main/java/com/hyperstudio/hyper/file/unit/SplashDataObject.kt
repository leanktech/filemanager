package com.hyperstudio.hyper.file.unit

import android.app.ActivityManager
import android.content.Context
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.file.defaultOrder
import com.hyperstudio.hyper.file.base_lib.tool.file.getBasePath
import com.hyperstudio.hyper.file.base_lib.tool.file.getFileSize
import com.hyperstudio.hyper.file.base_lib.tool.file.getSonNode
import com.hyperstudio.hyper.file.base_lib.tool.isNumeric
import com.hyperstudio.hyper.file.base_lib.tool.param.*
import com.hyperstudio.hyper.file.main.model.FileListItemBean
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.BufferedReader
import java.io.FileReader
import java.lang.Exception
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * @Name:SplashDataObject
 * @Description: Splash Loading时需要加载的数据
 * @Author: even@leanktech.com
 * @Date: 2021/11/10 15:45
 */
object SplashDataObject {

    var baseFileText: String? = null

    var fileList: MutableList<FileListItemBean>? = null

    suspend fun loadFolderList() {
        getBasePath()?.let { baseFile ->
            baseFileText = baseFile
            val list = getSonNode(baseFile)
            if (list != null) {
                withContext(Dispatchers.IO) {
                    fileList = mutableListOf()
                    Collections.sort(list, defaultOrder())
                    for (map in list) {
                        val fileType = map[FILE_INFO_TYPE] as String?
                        val gMap = FileListItemBean()
                        if (map[FILE_INFO_ISFOLDER] == true) {
                            gMap.fileIsDir = true
                            gMap.fileImg = R.mipmap.icon_filetype_folder
                            gMap.fileInfo =
                                map[FILE_INFO_NUM_SONDIRS].toString() + ActivityMgr.getContext()
                                    .getString(
                                        R.string.folder_text
                                    ) + "  " +
                                        map[FILE_INFO_NUM_SONFILES] + ActivityMgr.getContext()
                                    .getString(
                                        R.string.file_text
                                    )
                        } else {
                            gMap.fileIsDir = false
                            if (fileType == "txt" || fileType == "text") {
                                gMap.fileImg = R.mipmap.icon_filetype_text
                            } else {
                                gMap.fileImg = R.mipmap.icon_filetype_unknow
                            }
                            gMap.fileInfo = ActivityMgr.getContext()
                                .getString(R.string.file_size_text) + ":" + getFileSize(map[FILE_INFO_PATH].toString())
                        }
                        (map[FILE_INFO_NAME] as? String)?.let {
                            gMap.fName = it
                        }
                        gMap.fPath = map[FILE_INFO_PATH].toString()
                        fileList?.add(gMap)
                    }
                }
            }
        }
    }

    /**
     * 获取android总运行内存大小
     * @param context
     */
    fun getMemoryUsedPercentage(context: Context): String {
        var totalBytesUnit = 1000L
        var availBytesUnit = 400L

        //获取总运行内存大小
        val str1 = "/proc/meminfo" // 系统内存信息文件
        val str2: String
        val arrayOfString: Array<String>
        try {
            val localFileReader = FileReader(str1)
            val localBufferedReader = BufferedReader(localFileReader, 8192)
            str2 = localBufferedReader.readLine() // 读取meminfo第一行，系统总内存大小
            arrayOfString = str2.split("\\s+").toTypedArray()
            // 获得系统总内存，单位是KB
            val p: Pattern = Pattern.compile("\\d+")
            val m: Matcher = p.matcher(arrayOfString[0])
            m.find()
            val i = m.group().toInt()
            //int值乘以1024转换为long类型 单位是B
            val initialMemory = i.toLong() * 1024
            localBufferedReader.close()
            if (initialMemory > 0 && isNumeric(initialMemory.toString())) {
                totalBytesUnit = initialMemory
            }

            //获取可用内存大小
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val mi = ActivityManager.MemoryInfo()
            am.getMemoryInfo(mi)
            // mi.availMem; 当前系统的可用内存
            if (mi.availMem > 0 && isNumeric(mi.availMem.toString())) {
                availBytesUnit = mi.availMem
            }

            //获取已用内存大小
            val usedBytesUnit = totalBytesUnit - availBytesUnit
            //拿到总运行内存分成100份 1份的步长
            val memoryStepLength = totalBytesUnit / 100
            //拿到已用运行内存空间占到100份中的几份
            val usedMemoryLength = usedBytesUnit / memoryStepLength
            return if (usedMemoryLength > 0 && isNumeric(usedMemoryLength.toString())) {
                usedMemoryLength.toString()
            } else {
                "60"
            }

        } catch (e: Exception) {
        }
        return "60"
    }

}
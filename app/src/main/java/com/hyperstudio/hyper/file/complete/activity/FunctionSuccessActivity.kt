package com.hyperstudio.hyper.file.complete.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.view.KeyEvent
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.BR
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.admob.initNativeAdView
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.adapter.MultiAdapter
import com.hyperstudio.hyper.file.base_lib.adapter.`interface`.ItemConvert
import com.hyperstudio.hyper.file.base_lib.base.ItemClickPresenter
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.clean.activity.CleanCacheActivity
import com.hyperstudio.hyper.file.complete.model.FunctionSuccessBean
import com.hyperstudio.hyper.file.complete.viewmodel.FunctionSuccessViewModel
import com.hyperstudio.hyper.file.databinding.FunctionSuccessAdBinding
import com.hyperstudio.hyper.file.databinding.FunctionSuccessLayoutBinding
import com.hyperstudio.hyper.file.main.activity.MainActivity
import com.hyperstudio.hyper.file.score.GiveScoreDialogFragment
import com.hyperstudio.hyper.file.trustlook.activity.TrustLookActivity
import com.hyperstudio.hyper.file.trustlook.dialog.AgreeTrustDialogFragment
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import com.hyperstudio.hyper.file.unit.startCleanActivity
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Name:FunctionSuccess
 * @Description: 功能完成跳转页
 * @Author: even@leanktech.com
 * @Date: 2021/10/21 16:21
 */
class FunctionSuccessActivity : BaseActivity<FunctionSuccessLayoutBinding>(),
    ItemClickPresenter<FunctionSuccessBean> {

    private val functionSuccessViewModel by viewModel<FunctionSuccessViewModel>()

    var dangerList: MutableList<*>? = null

    var functionType = ""

    /**
     * Todo多布局
     */
    private val mAdapter by lazy {
        MultiAdapter(functionSuccessViewModel.functionSuccessList, BR.presenter, BR.item).apply {
            addTypeToLayout(FunctionSuccessBean.head, R.layout.function_success_head)
            addTypeToLayout(FunctionSuccessBean.dangerApp, R.layout.function_success_danger_app)
            addTypeToLayout(FunctionSuccessBean.ad, R.layout.function_success_ad)
            addTypeToLayout(FunctionSuccessBean.foot, R.layout.function_success_foot_item)
            itemPresenter = this@FunctionSuccessActivity
            itemConvert = object : ItemConvert {
                @SuppressLint("SetTextI18n")
                override fun multiConvert(
                    holder: BaseDataBindingHolder<ViewDataBinding>?,
                    item: MultiItemEntity
                ) {
                    val bindView = holder?.dataBinding
                    if (bindView is FunctionSuccessAdBinding) {
                        initNativeAdView(AdUtils.nativeAd, this@FunctionSuccessActivity)?.let {
                            bindView.flFunctionSuccessAd.addView(it)
                        }
                    }
                }
            }
        }
    }

    override fun getLayoutId() = R.layout.function_success_layout

    override fun onLeftClick(view: View) {
        super.onLeftClick(view)
        turnMainActivity()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_DOWN) {
            turnMainActivity()
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun turnMainActivity(){
        startActivity(Intent(this, MainActivity::class.java))
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun initView() {
        setToolBar(leftImage = R.mipmap.icon_hyper_back)
        toolBar?.setLeftTxt(getString(R.string.app_name))
        toolBar?.setToolbarColor(R.color.color_theme)
        toolBar?.setLeftTextSize(16F)
        //设置顶部导航栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.color_theme).init()
        bindingView.lifecycleOwner = this

        mAdapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.AlphaIn)
        mAdapter.isAnimationFirstOnly = false
        bindingView.rvFunctionList.apply {
            layoutManager = LinearLayoutManager(this@FunctionSuccessActivity)
            adapter = mAdapter
        }

        if (AppBean.firstStartAppTime - System.currentTimeMillis() > (24 * 60 * 60 * 1000 * 3)) {
            val dialogFragment = GiveScoreDialogFragment()
            dialogFragment.show(supportFragmentManager, "score")
        }
    }

    override fun loadData(isRefresh: Boolean) {
        dangerList = intent.getSerializableExtra("dangerList") as? MutableList<*>
        functionType = intent.getStringExtra("functionType") ?: ""

        val functionTypeText = when (functionType) {
            GlobalInfo.boostFunction -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                    screenName = AnalyticsUtils.PHONE_BOOST_END_LIST
                )
                getString(R.string.phone_boost_text)
            }
            GlobalInfo.saverFunction -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                    screenName = AnalyticsUtils.BATTERY_END_LIST
                )
                getString(R.string.battery_saver_text)
            }
            GlobalInfo.coolerFunction -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                    screenName = AnalyticsUtils.CPU_END_LIST
                )
                getString(R.string.cooler_success_text)
            }
            GlobalInfo.cleanFunction -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                    screenName = AnalyticsUtils.CLEAN_JUNK_END_LIST
                )
                getString(R.string.clean_success_text)
            }
            GlobalInfo.securityFunction -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                    screenName = AnalyticsUtils.SECURITY_END_LIST
                )
                if (dangerList?.size == null) {
                    getString(R.string.security_success_text)
                } else {
                    when (dangerList?.size) {
                        0 -> {
                            getString(R.string.security_success_text)
                        }
                        1 -> {
                            getString(R.string.security_fail_one_text)
                        }
                        else -> {
                            dangerList?.size.toString() + getString(R.string.security_fail_many_text)
                        }
                    }
                }
            }
            else -> getString(R.string.clean_up_done_text)
        }
        functionSuccessViewModel.getTodoListData(dangerList, functionTypeText)

        AppBean.residentNotificationChoiceType = ""
    }

    override fun onItemClick(v: View?, item: FunctionSuccessBean) {
        when (v?.id) {
            R.id.tv_function_btn -> {
                when (item.functionSuccessItem?.itemName) {
                    GlobalInfo.securityFunction -> {
                        if (AppBean.agreeTrustLook) {
                            startActivity(Intent(this, TrustLookActivity::class.java).apply {
                                putExtra("pageEmbeddingPoint", AnalyticsUtils.END_LIST_VALUE)
                                putExtra("appClick", true)
                            })
                            finish()
                        } else {
                            val dialogFragment = AgreeTrustDialogFragment {
                                AppBean.agreeTrustLook = true
                                startActivity(Intent(this, TrustLookActivity::class.java).apply {
                                    putExtra("pageEmbeddingPoint", AnalyticsUtils.END_LIST_VALUE)
                                    putExtra("appClick", true)
                                })
                                finish()
                            }
                            dialogFragment.show(supportFragmentManager, "trustAgree")
                        }
                    }
                    GlobalInfo.cleanFunction -> {
                        startActivity(Intent(this, CleanCacheActivity::class.java).apply {
                            putExtra("pageEmbeddingPoint", AnalyticsUtils.END_LIST_VALUE)
                            putExtra("appClick", true)
                        })
                    }
                    else -> {
                        item.functionSuccessItem?.itemName?.let { type ->
                            startCleanActivity(this, type, pageEmbeddingPoint = AnalyticsUtils.END_LIST_VALUE, appClick = true)
                            finish()
                        }
                    }
                }
            }
        }
    }
}
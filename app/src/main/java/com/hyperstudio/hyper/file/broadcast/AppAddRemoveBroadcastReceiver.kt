package com.hyperstudio.hyper.file.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.text.Html
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.push.notification.initCleanPushNotification
import com.hyperstudio.hyper.file.unit.AnalyticsUtils

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/1/11 18:38
 */
object AppAddRemoveBroadcastReceiver : BroadcastReceiver() {

    var appAddRemoveBroadcastReceiver: AppAddRemoveBroadcastReceiver? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent != null) {
            when (intent.action) {
                Intent.ACTION_PACKAGE_ADDED -> {
                    var appName = ""
                    try {
                        val packageName = intent.data?.schemeSpecificPart//包名
//                        context?.let {
//                            ToastTool.showToast(
//                                packageName,
//                                it,
//                                1
//                            )
//                        }
                        val packageManager = context?.packageManager
                        val applicationInfo = packageName?.let {
                            packageManager?.getApplicationInfo(
                                it,
                                PackageManager.GET_META_DATA
                            )
                        }

                        appName = if (applicationInfo != null) {
                            packageManager?.getApplicationLabel(applicationInfo)
                                .toString() + " "  //加空格隔开
                        } else {
                            ""
                        }
                    } catch (e: Exception) {
                        appName = ""
                    }


                    val currentTime = System.currentTimeMillis()
                    val lastAddPackagePushTime = AppBean.lastAddPackageNotifyTime
                    //一小时的通知推送间隔
//                    if (currentTime - lastAddPackagePushTime > NotificationConstants.INTERVAL_PACKAGE_PUSH_TIME) {
                        initCleanPushNotification(
                            Html.fromHtml(
                                appName +
                                        ActivityMgr.getContext()
                                            .getString(R.string.html_notification_clean_Installation)
                            ), R.mipmap.function_notification_package_add,
                            AnalyticsUtils.CARD_INSTALL
                        )
                        AppBean.lastAddPackageNotifyTime = System.currentTimeMillis()
//                    }
                }

                Intent.ACTION_PACKAGE_REMOVED -> {
                    val currentTime = System.currentTimeMillis()
                    val lastRemovePackagePushTime = AppBean.lastRemovePackageNotifyTime
                    //通知推送间隔
//                    if (currentTime - lastRemovePackagePushTime > NotificationConstants.INTERVAL_PACKAGE_PUSH_TIME) {
                        initCleanPushNotification(
                            Html.fromHtml(
                                ActivityMgr.getContext()
                                    .getString(R.string.html_notification_clean_uninstall)
                            ), R.mipmap.function_notification_remove,
                            AnalyticsUtils.CARD_UNINSTALL
                        )
                        AppBean.lastRemovePackageNotifyTime = System.currentTimeMillis()
//                    }
                }
            }
        }
    }

}
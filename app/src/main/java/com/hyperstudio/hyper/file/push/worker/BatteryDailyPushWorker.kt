package com.hyperstudio.hyper.file.push.worker

import android.content.Context
import android.text.Html
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
//import com.hyperstudio.hyper.file.push.notification.initBatteryPushNotification

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/12/3 19:52
 */
class BatteryDailyPushWorker(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
//        if (!AppBean.firstStartApp && AppBean.electricQuantity.toInt() <= 20) {
//            initBatteryPushNotification(
//                Html.fromHtml(
//                    ActivityMgr.getContext()
//                        .getString(R.string.html_notification_battery, "25")
//                )
//            )
//        }
        return Result.success()
    }

}
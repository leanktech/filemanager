package com.hyperstudio.hyper.file.widget.activity

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.Build
import android.view.View
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.databinding.AddWidgetLayoutBinding
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import com.hyperstudio.hyper.file.widget.BatteryAppWidget
import com.hyperstudio.hyper.file.widget.CleanAppWidget
import com.hyperstudio.hyper.file.widget.FunctionAppWidget
import com.hyperstudio.hyper.file.widget.dialog.GuidePageDialogFragment
import com.hyperstudio.hyper.file.widget.viewmodel.AddWidgetViewModel
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/4/21 18:27
 */
class AddWidgetActivity : BaseActivity<AddWidgetLayoutBinding>() {

    private val addWidgetViewModel by viewModel<AddWidgetViewModel>()

    override val showToolBar: Boolean
        get() = false

    override fun getLayoutId() = R.layout.add_widget_layout

    override fun initView() {
        bindingView.vm = addWidgetViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        //设置顶部导航栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.color_03178D).init()
    }

    override fun loadData(isRefresh: Boolean) {
        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
            screenName = AnalyticsUtils.WIDGET
        )
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        v?.let {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.ll_widget_back -> {
                        finish()
                    }
                    R.id.tv_function_add_home -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.WIDGET,
                            itemName = AnalyticsUtils.WIDGET_42
                        )
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            val appWidgetManager = getSystemService(AppWidgetManager::class.java)
                            val myProvider = ComponentName(this, FunctionAppWidget::class.java)
                            val pinnedWidgetCallbackIntent =
                                Intent(this, FunctionAppWidget::class.java).putExtra(
                                    "addSuccess",
                                    true
                                )
                            val successCallback = PendingIntent.getBroadcast(
                                this,
                                105,
                                pinnedWidgetCallbackIntent,
                                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                            )
                            //如果不需要创建成功回调，requestPinAppWidget的第三个参数可以为null
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                appWidgetManager?.requestPinAppWidget(
                                    myProvider,
                                    null,
                                    successCallback
                                )
                            }
                        }
//                        val dialogFragment = AddFunctionWidgetDialogFragment()
//                        dialogFragment.show(supportFragmentManager, "addFunction")
                    }
                    R.id.tv_clean_add_home -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.WIDGET,
                            itemName = AnalyticsUtils.WIDGET_411
                        )
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            val appWidgetManager = getSystemService(AppWidgetManager::class.java)
                            val myProvider = ComponentName(this, CleanAppWidget::class.java)
                            val pinnedWidgetCallbackIntent =
                                Intent(this, CleanAppWidget::class.java).putExtra(
                                    "addSuccess",
                                    true
                                )
                            val successCallback = PendingIntent.getBroadcast(
                                this,
                                106,
                                pinnedWidgetCallbackIntent,
                                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                            )
                            //如果不需要创建成功回调，requestPinAppWidget的第三个参数可以为null
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                appWidgetManager?.requestPinAppWidget(
                                    myProvider,
                                    null,
                                    successCallback
                                )
                            }
                        }
//                        val dialogFragment = AddCleanWidgetDialogFragment()
//                        dialogFragment.show(supportFragmentManager, "addClean")
                    }
                    R.id.tv_battery_add_home -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.WIDGET,
                            itemName = AnalyticsUtils.WIDGET_412
                        )
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            val appWidgetManager = getSystemService(AppWidgetManager::class.java)
                            val myProvider = ComponentName(this, BatteryAppWidget::class.java)
                            val pinnedWidgetCallbackIntent =
                                Intent(this, BatteryAppWidget::class.java).putExtra(
                                    "addSuccess",
                                    true
                                )
                            val successCallback = PendingIntent.getBroadcast(
                                this,
                                107,
                                pinnedWidgetCallbackIntent,
                                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                            )
                            //如果不需要创建成功回调，requestPinAppWidget的第三个参数可以为null
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                appWidgetManager?.requestPinAppWidget(
                                    myProvider,
                                    null,
                                    successCallback
                                )
                            }
                        }
//                        val dialogFragment = AddBatteryWidgetDialogFragment()
//                        dialogFragment.show(supportFragmentManager, "addBattery")
                    }
                    R.id.tv_widget_page -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.WIDGET,
                            itemName = AnalyticsUtils.WIDGET_TUTORIAL
                        )
                        val dialogFragment = GuidePageDialogFragment()
                        dialogFragment.show(supportFragmentManager, "widgetPage")
                    }
                }
            }
        }
    }
}
package com.hyperstudio.hyper.file.main.activity

import android.app.PendingIntent
import android.content.ComponentName
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.admob.showAppOpenAd
import com.hyperstudio.hyper.file.admob.showInterstitialAd
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.base_lib.tool.print
import com.hyperstudio.hyper.file.databinding.ActivityMainBinding
import com.hyperstudio.hyper.file.main.fragment.FileFragment
import com.hyperstudio.hyper.file.main.fragment.ToDoFragment
import com.hyperstudio.hyper.file.main.fragment.ToolsFragment
import com.hyperstudio.hyper.file.main.viewmodel.MainViewModel
import com.hyperstudio.hyper.file.push.notification.*
import com.hyperstudio.hyper.file.splash.activity.SplashActivity
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import com.hyperstudio.hyper.file.unit.getPermissionForApp
import com.hyperstudio.hyper.file.widget.dialog.ShortcutDialogFragment
import org.koin.android.viewmodel.ext.android.viewModel


/**
 * @Name: MainActivity
 * @Description: Home页面
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */

class MainActivity : BaseActivity<ActivityMainBinding>() {

    companion object {
        var firstStartBoostAnimation = true
    }

    private val mainViewModel by viewModel<MainViewModel>()

    //加载FragmentList
    private val fragList = mutableListOf<Fragment>(ToDoFragment(), ToolsFragment(), FileFragment())

    private var index = 0

    private var fragHashSet: HashSet<Fragment> = hashSetOf()

    private var firstStart = false

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun initView() {

        toolBar?.visibility = View.GONE

        if(AppBean.checkMemoryUsage){
            AppBean.checkMemoryUsage = false
        }

        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
            screenName = AnalyticsUtils.HOME
        )

        setTitleContent(leftImg = 0, titleName = getString(R.string.app_name))

        bindingView.vm = mainViewModel
        bindingView.lifecycleOwner = this

        ImmersionBar.with(this).statusBarColor(R.color.color_theme).init()

        selectDefaultFragment(0, true)
        bindingView.navigationMain.setOnNavigationItemSelectedListener {
            //通过Navigation点击区域刷新Vp显示页面
            when (it.itemId) {
                R.id.main_clean -> {
                    index = 0
                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                        screenName = AnalyticsUtils.HOME
                    )
                    selectDefaultFragment(index, false)

                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                        screenName = AnalyticsUtils.HOME
                    )
                }
                R.id.main_work -> {
                    index = 1
                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                        screenName = AnalyticsUtils.TOOLS
                    )
                    selectDefaultFragment(index, false)

                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                        screenName = AnalyticsUtils.TOOLS
                    )
                }
                R.id.main_file -> {
                    getPermissionForApp(this) { getPermission ->
                        if(getPermission){
                            AnalyticsUtils.appPageBuriedPoint(
                                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                screenName = AnalyticsUtils.CLEAN_JUNK_DIALOG_ACCESS,
                                itemName = AnalyticsUtils.DIALOG_ACCESS_ALLOW
                            )
                        }else{
                            AnalyticsUtils.appPageBuriedPoint(
                                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                screenName = AnalyticsUtils.CLEAN_JUNK_DIALOG_ACCESS,
                                itemName = AnalyticsUtils.DIALOG_ACCESS_DO_NOT_ALLOW
                            )
                        }
                        index = 2
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.FILES
                        )
                        selectDefaultFragment(index, false)

                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                            screenName = AnalyticsUtils.FILES
                        )
                    }
                }
            }
            return@setOnNavigationItemSelectedListener false
        }
    }

    override fun loadData(isRefresh: Boolean) {
        if (AppBean.firstStartApp) {
            bindingView.clMainViewBag.postDelayed({
                val dialogFragment = ShortcutDialogFragment {
                    if (it) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            addNewDesktopShotCut()
                        } else {
                            addDeskTopShortCutUnderO()
                        }
                    }
                    showAd()
                }

                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                    screenName = AnalyticsUtils.DIALOG_WIDGET
                )

                dialogFragment.show(supportFragmentManager, "shortCut")

                firstStart = AppBean.firstStartApp
                AppBean.firstStartApp = false

            }, 100)
        } else {
            showAd()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        showAd()
    }

    override fun onResume() {
        super.onResume()
        (fragList[0] as? ToDoFragment)?.notifyAdapter()
    }

    /**
     * 切换frag方法
     */
    fun selectDefaultFragment(index: Int, isInit: Boolean) {
        try {
            supportFragmentManager.beginTransaction().apply {
                fragList.forEach {
                    hide(it)
                    if (it == fragList[index]) {
                        if (fragHashSet.add(it)) {
                            add(bindingView.flMain.id, it)
                        }
                        show(it)
                    }
                }
                commitAllowingStateLoss()
            }

            bindingView.navigationMain.menu.getItem(index).isChecked = true
            //MainActivity重新加载时，如果在Tools页面时底部item可能不会选中Home，
            //在此做一下容错（比如跳转到权限页面不开启权限再返回页面时）
            if (isInit) {
                Handler(Looper.getMainLooper()).postDelayed(
                    {
                        bindingView.navigationMain.menu.getItem(index).isChecked = true
                    }, 300
                )
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    private fun showAd() {
        if (AppBean.residentNotificationChoiceType.isNotEmpty()) {
            //设置顶部导航栏颜色
            bindingView.clMainViewBag.visibility = View.VISIBLE
            bindingView.clMainViewBag.postDelayed({
                (fragList[0] as? ToDoFragment)?.getResidentNotificationType()
            }, 100)
        } else {
            when (AppBean.appStartUpType) {
                GlobalInfo.HOT_START_TAG -> {
                    if (AdUtils.appOpenAd == null) {
                        bindingView.clMainViewBag.visibility = View.VISIBLE
                    } else {
                        ImmersionBar.with(this).statusBarColor(R.color.white).init()
                        bindingView.clMainViewBag.visibility = View.GONE

                        showAppOpenAd(this) {
                            ImmersionBar.with(this).statusBarColor(R.color.color_03178D)
                                .init()
                            if (it == GlobalInfo.AD_DISMISSED) {
                                bindingView.clMainViewBag.visibility = View.VISIBLE
                                "showAppOpenAd".print()
                            }
                        }
                    }
                }
                GlobalInfo.COLD_BOOT_TAG -> {
                    //设置顶部导航栏颜色
                    bindingView.clMainViewBag.visibility = View.VISIBLE

                    if(!firstStart){
                        showInterstitialAd(this) {

                        }
                    }
                }
            }
        }
    }

    private fun addDeskTopShortCutUnderO() {
        //创建Intent对象
        val shortcutIntent = Intent()
        //设置点击快捷方式，进入指定的Activity
        shortcutIntent.component =
            ComponentName(
                ActivityMgr.getContext().packageName,
                "com.hyperstudio.hyper.file.splash.activity.SplashActivity"
            )
        //给Intent添加 对应的flag
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS or Intent.FLAG_ACTIVITY_NEW_TASK)
        //传递参数
//        shortcutIntent.putExtra("key1", "value1")
//        shortcutIntent.putExtra("key2", "value2")

        val resultIntent = Intent()
        //设置Action
        resultIntent.action = "com.android.launcher.action.INSTALL_SHORTCUT"
        //设置快捷方式的图标
        resultIntent.putExtra(
            Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
            Intent.ShortcutIconResource.fromContext(
                this,
                R.mipmap.app_icon
            )
        )
        //设置快捷方式的名称
        resultIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "shortCut")
        //设置启动的Intent
        resultIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent)

        //发送广播，通知系统创建桌面快捷方式
        sendBroadcast(resultIntent)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    fun addNewDesktopShotCut() {
        val shortcutManager = getSystemService(ShortcutManager::class.java)

        if (shortcutManager!!.isRequestPinShortcutSupported) {
            val intent = Intent(this, SplashActivity::class.java)
            intent.setAction(Intent.ACTION_VIEW)  //必须设置，可以直接使用Intent.ACTION_VIEW
            intent.putExtra("key", "value") //传递参数
            val pinShortcutInfo = ShortcutInfo.Builder(this, "shortcutId")
                .setShortLabel("shortCut")
                .setLongLabel("desktopShotCut")
                .setIcon(Icon.createWithResource(this, R.mipmap.app_icon))
                .setIntent(intent)
                .build()
            val pinnedShortcutCallbackIntent =
                shortcutManager.createShortcutResultIntent(pinShortcutInfo)
            val successCallback = PendingIntent.getBroadcast(
                this, /* request code */ 0,
                pinnedShortcutCallbackIntent, /* flags */ PendingIntent.FLAG_MUTABLE
            )
            shortcutManager.requestPinShortcut(pinShortcutInfo, successCallback.intentSender)
        }
    }

}
package com.hyperstudio.hyper.file.clean.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.admob.showInterstitialAd
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.base_lib.tool.setVisibilityAnimation
import com.hyperstudio.hyper.file.clean.viewmodel.TargetedCleanViewModel
import com.hyperstudio.hyper.file.complete.activity.FunctionSuccessActivity
import com.hyperstudio.hyper.file.databinding.TargetedCleanLayoutBinding
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/3/28 17:12
 */
class TargetedCleanActivity : BaseActivity<TargetedCleanLayoutBinding>() {

    private val targetedCleanViewModel by viewModel<TargetedCleanViewModel>()

    var cleanType = ""

    override val showToolBar: Boolean
        get() = false

    override fun getLayoutId() = R.layout.targeted_clean_layout

    override fun initView() {
        bindingView.vm = targetedCleanViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        //设置顶部导航栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.color_152BA1).init()
    }

    @SuppressLint("SetTextI18n")
    override fun loadData(isRefresh: Boolean) {
        cleanType = intent.getStringExtra("cleanType").toString()
        bindingView.tvTargetedBack.text = "$cleanType clean up"
        when (cleanType) {
            GlobalInfo.targetedTiktokFunction -> {
                bindingView.ivTargetedIcon.setImageResource(R.mipmap.tiktok_clean_up)
                bindingView.tvCleanUpText.text = getString(R.string.tiktok_cleaning_up_text)
            }
            GlobalInfo.targetedWhatsAppFunction -> {
                bindingView.ivTargetedIcon.setImageResource(R.mipmap.whatsapp_clean_up)
                bindingView.tvCleanUpText.text = getString(R.string.whats_app_cleaning_up_text)
            }
            GlobalInfo.targetedGooglePlayFunction -> {
                bindingView.ivTargetedIcon.setImageResource(R.mipmap.google_play_clean_up)
                bindingView.tvCleanUpText.text = getString(R.string.google_play_cleaning_up_text)
            }
            GlobalInfo.targetedYouTubeFunction -> {
                bindingView.ivTargetedIcon.setImageResource(R.mipmap.youtube_clean_up)
                bindingView.tvCleanUpText.text = getString(R.string.youtube_cleaning_up_text)
            }
            GlobalInfo.targetedLineFunction -> {
                bindingView.ivTargetedIcon.setImageResource(R.mipmap.line_clean_up)
                bindingView.tvCleanUpText.text = getString(R.string.line_cleaning_up_text)
            }
        }
        startFun {
            cleanCacheSuccess()
        }
    }

    private fun cleanCacheSuccess() {
        targetedCleanViewModel.launchUI {

            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                contentType = AppBean.contentType,
                itemCategory = AppBean.itemCategory,
                itemId = AppBean.pushEmbeddingPoint
            )

            when (cleanType) {
                GlobalInfo.targetedTiktokFunction -> {

                    intent.getStringExtra("pageEmbeddingPoint")?.let { pageEmbeddingPoint ->
                        AnalyticsUtils.appFunctionBuriedPoint(
                            eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                            contentType = "TikTok",
                            itemCategory = "Page",
                            pageEmbeddingPoint
                        )
                    }

                }
                GlobalInfo.targetedWhatsAppFunction -> {

                    intent.getStringExtra("pageEmbeddingPoint")?.let { pageEmbeddingPoint ->
                        AnalyticsUtils.appFunctionBuriedPoint(
                            eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                            contentType = "WhatsApp",
                            itemCategory = "Page",
                            pageEmbeddingPoint
                        )
                    }

                }
                GlobalInfo.targetedGooglePlayFunction -> {

                    intent.getStringExtra("pageEmbeddingPoint")?.let { pageEmbeddingPoint ->
                        AnalyticsUtils.appFunctionBuriedPoint(
                            eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                            contentType = "GooglePlay",
                            itemCategory = "Page",
                            pageEmbeddingPoint
                        )
                    }

                }
                GlobalInfo.targetedYouTubeFunction -> {

                    intent.getStringExtra("pageEmbeddingPoint")?.let { pageEmbeddingPoint ->
                        AnalyticsUtils.appFunctionBuriedPoint(
                            eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                            contentType = "Youtube",
                            itemCategory = "Page",
                            pageEmbeddingPoint
                        )
                    }

                }
            }

            bindingView.clCleaningUp.setVisibilityAnimation(View.GONE, 1000)
            delay(1000)
            bindingView.rlSuccessViewBag.visibility = View.VISIBLE

            val moveAnimTitle: Animation = AnimationUtils.loadAnimation(
                this@TargetedCleanActivity, R.anim.move_anim_down_to_up
            ).apply {
                duration = 650
            }
            bindingView.tvFunctionSuccessTitle.visibility = View.VISIBLE
            bindingView.tvFunctionSuccessTitle.startAnimation(moveAnimTitle)
            delay(650)

            val moveAnimSuccess: Animation = AnimationUtils.loadAnimation(
                this@TargetedCleanActivity, R.anim.move_anim_down_to_up
            ).apply {
                duration = 650
            }
            bindingView.tvFunctionSuccessText.visibility = View.VISIBLE
            bindingView.tvFunctionSuccessText.startAnimation(moveAnimSuccess)
            delay(650)

            showInterstitialAd(this@TargetedCleanActivity) {
                startActivity(Intent(
                    this@TargetedCleanActivity, FunctionSuccessActivity::class.java
                ).apply {
                    putExtra("functionType", cleanType)
                })
                finish()
            }
        }
    }

    private fun startFun(callBack: () -> Unit) {
        targetedCleanViewModel.launchUI {

            //load Ad
            val appClick = intent.getBooleanExtra("appClick", false)
            if (appClick) {
                AdUtils.loadAdObject(
                    this@TargetedCleanActivity,
                    interstitialId = null,
                    nativeId = GlobalInfo.ADMOB_TARGETED_ID_NATIVE_h
                )
            } else {
                AdUtils.loadAdObject(
                    this@TargetedCleanActivity,
                    GlobalInfo.ADMOB_TARGETED_ID_INTERSTITIAL_h,
                    GlobalInfo.ADMOB_TARGETED_ID_NATIVE_h
                )
            }

            targetedCleanViewModel.targetedCleanUp(cleanType)
            delay(1000)

            targetedCleanViewModel.asyncSimpleAsync {
                var loading = true

                var loadingValue = 0

                var maxTime = 0

                var loadInterstitialAdB = false

                var loadInterstitialAdBTime = 0

                var loadNativeAdB = false

                var loadNativeAdBTime = 0

                while (loading) {
                    val loadingSuccess = if (appClick) {
                        AdUtils.getNativeAdObjectSuccess() && loadingValue >= 300
                    } else {
                        AdUtils.getAdObjectSuccess() && loadingValue >= 300
                    }

                    if (loadingSuccess || maxTime > 10000) {
                        loading = false
                        callBack()
                    } else {
                        if (loadingValue < 300) {
                            loadingValue++
                        }
                        if (AdUtils.interstitialErrorMessage != null && AdUtils.interstitialErrorMessage?.isNotEmpty() == true) {
                            if (!loadInterstitialAdB) {
                                AdUtils.loadInterstitialAd(
                                    this@TargetedCleanActivity,
                                    GlobalInfo.ADMOB_TARGETED_ID_INTERSTITIAL_b
                                )

                                loadInterstitialAdBTime = maxTime
                                loadInterstitialAdB = true
                            } else {
                                if (maxTime > 8000 || (maxTime - loadInterstitialAdBTime > 6000 && AdUtils.interstitialErrorMessage != null)) {
                                    loading = false
                                    AdUtils.interstitialErrorMessage = null
                                    callBack()
                                }
                            }
                        }

                        if (AdUtils.nativeErrorMessage != null && AdUtils.nativeErrorMessage?.isNotEmpty() == true) {
                            if (!loadNativeAdB) {
                                AdUtils.loadNativeAd(
                                    this@TargetedCleanActivity,
                                    GlobalInfo.ADMOB_TARGETED_ID_NATIVE_b
                                )
                                loadNativeAdB = true
                                loadNativeAdBTime = maxTime
                            } else {
                                if (maxTime > 30000 || (maxTime - loadNativeAdBTime > 6000 && AdUtils.nativeErrorMessage != null)) {
                                    loading = false
                                    AdUtils.interstitialErrorMessage = null
                                    callBack()
                                }
                            }
                        }
                        delay(10)
                        maxTime += 10
                    }
                }
            }.run { }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        v?.let {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.ll_targeted_back -> {
                        finish()
                    }
                }
            }
        }
    }
}
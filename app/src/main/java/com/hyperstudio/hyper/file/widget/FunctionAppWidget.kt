package com.hyperstudio.hyper.file.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.widget.RemoteViews
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.base_lib.tool.showToast
import com.hyperstudio.hyper.file.splash.activity.SplashActivity
import com.hyperstudio.hyper.file.unit.SplashDataObject
import com.hyperstudio.hyper.file.widget.service.ClockService
import java.io.File


/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/4/18 14:49
 */
class FunctionAppWidget : AppWidgetProvider() {

    var firstStartWidget = true

    var updateBattery = false

    var useSizeGb = ""
    var totalSizeGb = ""
    var ramPercent = ""

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray?
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        if (firstStartWidget) {
            //启动ClockService
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context?.startForegroundService(Intent(context, ClockService::class.java))
            } else {
                context?.startService(Intent(context, ClockService::class.java))
            }
            firstStartWidget = false
        }

        val widgetFunctionView =
            RemoteViews(ActivityMgr.getContext().packageName, R.layout.widget_function_layout)

        /**
         * 点击事件更新*/
        //boost
        val boostIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
        boostIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
        boostIntent.putExtra("functionType", GlobalInfo.boostFunction)
        boostIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
        val pendingBoostIntent = PendingIntent.getActivity(
            ActivityMgr.getContext(),
            100,
            boostIntent,
            PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        widgetFunctionView.setOnClickPendingIntent(
            R.id.widget_function_boost_btn,
            pendingBoostIntent
        )

        //clean
        val cleanIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
        cleanIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
        cleanIntent.putExtra("functionType", GlobalInfo.cleanFunction)
        cleanIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
        val pendingCleanIntent = PendingIntent.getActivity(
            ActivityMgr.getContext(),
            101,
            cleanIntent,
            PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        widgetFunctionView.setOnClickPendingIntent(
            R.id.widget_function_clean_btn,
            pendingCleanIntent
        )

        //battery
        val batteryIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
        batteryIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
        batteryIntent.putExtra("functionType", GlobalInfo.saverFunction)
        batteryIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
        val pendingBatteryIntent = PendingIntent.getActivity(
            ActivityMgr.getContext(),
            102,
            batteryIntent,
            PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        widgetFunctionView.setOnClickPendingIntent(
            R.id.widget_function_battery_btn,
            pendingBatteryIntent
        )

        if (!updateBattery) {
            /**
             * 内部存储占用更新*/
            //获取内部存储根目录
            val path: File = Environment.getDataDirectory()
            //系统的空间描述类
            val stat = StatFs(path.path)
            //每个区块占字节数
            val blockSize = stat.blockSize.toLong()
            //区块总数
            val totalBlocks = stat.blockCount.toLong()
            val totalExternal = totalBlocks * blockSize

            //获取手机内部可用空间大小
            val availableBlocks = stat.availableBlocks.toLong()
            val canUseExternal = availableBlocks * blockSize

            //总共内存大小 GB为单位
            var totalSize = totalExternal / 1024
            if (totalSize >= 1024) {
                totalSize /= 1024
                if (totalSize >= 1024) {
                    totalSize /= 1024
                }
            }

            //总共内存大小 GB为单位
            var canUseSizeGb = canUseExternal / 1024
            if (canUseSizeGb >= 1024) {
                canUseSizeGb /= 1024
                if (canUseSizeGb >= 1024) {
                    canUseSizeGb /= 1024
                }
            }

            useSizeGb = (totalSize - canUseSizeGb).toString()
            totalSizeGb = totalSize.toString()

            ramPercent = context?.let {
                SplashDataObject.getMemoryUsedPercentage(it)
            } ?: ""
        }

        widgetFunctionView.setTextViewText(
            R.id.widget_function_clean_btn,
            useSizeGb + "GB" + "/" + totalSizeGb + "GB"
        )

        /**
         * 占用缓存更新*/
        context?.let {
            widgetFunctionView.setTextViewText(
                R.id.widget_function_boost_btn,
                "$ramPercent% RAM"
            )
        }

        val batteryStatus = context?.applicationContext?.registerReceiver(
            this,
            IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        )

        //Battery Level
        val level = batteryStatus?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)

        //Max Battery Level
        val scale = batteryStatus?.getIntExtra(BatteryManager.EXTRA_SCALE, -1)

        var showBattery = 100
        val batteryPct = scale?.toFloat()?.let { level?.div(it) }
        if (batteryPct != null) {
            showBattery = (batteryPct * 100).toInt()
        }

        widgetFunctionView.setTextViewText(
            R.id.widget_function_battery_btn,
            "$showBattery%"
        )
        updateBattery = false

        appWidgetManager?.updateAppWidget(appWidgetIds, widgetFunctionView)
    }

    override fun onEnabled(context: Context) {
        super.onEnabled(context)
    }


    override fun onDisabled(context: Context) {
        super.onDisabled(context)
        //关闭ClockService
        context.stopService(Intent(context, ClockService::class.java))
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)

        val addSuccess = intent?.getBooleanExtra("addSuccess", false)
        if (addSuccess == true) {
            context?.getString(R.string.widget_dialog_bottom_hint)?.showToast()
        }

        if (intent?.action == Intent.ACTION_BATTERY_CHANGED) {

            //get Bundle
            val extras = intent?.extras

            //if extras
            if (extras != null) {
                val appWidgetManager = AppWidgetManager.getInstance(context)
                val thisAppWidget =
                    context?.packageName?.let {
                        ComponentName(
                            it,
                            FunctionAppWidget::class.java.name
                        )
                    }
                val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget)

                updateBattery = true
                //call the onUpdate
                onUpdate(context, appWidgetManager, appWidgetIds)
            }
        }
    }
}
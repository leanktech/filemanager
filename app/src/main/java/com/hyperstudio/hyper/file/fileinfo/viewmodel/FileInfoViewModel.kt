package com.hyperstudio.hyper.file.fileinfo.viewmodel

import android.database.Cursor
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.databinding.ObservableArrayList
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.main.model.FileListItemBean
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File


/**
 * @Name: FileInfoViewModel
 * @Description: FileViewModel
 * @Author: even@leanktech.com
 * @Date: 2021/9/29 18:11
 */
class FileInfoViewModel : BaseViewMode() {

    var fileNameList = ObservableArrayList<FileListItemBean>()

    fun getMediaStoreData(type: String, callBack: (String) -> Unit) {
        launchUI {
            val listItems: MutableList<FileListItemBean> = mutableListOf()
            var cursor: Cursor?
            withContext(Dispatchers.IO) {
                val names: MutableList<String> = mutableListOf()
                val fileNames: MutableList<String> = mutableListOf()
                cursor = when (type) {
                    GlobalInfo.imageFunction -> ActivityMgr.getContext().contentResolver.query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        null,
                        null,
                        null,
                        null
                    )
                    GlobalInfo.videoFunction -> ActivityMgr.getContext().contentResolver.query(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        null,
                        null,
                        null,
                        null
                    )
                    GlobalInfo.musicFunction -> ActivityMgr.getContext().contentResolver.query(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        null,
                        null,
                        null,
                        MediaStore.Audio.Media.DEFAULT_SORT_ORDER
                    )
                    else -> null
                }

                while (cursor?.moveToNext() == true) {
                    //获取名称
                    val name =
                        when (type) {
                            GlobalInfo.imageFunction -> cursor?.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME)
                                ?.let {
                                    cursor?.getString(it)
                                }
                            GlobalInfo.videoFunction -> cursor?.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME)
                                ?.let {
                                    cursor?.getString(it)
                                }
                            GlobalInfo.musicFunction -> cursor?.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)
                                ?.let {
                                    cursor?.getString(it)
                                }
                            else -> null
                        }
                    //获取uri
                    val data =
                        when (type) {
                            GlobalInfo.imageFunction -> cursor?.getColumnIndex(MediaStore.Images.Media.DATA)
                                ?.let {
                                    cursor?.getBlob(it)
                                }
                            GlobalInfo.videoFunction -> cursor?.getColumnIndex(MediaStore.Video.Media.DATA)
                                ?.let {
                                    cursor?.getBlob(it)
                                }
                            GlobalInfo.musicFunction -> cursor?.getColumnIndex(MediaStore.Audio.Media.DATA)
                                ?.let {
                                    cursor?.getBlob(it)
                                }
                            else -> null
                        }

                    name?.let { names.add(it) }
                    data?.let { fileNames.add(String(it, 0, it.size - 1)) }
                }

                cursor?.close()

                names.forEach {
                    listItems.add(FileListItemBean().apply {
                        fName = it
                        when (type) {
                            GlobalInfo.imageFunction -> fileImg = R.mipmap.icon_filetype_image
                            GlobalInfo.videoFunction -> fileImg = R.mipmap.icon_filetype_video
                            GlobalInfo.musicFunction -> fileImg = R.mipmap.icon_filetype_music
                        }
                    })
                }
            }

            if (listItems.isEmpty()) {
                callBack("empty")
            } else {
                fileNameList.clear()
                fileNameList.addAll(listItems)
            }
        }
    }

    fun initTxtList(files: Array<File>?) {
        asyncSimpleAsync {
            val dataList = withContext(Dispatchers.IO) {
                val txtList = mutableListOf<FileListItemBean>()
                getFileName(files).let {
                    txtList.addAll(it)
                }
                txtList
            }
            fileNameList.addAll(dataList)
        }.run { }
    }

    //读取指定目录下的所有TXT文件的文件名
    private fun getFileName(files: Array<File>?): MutableList<FileListItemBean> {
        var str = ""
        val txtList = mutableListOf<FileListItemBean>()
        if (files != null) {    // 先判断目录是否为空，否则会报空指针
            for (file in files) {
                if (file.isDirectory) { //检查此路径名的文件是否是一个目录(文件夹)
//                    getFileName(file.listFiles())
                } else {
                    val fileName = file.name
                    if (fileName.endsWith(".txt")) {
                        txtList.add(FileListItemBean().apply {
                            fName = fileName
                            fileImg = R.mipmap.icon_filetype_text
                        })
                        val s = fileName.substring(0, fileName.lastIndexOf("."))
                        str += """
                        ${fileName.substring(0, fileName.lastIndexOf("."))}
                       
                        """.trimIndent()
                    }
                }
            }
        }
        return txtList
    }

    fun getDownloadList(callBack: (String) -> Unit) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val fileList =
                ActivityMgr.getContext().getExternalFilesDirs(Environment.DIRECTORY_DOWNLOADS)
            val downloadList = mutableListOf<FileListItemBean>()
            fileList?.forEach {
                downloadList.add(FileListItemBean().apply {
                    if(it?.name != null){
                        fName = it.name
                    }
                    fileImg = R.mipmap.icon_filetype_download
                })
            }
//            if (fileList == null || fileList.isEmpty()) {
                callBack("empty")
//            }
        } else {
            callBack("empty")
        }
    }
}
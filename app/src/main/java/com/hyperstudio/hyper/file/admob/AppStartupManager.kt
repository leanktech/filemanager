package com.hyperstudio.hyper.file.admob

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner

/**
 * @Name: AppStartupManager
 * @Description: 冷启动热启动 相关操作
 * @Author: even@leanktech.com
 * @Date: 2021/10/18 18:07
 */

@SuppressLint("StaticFieldLeak")
object AppStartupManager : LifecycleObserver,
    Application.ActivityLifecycleCallbacks {

    //计当前application的加载状态
    private var mFinalCount = 0

    //Act栈最上面的类 一般情况下为Splash
    private var currentActivity: Activity? = null
    private var mStartUpApplication: Application? = null


    fun init(myApplication: Application) {
        mStartUpApplication = myApplication
        mStartUpApplication?.registerActivityLifecycleCallbacks(this)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    /**
     * 监听前台事件
     * 需要从splash开始往下走业务
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStartToSplash() {
//        if (isForegroundMethodTemp && AdUtils.appOpenAd == null && AppBean.residentNotificationChoiceType.isEmpty()) {
//            val context = ActivityMgr.getContext()
//                context.startActivity(Intent(context, SplashActivity::class.java).apply {
//                    flags = FLAG_ACTIVITY_NEW_TASK
//                    putExtra("appStartUp", GlobalInfo.HOT_START_TAG)
//                })
//                isForegroundMethodTemp = false
//        }
    }

    /**
     * 临时变量 记录是否已经退到后台了
     */
    private var isForegroundMethodTemp = false

    /**
     * 实现ActivityLifecycleCallbacks
     */
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
    }

    override fun onActivityStarted(activity: Activity) {
        mFinalCount++
        currentActivity = activity
    }

    override fun onActivityStopped(activity: Activity) {
        mFinalCount--
        //如果mFinalCount ==0，说明是前台到后台
        if (mFinalCount == 0) {
            isForegroundMethodTemp = true
        }
    }

    override fun onActivityResumed(activity: Activity) {
        currentActivity = activity
    }

    override fun onActivityPaused(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
    }

    override fun onActivityDestroyed(activity: Activity) {
        currentActivity = null
    }

}
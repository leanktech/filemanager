package com.hyperstudio.hyper.file.unit

import android.app.ActivityManager
import android.app.job.JobScheduler
import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.TextUtils
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat


/**
 * @Description: 判断某一Service是否正在运行
 * author: Rhett
 * time: 2021/12/24
 */
object ServiceUtil {

    /**
     * 判断某个Service是否正在运行
     * */
    fun isServiceRunning(context: Context, ServiceName: String?): Boolean {
        if (TextUtils.isEmpty(ServiceName)) {
            return false
        }
        val myManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningService =
            myManager.getRunningServices(1000) as ArrayList<ActivityManager.RunningServiceInfo>
        Log.i("服务运行size：", "" + runningService.size)
        for (i in 0 until runningService.size) {
            Log.i("服务运行1：", "" + runningService[i].service.className)
            if (runningService[i].service.className.equals(ServiceName)) {
                return true
            }
        }
        return false
    }

    /**
     * 判断某个JobService是否运行中
     * */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun isJobServiceOn(context: Context, JOB_ID: Int): Boolean {
        val scheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        var hasBeenScheduled = false
        Log.i("服务运行size：", "" + scheduler.allPendingJobs.size)
        for (jobInfo in scheduler.allPendingJobs) {
            Log.i("服务运行：", "" + jobInfo.id)
            if (jobInfo.id == JOB_ID) {
                hasBeenScheduled = true
                break
            }
        }
        return hasBeenScheduled
    }

    /**
     * 启动Service适配
     */
    fun startServiceForVersion(context: Context, clazz: Class<*>) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                context.startService(Intent(context, clazz))
            } else {
                ContextCompat.startForegroundService(context, Intent(context, clazz))
            }
        } catch (e: Exception) {

        }
    }
}
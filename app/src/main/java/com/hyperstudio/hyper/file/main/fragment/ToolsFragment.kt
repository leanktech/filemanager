package com.hyperstudio.hyper.file.main.fragment

import android.content.Intent
import android.view.View
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.fragment.BaseFragment
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.base_lib.tool.showToast
import com.hyperstudio.hyper.file.clean.activity.CleanCacheActivity
import com.hyperstudio.hyper.file.clean.activity.TargetedCleanActivity
import com.hyperstudio.hyper.file.databinding.FragToolsBinding
import com.hyperstudio.hyper.file.fileinfo.activity.FileInfoActivity
import com.hyperstudio.hyper.file.h5.H5Activity
import com.hyperstudio.hyper.file.main.viewmodel.ToolsViewModel
import com.hyperstudio.hyper.file.trustlook.activity.TrustLookActivity
import com.hyperstudio.hyper.file.trustlook.dialog.AgreeTrustDialogFragment
import com.hyperstudio.hyper.file.unit.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Name: WorkFragment
 * @Description: Tools模块
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */

class ToolsFragment : BaseFragment<FragToolsBinding>() {

    private val toolsViewModel by viewModel<ToolsViewModel>()

    override fun getLayoutId() = R.layout.frag_tools

    override fun initView() {
        bindingView.vm = toolsViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this

        AnalyticsUtils.openSystemResidentBoost(
            AnalyticsUtils.APP_VIEW_EVENT_NAME, AnalyticsUtils.TOOLS
        )

    }

    override fun loadData(isRefresh: Boolean) {
        getTargetedApp()

        if (!AppBean.tiktokAlive) {
            bindingView.llTiktokTargetedClean.visibility = View.GONE
        }
        if (getPhoneCountry(context, "JP", true)) {
            if (!AppBean.whatsAppAlive) {
                bindingView.llWhatsappTargetedClean.visibility = View.GONE
            }
        } else {
            bindingView.llWhatsappTargetedClean.visibility = View.GONE
        }
        if (!AppBean.googlePlayAlive) {
            bindingView.llGooglePlayTargetedClean.visibility = View.GONE
        }
        if (!AppBean.youTubeAlive) {
            bindingView.llYoutubeTargetedClean.visibility = View.GONE
        }
        if (getPhoneCountry(context, "JP") || getPhoneCountry(
                context, "ID"
            ) || getPhoneCountry(context, "MY") || getPhoneCountry(
                context, "PH"
            ) || getPhoneCountry(context, "SG") || getPhoneCountry(context, "TH")
        ) {
            if (!AppBean.lineAlive) {
                bindingView.llLineTargetedClean.visibility = View.GONE
            }
        } else {
            bindingView.llLineTargetedClean.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        v?.let {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.tv_clean_ram -> {
//                        AnalyticsUtils.appPageBuriedPoint(
//                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
//                            screenName = AnalyticsUtils.TOOLS,
//                            itemName = AnalyticsUtils.TOOLS_BOOST_NOW_BUTTON
//                        )
//                        context?.let {
//                            startCleanActivity(
//                                it,
//                                GlobalInfo.boostFunction,
//                                pageEmbeddingPoint = AnalyticsUtils.TOOLS_VALUE,
//                                appClick = true
//                            )
//                        }
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_CLEAN_NOW_BUTTON
                        )

                        AnalyticsUtils.appFunctionBuriedPoint(
                            eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                            contentType = "CleanJunk",
                            itemCategory = "Page",
                            AnalyticsUtils.TOOLS_VALUE
                        )

                        startActivity(Intent(context, CleanCacheActivity::class.java).apply {
                            putExtra("pageEmbeddingPoint", AnalyticsUtils.TOOLS_VALUE)
                            putExtra("appClick", true)
                        })
                    }
                    R.id.tv_tools_clean -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_CLEAN
                        )

                        AnalyticsUtils.appFunctionBuriedPoint(
                            eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                            contentType = "CleanJunk",
                            itemCategory = "Page",
                            AnalyticsUtils.TOOLS_VALUE
                        )

                        startActivity(Intent(context, CleanCacheActivity::class.java).apply {
                            putExtra("pageEmbeddingPoint", AnalyticsUtils.TOOLS_VALUE)
                            putExtra("appClick", true)
                        })
                    }
                    R.id.tv_tools_saver -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_BATTERY
                        )
                        context?.let {
                            startCleanActivity(
                                it,
                                GlobalInfo.saverFunction,
                                pageEmbeddingPoint = AnalyticsUtils.TOOLS_VALUE,
                                appClick = true
                            )
                        }
                    }
                    R.id.tv_tools_cpu -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_CPU
                        )
                        context?.let {
                            startCleanActivity(
                                it,
                                GlobalInfo.coolerFunction,
                                pageEmbeddingPoint = AnalyticsUtils.TOOLS_VALUE,
                                appClick = true
                            )
                        }
                    }
                    R.id.tv_tools_safe -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_VIRUS
                        )

                        AnalyticsUtils.appFunctionBuriedPoint(
                            eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                            contentType = "Virus",
                            itemCategory = "Page",
                            AnalyticsUtils.TODO_VALUE
                        )

                        if (AppBean.agreeTrustLook) {
                            startActivity(Intent(context, TrustLookActivity::class.java).apply {
                                putExtra("pageEmbeddingPoint", AnalyticsUtils.TOOLS_VALUE)
                                putExtra("appClick", true)
                            })
                        } else {
                            val dialogFragment = AgreeTrustDialogFragment {
                                AppBean.agreeTrustLook = true
                                startActivity(Intent(context, TrustLookActivity::class.java).apply {
                                    putExtra("pageEmbeddingPoint", AnalyticsUtils.TOOLS_VALUE)
                                    putExtra("appClick", true)
                                })
                            }
                            dialogFragment.show(childFragmentManager, "trustAgree")
                        }
                    }
                    R.id.tv_category_image -> {
                        context?.let {
                            getPermissionForApp(it) { getPermission ->
                                setPoint(getPermission)
                                AnalyticsUtils.appPageBuriedPoint(
                                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                    screenName = AnalyticsUtils.TOOLS,
                                    itemName = AnalyticsUtils.TOOLS_IMAGE
                                )
                                startActivity(Intent(context, FileInfoActivity::class.java).apply {
                                    putExtra("fileType", GlobalInfo.imageFunction)
                                })
                            }
                        }

                    }
                    R.id.tv_category_video -> {
                        context?.let {
                            getPermissionForApp(it) { getPermission ->
                                setPoint(getPermission)
                                AnalyticsUtils.appPageBuriedPoint(
                                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                    screenName = AnalyticsUtils.TOOLS,
                                    itemName = AnalyticsUtils.TOOLS_VIDEO
                                )
                                startActivity(Intent(context, FileInfoActivity::class.java).apply {
                                    putExtra("fileType", GlobalInfo.videoFunction)
                                })
                            }
                        }
                    }
                    R.id.tv_category_music -> {
                        context?.let {
                            getPermissionForApp(it) { getPermission ->
                                setPoint(getPermission)
                                AnalyticsUtils.appPageBuriedPoint(
                                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                    screenName = AnalyticsUtils.TOOLS,
                                    itemName = AnalyticsUtils.TOOLS_MUSIC
                                )
                                startActivity(Intent(context, FileInfoActivity::class.java).apply {
                                    putExtra("fileType", GlobalInfo.musicFunction)
                                })
                            }
                        }
                    }
                    R.id.tv_category_txt -> {
                        context?.let {
                            getPermissionForApp(it) { getPermission ->
                                setPoint(getPermission)
                                AnalyticsUtils.appPageBuriedPoint(
                                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                    screenName = AnalyticsUtils.TOOLS,
                                    itemName = AnalyticsUtils.TOOLS_TEXT
                                )
                                startActivity(Intent(context, FileInfoActivity::class.java).apply {
                                    putExtra("fileType", GlobalInfo.textFunction)
                                })
                            }
                        }
                    }
                    R.id.tv_category_download -> {
                        context?.let {
                            getPermissionForApp(it) { getPermission ->
                                setPoint(getPermission)
                                AnalyticsUtils.appPageBuriedPoint(
                                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                    screenName = AnalyticsUtils.TOOLS,
                                    itemName = AnalyticsUtils.TOOLS_DOWNLOAD
                                )
                                startActivity(Intent(context, FileInfoActivity::class.java).apply {
                                    putExtra("fileType", GlobalInfo.downloadFunction)
                                })
                            }
                        }
                    }
                    R.id.ll_tiktok_targeted_clean -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_TIKTOK
                        )

                        AnalyticsUtils.appFunctionBuriedPoint(
                            eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                            contentType = "TikTok",
                            itemCategory = "Page",
                            AnalyticsUtils.TODO_VALUE
                        )

                        if (AppBean.tiktokAlive) {
                            startActivity(Intent(
                                context, TargetedCleanActivity::class.java
                            ).apply {
                                putExtra("cleanType", GlobalInfo.targetedTiktokFunction)
                                putExtra("pageEmbeddingPoint", AnalyticsUtils.TOOLS_VALUE)
                                putExtra("appClick", true)
                            })
                        } else {
                            getString(R.string.tiktok_not_install_text).showToast()
                        }
                    }
                    R.id.ll_whatsapp_targeted_clean -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_WHATS_APP
                        )
                        if (AppBean.whatsAppAlive) {

                            AnalyticsUtils.appFunctionBuriedPoint(
                                eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                                contentType = "WhatsApp",
                                itemCategory = "Page",
                                AnalyticsUtils.TODO_VALUE
                            )

                            startActivity(Intent(
                                context, TargetedCleanActivity::class.java
                            ).apply {
                                putExtra("cleanType", GlobalInfo.targetedWhatsAppFunction)
                                putExtra("pageEmbeddingPoint", AnalyticsUtils.TOOLS_VALUE)
                                putExtra("appClick", true)
                            })
                        } else {
                            getString(R.string.whats_app_not_install_text).showToast()
                        }
                    }
                    R.id.ll_google_play_targeted_clean -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_GOOGLE_PLAY
                        )
                        if (AppBean.googlePlayAlive) {

                            AnalyticsUtils.appFunctionBuriedPoint(
                                eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                                contentType = "GooglePlay",
                                itemCategory = "Page",
                                AnalyticsUtils.TODO_VALUE
                            )

                            startActivity(Intent(
                                context, TargetedCleanActivity::class.java
                            ).apply {
                                putExtra("cleanType", GlobalInfo.targetedGooglePlayFunction)
                                putExtra("pageEmbeddingPoint", AnalyticsUtils.TOOLS_VALUE)
                                putExtra("appClick", true)
                            })
                        } else {
                            getString(R.string.google_play_not_install_text).showToast()
                        }
                    }
                    R.id.ll_youtube_targeted_clean -> {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.TOOLS,
                            itemName = AnalyticsUtils.TOOLS_YOUTUBE
                        )

                        if (AppBean.youTubeAlive) {

                            AnalyticsUtils.appFunctionBuriedPoint(
                                eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                                contentType = "Youtube",
                                itemCategory = "Page",
                                AnalyticsUtils.TODO_VALUE
                            )

                            startActivity(Intent(
                                context, TargetedCleanActivity::class.java
                            ).apply {
                                putExtra("cleanType", GlobalInfo.targetedYouTubeFunction)
                                putExtra("pageEmbeddingPoint", AnalyticsUtils.TOOLS_VALUE)
                                putExtra("appClick", true)
                            })
                        } else {
                            getString(R.string.youtube_not_install_text).showToast()
                        }
                    }
                    R.id.ll_line_targeted_clean -> {
                        if (AppBean.lineAlive) {
                            startActivity(Intent(
                                context, TargetedCleanActivity::class.java
                            ).apply {
                                putExtra("cleanType", GlobalInfo.targetedLineFunction)
                            })
                        } else {
                            getString(R.string.line_not_install_text).showToast()
                        }
                    }
                    R.id.tv_about_us -> {
                        startActivity(Intent(context, H5Activity::class.java).apply {
                            putExtra("h5url", "https://hyper-filemanager.web.app")
                            putExtra("appClick", true)
                        })
                    }
                }
            }
        }
    }

    private fun setPoint(getPermission: Boolean) {
        if (getPermission) {
            AnalyticsUtils.appPageBuriedPoint(
                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                screenName = AnalyticsUtils.CLEAN_JUNK_DIALOG_ACCESS,
                itemName = AnalyticsUtils.DIALOG_ACCESS_ALLOW
            )
        } else {
            AnalyticsUtils.appPageBuriedPoint(
                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                screenName = AnalyticsUtils.CLEAN_JUNK_DIALOG_ACCESS,
                itemName = AnalyticsUtils.DIALOG_ACCESS_DO_NOT_ALLOW
            )
        }
    }

}
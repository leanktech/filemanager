package com.hyperstudio.hyper.file.clean.viewmodel

import android.os.Environment
import androidx.databinding.ObservableArrayList
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.clean.model.CleanCacheModel
import kotlinx.coroutines.delay
import java.io.File

/**
 * @Name: CleanCacheViewModel
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/26 17:24
 */
class CleanCacheViewModel : BaseViewMode() {

    /**
     * 加载需要清理缓存 缓存垃圾大小需要刷新的String
     */
    var fileSizeNow = init("")

    /**
     * 清理缓存 清理缓存大小需要刷新的String
     */
    var cleanFileSizeNow = init("")

    /**
     * 加载出的缓存种类以及大小的list
     */
    var cleanCacheList = ObservableArrayList<CleanCacheModel>()

    /**
     * 总垃圾缓存大小
     */
    private var fileSize = 0L

    /**
     * 获取app的缓存大小
     * 1. 录制的视频 包括广告(待研究)
     * 2. 录制的音频 (待研究)
     * 3. 下载的更新包
     * 4. 缓存cache
     */

    //内部存储文件垃圾 以及大小
    private var filesDir: File? = null
    private var filesDirSize: Long = 0

    //内部存储缓存垃圾 以及大小
    private var cacheDir: File? = null
    private var cacheDirSize: Long = 0

    //外部存储下载垃圾 以及大小
    private var filesExternalDir: File? = null
    private var filesExternalDirSize: Long = 0

    //外部存储缓存垃圾 以及大小
    private var cacheExternalDir: File? = null
    private var cacheExternalDirSize: Long = 0

    /**
     * 获取所有需要清理的种类以及大小
     */
    fun getAppCache(callBack: (Boolean) -> Unit) {
        launchUI {
            cleanCacheList.clear()
            ActivityMgr.getContext().let { context ->
                fileSizeNow.value = "0B"

                filesDir = context.filesDir
                cacheDir = context.cacheDir
                filesExternalDir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                cacheExternalDir = context.externalCacheDir

                filesDirSize = getDirSize(filesDir)
                initSizeData(
                    filesDirSize,
                    context.getString(R.string.internal_file_garbage_text)
                )
                delay(500)

                cacheDirSize = getDirSize(cacheDir)
                initSizeData(
                    cacheDirSize,
                    context.getString(R.string.internal_cache_garbage_text)
                )
                delay(500)

                filesExternalDirSize = getDirSize(filesExternalDir)
                initSizeData(
                    filesExternalDirSize,
                    context.getString(R.string.external_download_garbage_text)
                )
                delay(500)

                cacheExternalDirSize = getDirSize(cacheExternalDir)
                initSizeData(
                    cacheExternalDirSize,
                    context.getString(R.string.external_cache_garbage_text)
                )
                delay(500)

                //回调需不需要清理
                callBack(fileSize != 0L)
                ("getAppCache: 当前缓存大小$fileSize").print()
            }
        }
    }

    /**
     * 更新垃圾缓存总大小 以及清理种类和大小的方法
     * @param scanFileSize 需要清理的垃圾缓存大小
     * @param cleanText 清理的种类
     */
    private fun initSizeData(scanFileSize: Long, cleanText: String) {
        fileSize += scanFileSize
        this.fileSizeNow.value = formatFileSizeToUnit(fileSize)
        cleanCacheList.add(CleanCacheModel().apply {
            cleanType = cleanText
            cleanSize = formatFileSizeToUnit(scanFileSize)
        })
    }

    /**
     * 获取文件大小(字节为单位)
     * @param dir
     * @return 文件大小
     */
    private fun getDirSize(dir: File?): Long {
        if (dir == null) {
            return 0
        }
        if (!dir.isDirectory) {
            return 0
        }
        var dirSize: Long = 0
        dir.listFiles()?.let { fileList ->
            for (file in fileList) {
                if (file is File) {
                    if (file.isFile) {
                        dirSize += file.length() //文件的长度就是文件的大小
                    } else if (file.isDirectory) {
                        dirSize += file.length()
                        dirSize += getDirSize(file) // 递归调用继续统计
                    }
                }
            }
        }
        return dirSize
    }


    /**
     * 清楚app垃圾缓存方法
     * @param callBack 清楚成不成功的回调
     */
    fun clearAppCache(callBack: (Boolean) -> Unit) {
        launchUI {
            try {
                cleanFileSizeNow.value = formatFileSizeToUnit(fileSize)
                delay(500)

                if (filesDirSize != 0L) {
                    cleanCache(filesDir, filesDirSize)
                    delay(500)
                }

                if (cacheDirSize != 0L) {
                    cleanCache(cacheDir, cacheDirSize)
                    delay(500)
                }

                if (filesExternalDirSize != 0L) {
                    cleanCache(filesExternalDir, filesExternalDirSize)
                    delay(500)
                }

                if (cacheExternalDirSize != 0L) {
                    cleanCache(cacheExternalDir, cacheExternalDirSize)
                    delay(500)
                }

                callBack(true)
                ("缓存清除完毕").print()
            } catch (e: Exception) {
                callBack(false)
                e.printStackTrace()
                ("缓存清除失败").print()
            }
        }
    }


    /**
     * 清除缓存目录
     * @param dir 目录
     * @param curTime 当前系统时间
     */
    private fun clearCacheFolder(dir: File?, curTime: Long): Int {
        var deletedFiles = 0
        if (dir != null && dir.isDirectory) {
            try {
                dir.listFiles()?.let { fileList ->
                    for (child in fileList) {
                        if (child.isDirectory) {
                            deletedFiles += clearCacheFolder(child, curTime)
                        }
                        if (child.lastModified() < curTime) {
                            if (child.delete()) {
                                deletedFiles++
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        "clearCacheFolder: 清除目录: " + dir?.absolutePath?.print()
        return deletedFiles
    }

    /**
     * 清理file的方法 更新清理的问价大小value
     * @param file 需要清理的垃圾缓存
     * @param fileTypeSize 清理掉的文件大小
     */
    private fun cleanCache(file: File?, fileTypeSize: Long) {
        clearCacheFolder(file, System.currentTimeMillis())
        if(fileSize > 0){
            fileSize -= fileTypeSize
        }
        cleanFileSizeNow.value = formatFileSizeToUnit(fileSize)
    }

}
package com.hyperstudio.hyper.file.trustlook.viewmodel

import android.os.Environment
import androidx.databinding.ObservableArrayList
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.init
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.bundlebean.DangerAppBundleBean
import com.trustlook.sdk.data.AppInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

/**
 * @Name: TrustLookViewModel
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/3 10:36
 */
class TrustLookViewModel : BaseViewMode() {

    var folderDesc = init("")

    val apkFilePathList = ObservableArrayList<String>()

    var trustLoadingValue = init("0%")

    /**
     * 更新需要扫描的apkFile
     */
    fun getFolderDesc() {
        val sdRoot = Environment.getExternalStorageDirectory().absolutePath
        val sdRootDir = File(sdRoot)
        getFileName(sdRootDir.listFiles())
        folderDesc = init(
            ActivityMgr.getContext()
                .getString(R.string.folder_scan_desc, sdRoot, apkFilePathList.size)
        )
    }

    /**
     * 遍历apk文件
     */
    private fun getFileName(files: Array<File>?) {
        if (files != null) {
            for (file in files) {
                if (file.isDirectory) {
                    getFileName(file.listFiles())
                } else {
                    val fileName = file.name
                    if (fileName.endsWith(".apk")) {
                        apkFilePathList.add(file.path)
                    }
                }
            }
        }
    }

    /**
     * 更新有危险的app list
     */
    suspend fun updateDataList(resultList: MutableList<AppInfo>): ArrayList<DangerAppBundleBean> {
        return withContext(Dispatchers.IO) {

            val list: ArrayList<DangerAppBundleBean> = arrayListOf()

            if (resultList.size > 0) {
                for (appInfo in resultList) {
                    if (appInfo.score >= 6) {
                        list.add(DangerAppBundleBean().apply {
                            //App PackageName赋值
                            appPackageName = if (appInfo.packageName != null) {
                                appInfo.packageName
                            } else {
                                appInfo.apkPath
                            }

                            //App Name赋值
                            appName = appInfo.appName

                            //App category赋值(app行为)
                            appInfo.category?.let { category ->
                                val categoryText: String =
                                    if (Locale.getDefault().language == "zh") {
                                        category[0]
                                    } else {
                                        category[1]
                                    }
                                appCategory = categoryText
                            }

                            //App Summary赋值(app行为介绍)
                            appInfo.summary?.let { summary ->
                                val summaryText: String =
                                    if (Locale.getDefault().language == "zh") {
                                        summary[0]
                                    } else {
                                        summary[1]
                                    }
                                appSummary = summaryText
                            }
                        })
                    }
                }
            }

            list
        }
    }

}
package com.hyperstudio.hyper.file.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.widget.RemoteViews
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.base_lib.tool.showToast
import com.hyperstudio.hyper.file.splash.activity.SplashActivity


/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/4/27 18:56
 */
class BatteryAppWidget : AppWidgetProvider() {

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray?
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        val widgetBatteryView =
            RemoteViews(ActivityMgr.getContext().packageName, R.layout.widget_battery_layout)

        //battery
        val batteryIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
        batteryIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
        batteryIntent.putExtra("functionType", GlobalInfo.saverFunction)
        batteryIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
        val pendingBatteryIntent = PendingIntent.getActivity(
            ActivityMgr.getContext(),
            104,
            batteryIntent,
            PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        widgetBatteryView.setOnClickPendingIntent(
            R.id.widget_battery_btn,
            pendingBatteryIntent
        )

        val batteryStatus = context?.applicationContext?.registerReceiver(
            this,
            IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        )

        //Battery Level
        val level = batteryStatus?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)

        //Max Battery Level
        val scale = batteryStatus?.getIntExtra(BatteryManager.EXTRA_SCALE, -1)

        var showBattery = 100
        val batteryPct = scale?.toFloat()?.let { level?.div(it) }
        if (batteryPct != null) {
            showBattery = (batteryPct * 100).toInt()
        }

        widgetBatteryView.setTextViewText(
            R.id.tv_battery_percent,
            "$showBattery%"
        )

        appWidgetManager?.updateAppWidget(appWidgetIds, widgetBatteryView)
    }

    override fun onEnabled(context: Context) {
        super.onEnabled(context)
    }


    override fun onDisabled(context: Context) {
        super.onDisabled(context)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)

        val addSuccess = intent?.getBooleanExtra("addSuccess", false)
        if(addSuccess == true){
            context?.getString(R.string.widget_dialog_bottom_hint)?.showToast()
        }

        if (intent?.action == Intent.ACTION_BATTERY_CHANGED || ((intent?.action).equals(Intent.ACTION_POWER_CONNECTED)) || ((intent?.action).equals(
                Intent.ACTION_POWER_DISCONNECTED
            ))
        ) {
            //get Bundle
            val extras = intent?.extras

            //if extras
            if (extras != null) {
                val appWidgetManager = AppWidgetManager.getInstance(context)
                val thisAppWidget =
                    context?.packageName?.let {
                        ComponentName(
                            it,
                            BatteryAppWidget::class.java.name
                        )
                    }
                val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget)

                //call the onUpdate
                onUpdate(context, appWidgetManager, appWidgetIds)
            }
        }
    }
}
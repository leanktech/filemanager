package com.hyperstudio.hyper.file.admob

import android.content.Context
import androidx.annotation.NonNull
import com.google.android.gms.ads.*
import com.google.android.gms.ads.appopen.AppOpenAd
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.NativeAd
import com.hyperstudio.hyper.file.base_lib.tool.print
import com.hyperstudio.hyper.file.base_lib.tool.toJson
import java.util.*

/**
 * @Name: AdUtils
 * @Description: 加载广告单例(存储广告对象)
 * @Author: even@leanktech.com
 * @Date: 2021/10/20 14:51
 */
object AdUtils {

    //interstitial 广告对象
    var interstitialAd: InterstitialAd? = null
    var interstitialErrorMessage: String? = null

    //native 广告对象
    var nativeAd: NativeAd? = null
    var nativeErrorMessage: String? = null

    //app open 广告对象
    var appOpenAd: AppOpenAd? = null

    //banner 对象
    var bannerAd: AdView? = null

    /**
     * 加载interstitial广告
     */
    fun loadInterstitialAd(context: Context, interstitialId: String) {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            context, interstitialId, adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(@NonNull interstitialAd: InterstitialAd) {
                    "Splash onAdLoaded success".print()
                    this@AdUtils.interstitialAd = interstitialAd
                    interstitialErrorMessage = null
                }

                override fun onAdFailedToLoad(@NonNull loadAdError: LoadAdError) {
                    "Splash onAdFailedToLoad error${loadAdError.message}".print()
                    interstitialErrorMessage = loadAdError.message
                    interstitialAd = null
                }

            })
    }

    /**
     * 加载native广告
     */
    fun loadNativeAd(context: Context, nativeId: String) {
        val adLoader = AdLoader.Builder(context, nativeId)
            .forNativeAd { nativeAd ->
                AdUtils.nativeAd = nativeAd
            }
            .withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(error: LoadAdError) {
                    nativeAd = null
                    nativeErrorMessage = error.toString()
                    "Failed to native ad: $error".print()
                }
            }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    /**
     * 加载app open广告
     */
    fun loadAppOpenAd(context: Context, appOpenId: String) {
        val request = getAdRequest()
        AppOpenAd.load(
            context, appOpenId, request,
            AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT,
            object : AppOpenAd.AppOpenAdLoadCallback() {
                override fun onAdFailedToLoad(p0: LoadAdError) {
                    super.onAdFailedToLoad(p0)
                    "onAdFailedToLoad errorMessage=${p0.message} errorCode=${p0.code}".print()
                }

                override fun onAdLoaded(p0: AppOpenAd) {
                    super.onAdLoaded(p0)
                    "onAdLoaded ${p0.toJson()}".print()
                    appOpenAd = p0
                }
            }
        )
    }

    fun loadBannerAd(context: Context?, bannerId: String, adSize: AdSize): AdView? {
        context?.let{
            val ad = AdView(context)
            //adView.addView(ad)
            ad.adUnitId = bannerId
            ad.adSize = adSize
            val adRequest = AdRequest
                .Builder().build()
            ad.loadAd(adRequest)
            ad.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
                    "onAdLoaded".print("banner")
                    bannerAd = ad
                }

                override fun onAdFailedToLoad(adError: LoadAdError) {
                    // Code to be executed when an ad request fails.
                    "banner error code=${adError.code} message=${adError.message}".print("banner")
                    if (adError.code == 3) {
                        ad.destroy()
                        bannerAd = null
                    }
                }

                override fun onAdOpened() {
                    // Code to be executed when an ad opens an overlay that
                    // covers the screen.
                    "onAdOpened".print("banner")
                }

                override fun onAdClicked() {
                    // Code to be executed when the user clicks on an ad.
                    "onAdClicked".print("banner")
                }

                override fun onAdClosed() {
                    // Code to be executed when the user is about to return
                    // to the app after tapping on an ad.
                    "onAdClosed".print("banner")
//                    ad.destroy()
                    bannerAd = null
                }
            }
        }
        return bannerAd
    }

    fun loadAdObject(context: Context, interstitialId: String?, nativeId: String?){
        //加载Native和interstitial广告
        interstitialId?.let { loadInterstitialAd(context, it) }
        nativeId?.let { loadNativeAd(context, it) }
    }

    fun getAdObjectSuccess(): Boolean{
        return interstitialAd != null && nativeAd != null
    }

    fun getNativeAdObjectSuccess(): Boolean{
        return nativeAd != null
    }

    fun getIntAdObjectSuccess(): Boolean{
        return interstitialAd != null
    }

    /**
     * 创建并返回广告请求。
     * */
    private fun getAdRequest(): AdRequest {
        return AdRequest.Builder().build()
    }

}
package com.hyperstudio.hyper.file.h5

import android.annotation.SuppressLint
import com.alibaba.android.arouter.facade.annotation.Autowired
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.databinding.H5ActivityBinding
import com.gyf.immersionbar.ImmersionBar

/**
 * @Name: H5Activity
 * @Description: 加载h5页面
 * @Author: jason@leanktech.com
 * @Date: 2021/9/17 13:44
 */
class H5Activity : BaseActivity<H5ActivityBinding>() {

    /**
     * H5页面url
     */
    @Autowired(name = "h5url")
    @JvmField
    var h5url: String? = ""

    /**
     * H5页面标题
     */
    @Autowired(name = "h5title")
    @JvmField
    var h5title: String? = ""


    override fun getLayoutId(): Int = R.layout.h5_activity

    @SuppressLint("SetJavaScriptEnabled", "NewApi")
    override fun initView() {
        toolBar?.setToolbarColor(R.color.color_0078FF)
        setTitleContent(titleName = "Web browsing")
        ImmersionBar.with(this).statusBarColor(R.color.color_0078FF).init()
        bindingView.lifecycleOwner = this

        bindingView.x5WebView.settings.apply {
            javaScriptEnabled = true
            javaScriptCanOpenWindowsAutomatically = true
            setSupportZoom(true)
            displayZoomControls = false
            allowContentAccess = true
            domStorageEnabled = true
            mediaPlaybackRequiresUserGesture = true
            loadsImagesAutomatically = true

        }
    }

    override fun loadData(isRefresh: Boolean) {
        if (h5url?.isEmpty() == true) {
            h5url = intent?.getStringExtra("h5url") ?: ""
        }
        h5url?.let{
            bindingView.x5WebView.loadUrl(it)
        }
    }

}
package com.hyperstudio.hyper.file.clean.viewmodel

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.lifecycle.MutableLiveData
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.init
import com.hyperstudio.hyper.file.base_lib.tool.print
import kotlinx.coroutines.delay
import java.util.*
import kotlin.random.Random

class CleanAppViewModel : BaseViewMode() {

    var cleanAppPackageIcon = MutableLiveData<Drawable>()

    val cleanAppLimit = init("")

    val cleanAppTotalLimit = init("")

    private var activityManager: ActivityManager? = null

    private var appLimit = 0

    /**
     * 获取其他app以及kill掉的方法
     */
    @SuppressLint("DefaultLocale")
    fun getAnotherAppList(callback: () -> Unit) {
        launchUI {
            activityManager = ActivityMgr.getContext()
                .getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                val appTasks = activityManager?.getRunningTasks(20)
                appTasks?.forEach {
                    it.topActivity?.packageName?.let { packageName ->
                        initItem(packageName)
                    }
                    delay(300)
                }
            } else {
                val pm = ActivityMgr.getContext().packageManager
                // 查询所有已经安装的应用程序
                val listApplications: List<ApplicationInfo> =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        pm.getInstalledApplications(PackageManager.MATCH_UNINSTALLED_PACKAGES)
                    } else {
                        pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES)
                    }

                var packageNameList: MutableList<String> = mutableListOf()
                listApplications.forEach {
                    if (!it.packageName.contains("android") &&
                        !it.packageName.contains("system") &&
                        !it.packageName.contains("services") &&
                        !it.packageName.contains("com.sec") &&
                        !it.packageName.contains(Build.BRAND.toLowerCase(Locale.getDefault())) &&
                        !it.packageName.equals(ActivityMgr.getContext().packageName) &&
                        !it.packageName.contains("miui") &&
                        !it.packageName.contains("qualcomm") &&
                        !it.packageName.contains("vendor")
                    ) {
                        packageNameList.add(it.packageName)
                    }
                }

                //限制一下扫描APP的个数,N是实际app个数。如果N<30,n=N;如果N>=30,n取一个20-30随机整数
                //扫描动画显示前n个app，数字显示x/n这样效果
                if (packageNameList.size >= 30) {
                    val randomNum = Random.nextInt(20, 30)
                    packageNameList = packageNameList.subList(0, randomNum)
                }
                cleanAppTotalLimit.value = (packageNameList.size - 1).toString()
                packageNameList.forEach {
                    //捕获异常 有些app由于没有定义这样的类，或忘了在AndroidManifest中注册 会报NameNotFoundException
                    try {
                        initItem(it)
                        delay(100)
                    } catch (e: Exception) {
                    }
                }
            }
            callback()
        }
    }

    private fun initItem(packageName: String) {
        cleanAppLimit.value = (appLimit++).toString()
        activityManager?.killBackgroundProcesses(packageName)
        val aInfo = ActivityMgr.getContext().packageManager.getApplicationInfo(
            packageName,
            PackageManager.GET_META_DATA
        )
        val appIcon: Drawable =
            ActivityMgr.getContext().packageManager.getApplicationIcon(aInfo)
        cleanAppPackageIcon.value = appIcon
        ("running app is :$packageName").print()
    }
}
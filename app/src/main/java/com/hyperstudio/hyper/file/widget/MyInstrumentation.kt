package com.hyperstudio.hyper.file.widget

import android.app.Instrumentation
import android.os.Build

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/8/2 17:08
 */
class MyInstrumentation : Instrumentation() {
    override fun onException(obj: Any?, e: Throwable?): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (e.toString().contains("DeadSystemException")) {
                return true
            }
            return super.onException(obj, e)
        }
        return super.onException(obj, e)
    }
}
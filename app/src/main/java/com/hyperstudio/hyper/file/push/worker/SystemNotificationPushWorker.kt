package com.hyperstudio.hyper.file.push.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hyperstudio.hyper.file.push.notification.initSystemResidentNotification

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/1/29 19:49
 */
class SystemNotificationPushWorker(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
        initSystemResidentNotification()
        return Result.success()
    }

}
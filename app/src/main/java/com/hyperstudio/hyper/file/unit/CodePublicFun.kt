package com.hyperstudio.hyper.file.unit

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.telephony.TelephonyManager
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.base_lib.tool.permission.getPermissionMultiple
import com.hyperstudio.hyper.file.base_lib.tool.permission.setPermissionCallBacks
import com.hyperstudio.hyper.file.broadcast.AppAddRemoveBroadcastReceiver
import com.hyperstudio.hyper.file.broadcast.ScreenOnBroadcastReceiver
import com.hyperstudio.hyper.file.clean.activity.BatteryCleanActivity
import com.hyperstudio.hyper.file.clean.activity.BoostCleanActivity
import com.hyperstudio.hyper.file.clean.activity.CoolerCleanActivity
import com.hyperstudio.hyper.file.push.notification.initSystemResidentNotification
import com.hyperstudio.hyper.file.service.NotificationPushMonitorService
import java.io.File
import java.util.*

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/11/2 18:53
 */

/**
 * 开启清理其他app进程的类
 */
fun startCleanActivity(
    context: Context,
    cleanType: String,
    memoryUsage: Boolean = false,
    pageEmbeddingPoint: String = "",
    appClick: Boolean = false
) {
    val turnClass = when(cleanType){
        GlobalInfo.boostFunction -> {
            BoostCleanActivity::class.java
        }
        GlobalInfo.saverFunction -> {
            BatteryCleanActivity::class.java
        }
        GlobalInfo.coolerFunction -> {
            CoolerCleanActivity::class.java
        }
        else -> {
            BoostCleanActivity::class.java
        }
    }

    val intent = Intent(context, turnClass).apply {
        putExtra("cleanType", cleanType)
        if (memoryUsage) {
            putExtra("memoryUsage", memoryUsage)
        }
        if (pageEmbeddingPoint.isNotEmpty()) {
            putExtra("pageEmbeddingPoint", pageEmbeddingPoint)
        }
        putExtra("appClick", appClick)
    }
    context.startActivity(intent)
}

fun getSimpleReceiverIntentFilter(): IntentFilter {
    return IntentFilter().apply {
        addAction(Intent.ACTION_SCREEN_OFF)
        addAction(Intent.ACTION_BATTERY_CHANGED)
        addAction(Intent.ACTION_POWER_CONNECTED)
        addAction(Intent.ACTION_USER_PRESENT)
        addAction(Intent.ACTION_TIME_TICK)
    }
}

fun getAppAddRemoveReceiverIntentFilter(): IntentFilter {
    return IntentFilter().apply {
        addAction(Intent.ACTION_PACKAGE_ADDED)
        addAction(Intent.ACTION_PACKAGE_REPLACED)
        addAction(Intent.ACTION_PACKAGE_REMOVED)
        addDataScheme("package")
    }
}

fun updateSystemResidentNotification() {
    initSystemResidentNotificationChannel()
    initSystemResidentNotification()
}

fun startAliveService(context: Context) {
    val isRunning =
        ServiceUtil.isServiceRunning(context, NotificationPushMonitorService::class.java.name)
    if (!isRunning) {
        try {
            if (AppTool.isAppForeground()) {//如果APP在前台
                try {
                    //启动亮屏监听服务
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        context.startForegroundService(
                            Intent(
                                context,
                                NotificationPushMonitorService::class.java
                            )
                        )
                    } else {
                        context.startService(
                            Intent(
                                context,
                                NotificationPushMonitorService::class.java
                            )
                        )
                    }
                } catch (e: Exception) {
                    e.message.toString().print("============doWork")
                    registerBroadcastReceiver(context)
                }
            } else {
                registerBroadcastReceiver(context)
            }
        } catch (e: Exception) {
            e.message.toString().print("============doWork1")
        }
    }
}

fun registerBroadcastReceiver(context: Context) {
    ScreenOnBroadcastReceiver.screenOnBroadcastReceiver = ScreenOnBroadcastReceiver
    //注册广播
    context.registerReceiver(
        ScreenOnBroadcastReceiver.screenOnBroadcastReceiver,
        getSimpleReceiverIntentFilter()
    )
    AppAddRemoveBroadcastReceiver.appAddRemoveBroadcastReceiver = AppAddRemoveBroadcastReceiver
    context.registerReceiver(
        AppAddRemoveBroadcastReceiver.appAddRemoveBroadcastReceiver,
        getAppAddRemoveReceiverIntentFilter()
    )
}

fun getTargetedApp() {
    var tiktokAlive = false
    var whatsAppAlive = false
    var googlePlayAlive = false
    var youTubeAlive = false
    var lineAlive = false
    val pm = ActivityMgr.getContext().packageManager
    // 查询所有已经安装的应用程序
    val listApplications: List<ApplicationInfo> =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            pm.getInstalledApplications(PackageManager.MATCH_UNINSTALLED_PACKAGES)
        } else {
            pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES)
        }

    listApplications.forEach {
        if (it.packageName.contains(GlobalInfo.tiktokPackageName)) {
            tiktokAlive = true
        }
        if (it.packageName.contains(GlobalInfo.whatsAppPackageName)) {
            whatsAppAlive = true
        }
        if (it.packageName.contains(GlobalInfo.googlePlayPackageName)) {
            googlePlayAlive = true
        }
        if (it.packageName.contains(GlobalInfo.youTubePackageName)) {
            youTubeAlive = true
        }
        if (it.packageName.contains(GlobalInfo.linePackageName)) {
            lineAlive = true
        }
        if (tiktokAlive && whatsAppAlive && googlePlayAlive && youTubeAlive && lineAlive) {
            return@forEach
        }

    }
    AppBean.tiktokAlive = tiktokAlive
    AppBean.whatsAppAlive = whatsAppAlive
    AppBean.googlePlayAlive = googlePlayAlive
    AppBean.youTubeAlive = youTubeAlive
    AppBean.lineAlive = lineAlive
}

fun getPhoneCountry(context: Context?, country: String, doNotCountry: Boolean = false): Boolean {
    val localeCountry = Locale.getDefault().country
    val tm = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    val tmCountry = tm.networkCountryIso?.toUpperCase(Locale.US)
    country.toUpperCase(Locale.US)
    return if (doNotCountry) {
        localeCountry != country && tmCountry != country
    } else {
        localeCountry == country && tmCountry == country
    }
}

fun getExternalUsedPercentage(): String {
    //获取内部存储根目录
    val path: File = Environment.getDataDirectory()
    //系统的空间描述类
    val stat = StatFs(path.path)
    //每个区块占字节数
    val blockSize = stat.blockSize.toLong()
    //区块总数
    val totalBlocks = stat.blockCount.toLong()
    val totalExternal = totalBlocks * blockSize

    //获取手机内部可用空间大小
    val availableBlocks = stat.availableBlocks.toLong()
    val canUseExternal = availableBlocks * blockSize

    return if (isNumeric(canUseExternal.toString()) && isNumeric(totalExternal.toString())) {
        val canUseExternalPercentage = canUseExternal.toFloat() / totalExternal.toFloat() * 100
        (100 - canUseExternalPercentage).toInt().toString()
    } else {
        "20"
    }
}


/**
 * 文件夹扫描需要的权限
 */
fun getPermissionForApp(context: Context, callBack: (Boolean) -> Unit) {
    getPermissionMultiple(
        context,
        mutableListOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
        arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
        ),
        setPermissionCallBacks {
            setOnGrantedAllSuccess {
                callBack(true)
            }
            setOnGrantedHaveError {
                callBack(false)
            }
            setOnDeniedNever {
                callBack(false)
            }
            setOnDenied {
                callBack(false)
            }
        }
    )
}
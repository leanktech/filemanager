package com.hyperstudio.hyper.file.score

import android.content.Intent
import android.net.Uri
import android.view.Gravity
import android.view.View
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.fragment.BaseDialogFragment
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.databinding.GiveScoreLayoutBinding


/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/12/21 18:04
 */
class GiveScoreDialogFragment : BaseDialogFragment<GiveScoreLayoutBinding>() {

    /** 位置 */
    override val gravity = Gravity.CENTER

    override var dialogWidth: Int = (AppTool.getScreenWidth() * 0.9).toInt()

    override fun getLayoutId() = R.layout.give_score_layout

    override fun initView() {
        bindingView.lifecycleOwner = this
        bindingView.presenter = this

    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.iv_give_score_close -> {
                dismiss()
            }
            R.id.ll_give_score_rating, R.id.tv_rate_app -> {
                showGooglePlayStore()
                dismiss()
            }
        }
    }

    private fun showGooglePlayStore() {
        activity?.runOnUiThread {
            val appPkg = activity?.packageName
            val url = "http://play.google.com/store/apps/details?id=$appPkg"
            activity!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }
    }

}
package com.hyperstudio.hyper.file.main.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.BatteryManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.hyperstudio.hyper.file.BR
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.base_lib.adapter.MultiAdapter
import com.hyperstudio.hyper.file.base_lib.adapter.`interface`.ItemConvert
import com.hyperstudio.hyper.file.base_lib.base.ItemClickPresenter
import com.hyperstudio.hyper.file.base_lib.fragment.BaseFragment
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.clean.activity.CleanCacheActivity
import com.hyperstudio.hyper.file.clean.activity.TargetedCleanActivity
import com.hyperstudio.hyper.file.databinding.FragTodoBinding
import com.hyperstudio.hyper.file.databinding.TodoBannerItemBinding
import com.hyperstudio.hyper.file.databinding.TodoFootItemBinding
import com.hyperstudio.hyper.file.databinding.TodoHeadItemBinding
import com.hyperstudio.hyper.file.main.activity.MainActivity
import com.hyperstudio.hyper.file.main.model.TodoListBean
import com.hyperstudio.hyper.file.main.viewmodel.TodoViewModel
import com.hyperstudio.hyper.file.trustlook.activity.TrustLookActivity
import com.hyperstudio.hyper.file.trustlook.dialog.AgreeTrustDialogFragment
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import com.hyperstudio.hyper.file.unit.startCleanActivity
import com.hyperstudio.hyper.file.widget.activity.AddWidgetActivity
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*


/**
 * @Name: TodoFragment
 * @Description: Todo模块
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */


class ToDoFragment : BaseFragment<FragTodoBinding>(), ItemClickPresenter<TodoListBean> {

    private val todoViewModel by viewModel<TodoViewModel>()

    /**
     * Todo多布局
     */
    private val mAdapter by lazy {
        MultiAdapter(todoViewModel.todoList, BR.presenter, BR.item).apply {
            addTypeToLayout(TodoListBean.head, R.layout.todo_head_item)
            addTypeToLayout(TodoListBean.foot, R.layout.todo_foot_item)
            addTypeToLayout(TodoListBean.banner, R.layout.todo_banner_item)
            itemPresenter = this@ToDoFragment
            itemConvert = object : ItemConvert {
                @SuppressLint("SetTextI18n")
                override fun multiConvert(
                    holder: BaseDataBindingHolder<ViewDataBinding>?,
                    item: MultiItemEntity
                ) {
                    val bindView = holder?.dataBinding
                    if (item is TodoListBean) {
                        when (bindView) {
                            is TodoHeadItemBinding -> {
                                bindView.pbMemory.progress = item.memoryNum.let {
                                    it.removeRange(it.length - 1, it.length).toInt()
                                }
                                todoViewModel.launchUI {
                                    //当前显示的内存使用率
                                    val ramNumStr =
                                        bindView.tvRamNum.text?.toString()?.replace("%", "")
                                    val ramNum = if (isNumeric(ramNumStr)) ramNumStr?.toInt() else 0
                                    //最新统计的内存使用率
                                    val memoryNumber =
                                        if (isNumeric(AppBean.memoryNumber)) AppBean.memoryNumber.toInt() else 0
                                    item.memoryUsageNum = AppBean.memoryNumber//更新用户的内存使用率

                                    if (MainActivity.firstStartBoostAnimation || ramNum == 0 || (ramNum != memoryNumber)) {
                                        delay(1000)
                                        var memoryLimitNum = 0
                                        var loadingFinish = true
                                        while (loadingFinish) {
                                            if (memoryLimitNum < item.memoryUsageNum.toInt()) {
                                                var limit = memoryLimitNum++
                                                bindView.tvRamNum.text =
                                                    "$limit%"
                                                bindView.cpbTodoHeaderLoading.setProgress(limit)
                                                delay(55)
                                            } else {
                                                loadingFinish = false
                                                bindView.tvRamNum.text = item.memoryUsageNum + "%"
                                                bindView.cpbTodoHeaderLoading.setProgress(item.memoryUsageNum.toInt())
                                            }
                                        }
                                        MainActivity.firstStartBoostAnimation = false
                                    }
                                }

                            }
                            is TodoFootItemBinding -> {
                                when (item.todoFootItem?.itemName) {
                                    GlobalInfo.boostFunction -> {
                                        if (AppBean.startBoostToday == getTodayDateStr()) {
                                            bindView.vStartToday.visibility = View.GONE
                                        } else {
                                            bindView.vStartToday.visibility = View.VISIBLE
                                        }
                                    }
                                    GlobalInfo.saverFunction -> {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            if (AppBean.startBatteryToday == getTodayDateStr()) {
                                                bindView.tvItemInfo.visibility = View.GONE
                                            } else {
                                                bindView.tvItemInfo.visibility = View.VISIBLE

                                                val manager =
                                                    context?.getSystemService(FragmentActivity.BATTERY_SERVICE) as? BatteryManager
                                                val currentLevel =
                                                    manager?.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)

                                                bindView.tvItemInfo.text = "$currentLevel%"
                                                AppBean.electricQuantity = currentLevel.toString()
                                            }
                                        } else {
                                            if (AppBean.startBatteryToday == getTodayDateStr()) {
                                                bindView.vStartToday.visibility = View.GONE
                                            } else {
                                                bindView.vStartToday.visibility = View.VISIBLE
                                            }
                                        }
                                    }
                                    GlobalInfo.coolerFunction -> {
                                        if (AppBean.startCoolerToday == getTodayDateStr()) {
                                            bindView.tvItemInfo.visibility = View.GONE
                                        } else {
                                            bindView.tvItemInfo.visibility = View.VISIBLE
                                            val country =
                                                context?.let { DeviceTool.getCurrentCountry(it) }
                                            /**美国地区温度单位℉（华氏度）**/
                                            if (country?.trim() == "US") {
                                                bindView.tvItemInfo.text =
                                                    AppBean.temperature + "°F"
                                            } else {
                                                bindView.tvItemInfo.text =
                                                    AppBean.temperature + "°C"
                                            }
                                        }
                                    }
                                    GlobalInfo.securityFunction -> {
                                        if (AppBean.startSecurityToday == getTodayDateStr()) {
                                            bindView.vStartToday.visibility = View.GONE
                                        } else {
                                            bindView.vStartToday.visibility = View.VISIBLE
                                        }
                                    }
                                }
                            }
                            is TodoBannerItemBinding -> {
                                val parent: ViewGroup? = AdUtils.bannerAd?.parent as ViewGroup?
                                parent?.removeView(AdUtils.bannerAd)
                                AdUtils.bannerAd?.let {
                                    bindView.flToDoBannerAd.removeAllViews()
                                    bindView.flToDoBannerAd.addView(it)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun getLayoutId() = R.layout.frag_todo

    override fun initView() {
        bindingView.lifecycleOwner = this
        bindingView.rvTodoList.apply {
            layoutManager = LinearLayoutManager(this@ToDoFragment.context)
            adapter = mAdapter
        }
    }

    override fun loadData(isRefresh: Boolean) {
        todoViewModel.launchUI {
            todoViewModel.getTodoListData()
        }
    }

    override fun onItemClick(v: View?, item: TodoListBean) {
        when (v?.id) {
            R.id.tv_clean -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                    screenName = AnalyticsUtils.HOME,
                    itemName = AnalyticsUtils.HOME_CLEAN_NOW_BUTTON
                )
                startActivity(Intent(context, CleanCacheActivity::class.java).apply {
                    putExtra("pageEmbeddingPoint", AnalyticsUtils.TODO_VALUE)
                    putExtra("appClick", true)
                })
            }
            R.id.cl_clean_loading -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                    screenName = AnalyticsUtils.HOME,
                    itemName = AnalyticsUtils.HOME_CLEAN_NOW_CIRCLE
                )
                startActivity(Intent(context, CleanCacheActivity::class.java).apply {
                    putExtra("pageEmbeddingPoint", AnalyticsUtils.TODO_VALUE)
                    putExtra("appClick", true)
                })
            }
            R.id.tv_todo_btn -> {
                item.todoFootItem?.itemName?.let {
                    startFun(it, click = true, appClick = true)
                }
            }
            R.id.iv_tools_entrance -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                    screenName = AnalyticsUtils.HOME,
                    itemName = AnalyticsUtils.HOME_WIDGET
                )
                startActivity(Intent(context, AddWidgetActivity::class.java))
            }
        }
    }

    private fun startTrustLook(click: Boolean, appClick: Boolean = false) {
        if (click) {
            startActivity(Intent(context, TrustLookActivity::class.java).apply {
                putExtra("pageEmbeddingPoint", AnalyticsUtils.TODO_VALUE)
                putExtra("appClick", appClick)
            })
        } else {
            startActivity(Intent(context, TrustLookActivity::class.java))
        }
    }

    private fun startFun(type: String, click: Boolean = true, appClick: Boolean = false) {
        when (type) {
            GlobalInfo.moreFunction -> {//点击常驻通知栏的更多按钮
                try {
                    val mainActivity =
                        ActivityMgr.getOneActivity(MainActivity::class.java) as MainActivity
                    if (mainActivity != null && !mainActivity.isFinishing) {
                        Handler(Looper.getMainLooper()).postDelayed(Runnable {
                            mainActivity.selectDefaultFragment(0, false)
                        }, 500)
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }
            GlobalInfo.securityFunction -> {
                if (click) {
                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                        screenName = AnalyticsUtils.HOME,
                        itemName = AnalyticsUtils.HOME_VIRUS
                    )

                    AnalyticsUtils.appFunctionBuriedPoint(
                        eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                        contentType = "Virus",
                        itemCategory = "Page",
                        AnalyticsUtils.TODO_VALUE
                    )
                }

                if (AppBean.residentNotificationChoiceType.isNotEmpty()) {
                    startTrustLook(click, appClick)
                } else {
                    if (AppBean.agreeTrustLook) {
                        startTrustLook(click, appClick)
                    } else {
                        val dialogFragment = AgreeTrustDialogFragment {
                            AppBean.agreeTrustLook = true
                            startTrustLook(click, appClick)
                        }
                        dialogFragment.show(childFragmentManager, "trustAgree")
                    }
                }
            }
            GlobalInfo.targetedTiktokFunction -> {

                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                    contentType = "TikTok",
                    itemCategory = "Page",
                    AnalyticsUtils.TODO_VALUE
                )

                startActivity(
                    Intent(
                        context,
                        TargetedCleanActivity::class.java
                    ).apply {
                        putExtra("cleanType", GlobalInfo.targetedTiktokFunction)
                        putExtra("pageEmbeddingPoint", AnalyticsUtils.TODO_VALUE)
                        putExtra("appClick", appClick)
                    })
            }
            GlobalInfo.targetedWhatsAppFunction -> {

                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                    contentType = "WhatsApp",
                    itemCategory = "Page",
                    AnalyticsUtils.TODO_VALUE
                )

                startActivity(
                    Intent(
                        context,
                        TargetedCleanActivity::class.java
                    ).apply {
                        putExtra("cleanType", GlobalInfo.targetedWhatsAppFunction)
                        putExtra("pageEmbeddingPoint", AnalyticsUtils.TODO_VALUE)
                        putExtra("appClick", appClick)
                    })
            }
            GlobalInfo.targetedGooglePlayFunction -> {

                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                    contentType = "GooglePlay",
                    itemCategory = "Page",
                    AnalyticsUtils.TODO_VALUE
                )

                startActivity(
                    Intent(
                        context,
                        TargetedCleanActivity::class.java
                    ).apply {
                        putExtra("cleanType", GlobalInfo.targetedGooglePlayFunction)
                        putExtra("pageEmbeddingPoint", AnalyticsUtils.TODO_VALUE)
                        putExtra("appClick", appClick)
                    })
            }
            GlobalInfo.targetedYouTubeFunction -> {

                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                    contentType = "Youtube",
                    itemCategory = "Page",
                    AnalyticsUtils.TODO_VALUE
                )

                startActivity(
                    Intent(
                        context,
                        TargetedCleanActivity::class.java
                    ).apply {
                        putExtra("cleanType", GlobalInfo.targetedYouTubeFunction)
                        putExtra("pageEmbeddingPoint", AnalyticsUtils.TODO_VALUE)
                        putExtra("appClick", appClick)
                    })
            }
            GlobalInfo.targetedLineFunction -> {
                startActivity(
                    Intent(
                        context,
                        TargetedCleanActivity::class.java
                    ).apply {
                        putExtra("cleanType", GlobalInfo.targetedLineFunction)
                        putExtra("appClick", appClick)
                    })
            }
            else -> {
                if (type == GlobalInfo.coolerFunction && click) {
                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                        screenName = AnalyticsUtils.HOME,
                        itemName = AnalyticsUtils.HOME_CPU
                    )
                }
                if (type == GlobalInfo.saverFunction && click) {
                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                        screenName = AnalyticsUtils.HOME,
                        itemName = AnalyticsUtils.HOME_BATTERY
                    )
                }
                if (type == GlobalInfo.boostFunction && click) {
                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                        screenName = AnalyticsUtils.HOME,
                        itemName = AnalyticsUtils.HOME_BOOST
                    )
                }
                if (type == GlobalInfo.cleanFunction && click) {
                    AnalyticsUtils.appPageBuriedPoint(
                        eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                        screenName = AnalyticsUtils.HOME,
                        itemName = AnalyticsUtils.HOME_CLEAN
                    )
                }
                if(type == GlobalInfo.cleanFunction){
                    startActivity(Intent(context, CleanCacheActivity::class.java).apply {
                        putExtra("pageEmbeddingPoint", AnalyticsUtils.TODO_VALUE)
                        putExtra("appClick", true)
                    })
                }else {
                    context?.let {
                        startCleanActivity(
                            it,
                            type,
                            pageEmbeddingPoint = AnalyticsUtils.TODO_VALUE,
                            appClick = appClick
                        )
                    }
                }
            }
        }
    }

    fun getResidentNotificationType() {
        if (AppBean.residentNotificationChoiceType.isNotEmpty()) {
            startFun(AppBean.residentNotificationChoiceType, false)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun notifyAdapter() {
        mAdapter.notifyDataSetChanged()
    }


}
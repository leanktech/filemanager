package com.hyperstudio.hyper.file.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.base_lib.tool.showToast
import com.hyperstudio.hyper.file.splash.activity.SplashActivity
import com.hyperstudio.hyper.file.unit.getExternalUsedPercentage

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/4/27 18:56
 */
class CleanAppWidget: AppWidgetProvider() {

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray?
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        val widgetCleanView =
            RemoteViews(ActivityMgr.getContext().packageName, R.layout.widget_clean_layout)

        //clean
        val cleanIntent = Intent(ActivityMgr.getContext(), SplashActivity::class.java)
        cleanIntent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
        cleanIntent.putExtra("functionType", GlobalInfo.cleanFunction)
        cleanIntent.putExtra("notificationType", GlobalInfo.NOTIFICATION_PICTURE_CARD)//通知类型
        val pendingCleanIntent = PendingIntent.getActivity(
            ActivityMgr.getContext(),
            103,
            cleanIntent,
            PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        widgetCleanView.setOnClickPendingIntent(
            R.id.widget_clean_btn,
            pendingCleanIntent
        )

        widgetCleanView.setTextViewText(R.id.tv_storage_percent, getExternalUsedPercentage() + "%")

        appWidgetManager?.updateAppWidget(appWidgetIds, widgetCleanView)
    }

    override fun onEnabled(context: Context) {
        super.onEnabled(context)
    }


    override fun onDisabled(context: Context) {
        super.onDisabled(context)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        val addSuccess = intent?.getBooleanExtra("addSuccess", false)
        if(addSuccess == true){
            context?.getString(R.string.widget_dialog_bottom_hint)?.showToast()
        }
    }


}
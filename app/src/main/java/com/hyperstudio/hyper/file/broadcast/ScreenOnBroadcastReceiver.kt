package com.hyperstudio.hyper.file.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
//import com.hyperstudio.hyper.file.unit.SystemBroadcastModel.handleReceivedBatteryTemperature
import com.hyperstudio.hyper.file.unit.SystemBroadcastModel.handleReceivedUserPresent
import com.hyperstudio.hyper.file.unit.SystemBroadcastModel.handleReceiverPackageAddUpdateRemoved
//import com.hyperstudio.hyper.file.unit.SystemBroadcastModel.handlerReceiverCoolCpu

/**
 * @Description:
 * author: Rhett
 * time: 2021/12/24
 */
object ScreenOnBroadcastReceiver : BroadcastReceiver() {

    var screenOnBroadcastReceiver: ScreenOnBroadcastReceiver? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.e("AppLockerService", "事件 ${intent?.action}")
        println("intent?.action ========================= "+intent?.action)
        when (intent?.action) {
            // 亮屏
            Intent.ACTION_SCREEN_ON -> handleReceivedUserPresent(context!!, intent)
            // 锁屏
            Intent.ACTION_SCREEN_OFF -> {
            }
            // 电池状态
//            Intent.ACTION_BATTERY_CHANGED -> handleReceivedBatteryTemperature(context!!, intent)
            // 充电
//            Intent.ACTION_POWER_CONNECTED -> handleReceivedBatteryTemperature(context!!, intent, true)
            // 用户解锁手机
            Intent.ACTION_USER_PRESENT -> handleReceivedUserPresent(context!!, intent)
            // 应用安装
            Intent.ACTION_PACKAGE_ADDED -> handleReceiverPackageAddUpdateRemoved(context!!, intent, true)
            // 应用升级
            Intent.ACTION_PACKAGE_REPLACED -> handleReceiverPackageAddUpdateRemoved(context!!, intent, true)
            // 应用卸载
            Intent.ACTION_PACKAGE_REMOVED -> handleReceiverPackageAddUpdateRemoved(context!!, intent, false)
            // 每分钟触发一次
//            Intent.ACTION_TIME_TICK -> handlerReceiverCoolCpu(context!!)
        }
    }
}
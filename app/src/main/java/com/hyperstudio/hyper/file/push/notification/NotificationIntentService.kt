package com.hyperstudio.hyper.file.push.notification

import android.app.IntentService
import android.content.Intent
import androidx.core.app.NotificationManagerCompat


/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/12/14 17:58
 */
class NotificationIntentService : IntentService("notification") {
    override fun onHandleIntent(intent: Intent?) {
        when(intent?.action){
            "dismiss" -> {
                val notificationManagerCompat = NotificationManagerCompat.from(
                    applicationContext
                )
                notificationManagerCompat.cancel(intent.getIntExtra("notificationId", 0))
            }
        }
    }
}
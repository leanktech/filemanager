package com.hyperstudio.hyper.file.main.model

import androidx.databinding.BaseObservable
import com.hyperstudio.hyper.file.R
import java.io.Serializable

/**
 * @Name: FileListItemBean
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/8/31 16:56
 */
class FileListItemBean: BaseObservable(), Serializable {
    var fileIsDir: Boolean ?= null
    var fileImg: Int ?= null
    var fileInfo: String ?= null
    var fName: String ?= null
    var fPath: String ?= null

    var fileImgBg = R.color.color_0078FF
}
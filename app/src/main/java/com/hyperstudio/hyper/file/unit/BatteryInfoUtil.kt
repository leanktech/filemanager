package com.hyperstudio.hyper.file.unit

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import androidx.fragment.app.FragmentActivity

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/6/22 15:12
 */
/**
 * @Description:电池信息工具类
 * author: Rhett
 * time: 2022/6/16
 */
object BatteryInfoUtil {
    /**
     * 获取电池温度
     * */
    fun getTemperature(intent: Intent): Int {
        return try {
            intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1).let {
                if (it > 100) {
                    (it / 10)
                } else {
                    33
                }
            }
        }catch (e: Exception){
            33
        }
    }
    /**
     * ℃（摄氏度）转℉（华氏度）
     * 转换公式：℃ = (℉- 32) / 1.8
     * */
    fun transformC2F(temperC: Int): Int {
        val temperF = temperC * 1.8 + 32
        return temperF.toInt()
    }
    /**
     * 获取电池的剩余电量
     * */
    @SuppressLint("NewApi")
    fun getElectricQuantity(context: Context): Int {
        return try {
            val manager =
                context.getSystemService(FragmentActivity.BATTERY_SERVICE) as? BatteryManager
            val currentLevel =
                manager?.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
            currentLevel!!
        }catch (e: Exception) {
            e.printStackTrace()
            50
        }
    }
}
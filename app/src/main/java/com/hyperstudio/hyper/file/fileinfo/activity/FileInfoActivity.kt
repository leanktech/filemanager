package com.hyperstudio.hyper.file.fileinfo.activity

import android.os.Environment
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.hyperstudio.hyper.file.BR
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.adapter.SimpleAdapter
import com.hyperstudio.hyper.file.base_lib.base.ItemClickPresenter
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.databinding.FileInfoLayoutBinding
import com.hyperstudio.hyper.file.fileinfo.viewmodel.FileInfoViewModel
import com.hyperstudio.hyper.file.main.model.FileListItemBean
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File


/**
 * @Name: FileInfoActivity
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/29 18:11
 */
class FileInfoActivity : BaseActivity<FileInfoLayoutBinding>(),
    ItemClickPresenter<FileListItemBean> {

    private val fileInfoViewModel by viewModel<FileInfoViewModel>()

    var fileType = ""

    override val showToolBar: Boolean
        get() = false

    /**
     * 文件适配器
     */
    private val mAdapter by lazy {
        SimpleAdapter(
            R.layout.file_list_item,
            fileInfoViewModel.fileNameList,
            BR.presenter,
            BR.item
        ).apply {
            itemPresenter = this@FileInfoActivity
        }
    }

    override fun getLayoutId() = R.layout.file_info_layout

    override fun initView() {
        fileType = intent.getStringExtra("fileType").toString()
        bindingView.tvFileInfoBack.text = fileType

        bindingView.vm = fileInfoViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this

        mAdapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.SlideInLeft)
        mAdapter.isAnimationFirstOnly = false
        bindingView.rvFileInfoList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }

    override fun loadData(isRefresh: Boolean) {
        fileInfoViewModel.launchUI {
            when (fileType) {
                GlobalInfo.imageFunction, GlobalInfo.videoFunction, GlobalInfo.musicFunction -> {
                    fileInfoViewModel.getMediaStoreData(fileType) {
                        if (it == "empty") {
                            bindingView.emptyView.clEmptyView.visibility = View.VISIBLE
                        }
                    }
                }
                GlobalInfo.textFunction -> {
                    val file = File(Environment.getExternalStorageDirectory().path)
                    fileInfoViewModel.initTxtList(file.listFiles())
                    if(fileInfoViewModel.fileNameList.isEmpty()){
                        bindingView.emptyView.clEmptyView.visibility = View.VISIBLE
                    }
                }
                GlobalInfo.downloadFunction -> {
                    fileInfoViewModel.getDownloadList{
                        bindingView.emptyView.clEmptyView.visibility = View.VISIBLE
                    }
                }
                else -> {
                }
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_file_info_back -> {
                finish()
            }
        }
    }

    override fun onItemClick(v: View?, item: FileListItemBean) {

    }
}
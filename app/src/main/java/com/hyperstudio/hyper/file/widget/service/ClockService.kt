package com.hyperstudio.hyper.file.widget.service

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.app.Service
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.initKeepAliveNotificationChannel
import com.hyperstudio.hyper.file.widget.FunctionAppWidget
import java.text.SimpleDateFormat
import java.util.*


/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/4/27 22:08
 */
class ClockService: Service() {

    var firstStart = true

    // 定时器
    private var timer: Timer? = null

    // 日期格式
    @SuppressLint("SimpleDateFormat")
    private val hm: SimpleDateFormat = SimpleDateFormat("HH:mm")
    @SuppressLint("SimpleDateFormat")
    private val mdy: SimpleDateFormat = SimpleDateFormat("MM dd,yyyy")

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            initKeepAliveNotificationChannel()
            val keepServiceAliveNotification = NotificationCompat.Builder(this, "serviceLive").apply {
                setContentTitle("Hyper File Manager")
                setContentText("App is running")
                setWhen(System.currentTimeMillis())
                setSmallIcon(R.mipmap.keep_alive_notification_small_icon)
                setContentIntent(
                    PendingIntent.getActivity(
                        ActivityMgr.getContext(),
                        -1,
                        Intent(),
                        PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                    ))
            }.build()
            startForeground(4, keepServiceAliveNotification)
        }

        timer = Timer()
        /**
         * 參数：1.事件2.延时事件3.运行间隔事件
         */
        timer?.schedule(object : TimerTask() {
            override fun run() {
                updateView()
            }
        }, 0, 1000)
    }

    /**
     * 更新事件的方法
     */
    private fun updateView() {
        // 时间
        val time: String = hm.format(Date())
        val date: String = mdy.format(Date())

        /**
         * 參数：1.包名2.小组件布局
         */
        val rViews = RemoteViews(
            packageName,
            R.layout.widget_function_layout
        )
        // 显示当前事件
        if(time.isNotEmpty()){
            rViews.setTextViewText(R.id.tv_time, time)
        }
        if(date.isNotEmpty()){
            rViews.setTextViewText(R.id.tv_date, date)
        }

        // 刷新
        val manager = AppWidgetManager
            .getInstance(applicationContext)
        val cName = ComponentName(
            applicationContext,
            FunctionAppWidget::class.java
        )

        manager.updateAppWidget(cName, rViews)
    }

    override fun onDestroy() {
        super.onDestroy()
        timer = null
    }

}
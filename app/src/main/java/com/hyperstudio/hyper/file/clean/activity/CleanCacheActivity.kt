package com.hyperstudio.hyper.file.clean.activity

import android.content.Intent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.BR
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.admob.showInterstitialAd
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.adapter.SimpleAdapter
import com.hyperstudio.hyper.file.base_lib.base.ItemClickPresenter
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.clean.model.CleanCacheModel
import com.hyperstudio.hyper.file.clean.viewmodel.CleanCacheViewModel
import com.hyperstudio.hyper.file.complete.activity.FunctionSuccessActivity
import com.hyperstudio.hyper.file.databinding.CleanCacheLayoutBinding
import com.hyperstudio.hyper.file.push.notification.initSystemResidentNotification
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Name: CleanCacheActivity
 * @Description: 清理垃圾缓存模块
 * @Author: even@leanktech.com
 * @Date: 2021/9/26 17:22
 */
class CleanCacheActivity : BaseActivity<CleanCacheLayoutBinding>(),
    ItemClickPresenter<CleanCacheModel> {

    private val cleanCacheAppViewModel by viewModel<CleanCacheViewModel>()

    private var needClean: Boolean? = null

    private var appClick: Boolean = false

    override val showToolBar: Boolean
        get() = false

    /**
     * 清理种类list适配器
     */
    private val mAdapter by lazy {
        SimpleAdapter(
            R.layout.clean_cache_item,
            cleanCacheAppViewModel.cleanCacheList,
            BR.presenter,
            BR.item
        )
    }

    override fun getLayoutId() = R.layout.clean_cache_layout

    override fun initView() {
        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
            screenName = AnalyticsUtils.CLEAN_JUNK_BEGIN
        )
        bindingView.vm = cleanCacheAppViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        //设置顶部导航栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.color_1832BA).init()

        //设置adapter加载动画
        mAdapter.setAnimationWithDefault(BaseQuickAdapter.AnimationType.SlideInLeft)
        //动画是否仅第一次执行
        mAdapter.isAnimationFirstOnly = false

        bindingView.rvCleanTypeList.layoutManager = LinearLayoutManager(this)
        bindingView.rvCleanTypeList.adapter = mAdapter

        //progressbar走进度
        loadingViewInit()
    }

    override fun loadData(isRefresh: Boolean) {
        appClick = intent.getBooleanExtra("appClick", false)
        if (appClick) {
            AdUtils.loadAdObject(
                this,
                interstitialId = null,
                nativeId = GlobalInfo.ADMOB_CLEAN_ID_NATIVE_h
            )
        } else {
            AdUtils.loadAdObject(
                this,
                GlobalInfo.ADMOB_CLEAN_ID_INTERSTITIAL_h,
                GlobalInfo.ADMOB_CLEAN_ID_NATIVE_h
            )
        }
        cleanCacheAppViewModel.getAppCache {
            needClean = it
        }
    }

    private fun loadingViewInit() {
        cleanCacheAppViewModel.asyncSimpleAsync {
            var loading = true

            var loadingValue = 0

            var maxTime = 0

            var loadInterstitialAdB = false

            var loadInterstitialAdBTime = 0

            var loadNativeAdB = false

            var loadNativeAdBTime = 0

            while (loading) {
                val loadSuccess =
                    if (appClick) {
                        AdUtils.getNativeAdObjectSuccess() && needClean != null && loadingValue >= 300
                    } else {
                        AdUtils.getAdObjectSuccess() && needClean != null && loadingValue >= 300
                    }
                if (loadSuccess || maxTime > 10000) {
                    bindingView.pbCleanCacheLoading.progress = 300
                    loading = false
                    getAppCacheSuccess()
                } else {
                    if (loadingValue <= 300) {
                        bindingView.pbCleanCacheLoading.progress = loadingValue++
                    }
                    if (AdUtils.interstitialErrorMessage != null && AdUtils.interstitialErrorMessage?.isNotEmpty() == true) {
                        if (!loadInterstitialAdB) {
                            AdUtils.loadInterstitialAd(
                                this@CleanCacheActivity,
                                GlobalInfo.ADMOB_CLEAN_ID_INTERSTITIAL_b
                            )
                            loadInterstitialAdBTime = maxTime
                            loadInterstitialAdB = true
                        } else {
                            if (maxTime > 30000 || (maxTime - loadInterstitialAdBTime > 6000 && AdUtils.interstitialErrorMessage != null)) {
                                bindingView.pbCleanCacheLoading.progress = 300
                                loading = false
                                getAppCacheSuccess()
                                AdUtils.interstitialErrorMessage = null
                            }
                        }
                    }

                    if (AdUtils.nativeErrorMessage != null && AdUtils.nativeErrorMessage?.isNotEmpty() == true) {
                        if (!loadNativeAdB) {
                            AdUtils.loadNativeAd(
                                this@CleanCacheActivity,
                                GlobalInfo.ADMOB_CLEAN_ID_NATIVE_b
                            )
                            loadNativeAdB = true
                            loadNativeAdBTime = maxTime
                        } else {
                            if (maxTime > 30000 || (maxTime - loadNativeAdBTime > 6000 && AdUtils.nativeErrorMessage != null)) {
                                bindingView.pbCleanCacheLoading.progress = 300
                                loading = false
                                getAppCacheSuccess()
                                AdUtils.interstitialErrorMessage = null
                            }
                        }
                    }

                    delay(10)
                    maxTime += 10
                }
            }
        }.run { }
    }

    private fun getAppCacheSuccess() {
        //如果没有可清理内存直接走动画 finish
        needClean?.let {
            AnalyticsUtils.appPageBuriedPoint(
                eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                screenName = AnalyticsUtils.CLEAN_JUNK_END
            )

            if (AppBean.itemCategory == "Permanent") {
                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                    contentType = AppBean.contentType,
                    itemCategory = AppBean.itemCategory
                )
            } else {
                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                    contentType = AppBean.contentType,
                    itemCategory = AppBean.itemCategory,
                    itemId = AppBean.pushEmbeddingPoint
                )
            }

            intent.getStringExtra("pageEmbeddingPoint")?.let { pageEmbeddingPoint ->
                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                    contentType = "CleanJunk",
                    itemCategory = "Page",
                    pageEmbeddingPoint
                )
            }

            if (it) {
                bindingView.tvCleanCache.setVisibilityAnimation(View.VISIBLE, 1000)
            } else {
                bindingView.clCleanCacheView.setVisibilityAnimation(View.GONE, 1000)
                animSuccess()
            }
        }
    }

    private fun animSuccess() {
        cleanCacheAppViewModel.launchUI {

            bindingView.llSuccessViewBag.visibility = View.VISIBLE
            bindingView.dhvCleanSuccess.startAnim(500)
            delay(500)
            bindingView.dhvCleanSuccess.stopAnim()

            val moveAnimTitle: Animation = AnimationUtils.loadAnimation(
                this@CleanCacheActivity,
                R.anim.move_anim_down_to_up
            ).apply {
                duration = 650
            }
            bindingView.tvFunctionSuccessTitle.visibility = View.VISIBLE
            bindingView.tvFunctionSuccessTitle.startAnimation(moveAnimTitle)
            delay(650)

            val moveAnimSuccess: Animation = AnimationUtils.loadAnimation(
                this@CleanCacheActivity,
                R.anim.move_anim_down_to_up
            ).apply {
                duration = 650
            }
            bindingView.tvFunctionSuccessText.visibility = View.VISIBLE
            bindingView.tvFunctionSuccessText.startAnimation(moveAnimSuccess)
            delay(650)

            showInterstitialAd(this@CleanCacheActivity) {
                cleanCacheAppViewModel.launchUI {
                    startActivity(
                        Intent(
                            this@CleanCacheActivity,
                            FunctionSuccessActivity::class.java
                        ).apply {
                            putExtra("functionType", GlobalInfo.cleanFunction)
                        })
                    initSystemResidentNotification()
                    finish()
                }
            }
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_clean_cache -> {
                cleanCacheAppViewModel.launchUI {
                    bindingView.clCleanCacheView.setVisibilityAnimation(View.GONE, 1000)
                    delay(1000)
                    bindingView.clCleanCacheLoading.setVisibilityAnimation(View.VISIBLE, 1000)
                    bindingView.ivCleanCacheAnim.startFlick()
                    delay(1000)
                    cleanCacheAppViewModel.clearAppCache { success ->
                        cleanCacheAppViewModel.launchUI {
                            if (success) {
                                bindingView.clCleanCacheLoading.setVisibilityAnimation(
                                    View.GONE,
                                    1000
                                )
                                delay(1000)
                                bindingView.llSuccessViewBag.visibility = View.VISIBLE
                                animSuccess()
                            } else {
                                getString(R.string.error_text).showToast(this@CleanCacheActivity)
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onItemClick(v: View?, item: CleanCacheModel) {}

}
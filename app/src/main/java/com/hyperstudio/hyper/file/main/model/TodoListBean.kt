package com.hyperstudio.hyper.file.main.model

import com.chad.library.adapter.base.entity.MultiItemEntity

/**
 * @Name: TodoListBean
 * @Description:
 * @Author: even@leanktech.com
 * @Date: 2021/8/26 18:09
 */
class TodoListBean(override var itemType: Int) : MultiItemEntity {
    companion object {
        const val head = 1
        const val foot = 2
        const val banner = 3
    }
    var todoHeadText: String = ""
    var memoryUsageNum: String = ""
    var memoryNum: String = ""
    var todoFootItem: TodoItemBean?= null
}
package com.hyperstudio.hyper.file.clean.model

import androidx.databinding.BaseObservable
import java.io.Serializable

/**
 * @Name: CleanCacheModel
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/27 16:45
 */
class CleanCacheModel: BaseObservable(), Serializable {
    var cleanType: String ?= null
    var cleanSize: String ?= null
}
package com.hyperstudio.hyper.file.push.worker

import android.content.Context
import android.text.Html
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
//import com.hyperstudio.hyper.file.push.notification.initBoostPushNotification

/**
 * @Name: BoostDailyPushWorker
 * @Description: workManager 推送实现
 * @Author: even@leanktech.com
 * @Date: 2021/10/12 18:02
 */
class BoostDailyPushWorker(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
//        if (!AppBean.firstStartApp) {
//            initBoostPushNotification(
//                Html.fromHtml(
//                    ActivityMgr.getContext()
//                        .getString(R.string.html_notification_boost, AppBean.memoryNumber + "%")
//                ))
//        }
        return Result.success()
    }

}
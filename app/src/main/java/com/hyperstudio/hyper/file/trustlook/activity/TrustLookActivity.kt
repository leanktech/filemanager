package com.hyperstudio.hyper.file.trustlook.activity

import android.content.Intent
import android.view.KeyEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.admob.showInterstitialAd
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.bundlebean.DangerAppBundleBean
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.complete.activity.FunctionSuccessActivity
import com.hyperstudio.hyper.file.databinding.TrustLayoutBinding
import com.hyperstudio.hyper.file.push.notification.initSystemResidentNotification
import com.hyperstudio.hyper.file.trustlook.viewmodel.TrustLookViewModel
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import com.trustlook.sdk.Constants
import com.trustlook.sdk.cloudscan.CloudScanClient
import com.trustlook.sdk.cloudscan.CloudScanListener
import com.trustlook.sdk.data.AppInfo
import com.trustlook.sdk.data.Error
import com.trustlook.sdk.data.Region
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

/**
 * @Name: TrustLookActivity
 * @Description: 杀毒界面
 * @Author: even@leanktech.com
 * @Date: 2021/9/2 18:42
 */
class TrustLookActivity : BaseActivity<TrustLayoutBinding>() {

    private val trustViewModel by viewModel<TrustLookViewModel>()

    /**
     * 扫描工具实体类
     */
    private var cloudScanClient: CloudScanClient? = null

    //超时时间
    private val connectionTimeOut = 30000

    private val socketTimeOut = 30000

    private var scanSuccessList: MutableList<AppInfo>? = null

    private var scanSuccess = false

    private var dangerList: ArrayList<DangerAppBundleBean>? = null

    private var appClick = false

    override fun getLayoutId(): Int = R.layout.trust_layout

    override fun onLeftClick(view: View) {
        super.onLeftClick(view)
        cloudScanClient?.cancelScan()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_DOWN) {
            cloudScanClient?.cancelScan()
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun initView() {
        bindingView.vm = trustViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        setToolBar(leftImage = R.mipmap.icon_hyper_back)
        toolBar?.setLeftTxt(GlobalInfo.securityFunction)
        toolBar?.setToolbarColor(R.color.color_theme)
        toolBar?.setLeftTextSize(16F)
        //设置顶部导航栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.color_theme).init()

        bindingView.ivTrustAnim.startFlick()
    }

    override fun loadData(isRefresh: Boolean) {
        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
            screenName = AnalyticsUtils.SECURITY_BEGIN
        )

        //TODO 获取手机的apk文件 (记录循环方式有问题,可用MediaPlay尝试)
//        trustViewModel.getFolderDesc()

        trustViewModel.asyncSimpleAsync {
            appClick = intent.getBooleanExtra("appClick", false)
            if (appClick) {
                AdUtils.loadAdObject(
                    this@TrustLookActivity,
                    interstitialId = null,
                    nativeId = GlobalInfo.ADMOB_SECURITY_ID_NATIVE_h
                )
            } else {
                AdUtils.loadAdObject(
                    this@TrustLookActivity,
                    GlobalInfo.ADMOB_SECURITY_ID_INTERSTITIAL_h,
                    GlobalInfo.ADMOB_SECURITY_ID_NATIVE_h
                )
            }
        }.run { }

        cloudScanClient = CloudScanClient.Builder(this).setRegion(Region.INTL)
            .setConnectionTimeout(connectionTimeOut).setSocketTimeout(socketTimeOut).build()

        //直接开始快速扫描
        quickScanStart()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        v?.let {
            AppTool.singleClick(v) {
                when (v.id) {
                }
            }
        }
    }

    /**
     * 快速扫描
     */
    private fun quickScanStart() {
        trustViewModel.launchUI {
            addLoadingLogic()
            cloudScanClient?.startQuickScan(object : CloudScanListener() {
                override fun onScanStarted() {
                    "startQuickScan onScanStarted".print(tag = Constants.TAG)
                }

                override fun onScanProgress(curr: Int, total: Int, result: AppInfo?) {
                    "startQuickScan onScanProgress $curr/$total".print(tag = Constants.TAG)
                    when {
                        result?.packageName?.isNotEmpty() == true -> {
                            bindingView.tvScanContent.text = result.packageName
                        }
                        result?.apkPath?.isNotEmpty() == true -> {
                            bindingView.tvScanContent.text = result.apkPath
                        }
                        else -> {
                            bindingView.tvScanContent.text = getString(R.string.scanning_text)
                        }
                    }
                }

                override fun onScanError(errCode: Int) {
                    "startQuickScan onScanError $errCode".print(tag = Constants.TAG)

                    val errorMessage = when (errCode) {
                        Error.HOST_NOT_DEFINED -> {
                            "Error $errCode: HOST_NOT_DEFINED"
                        }
                        Error.INVALID_INPUT -> {
                            "Error  $errCode: INVALID_INPUT, no samples to scan"
                        }
                        Error.SERVER_NOT_AVAILABLE -> {
                            "Error $errCode: SERVER_NOT_AVAILABLE, please check the key in AndroidManifest.xml"
                        }
                        Error.JSON_EXCEPTION -> {
                            "Error $errCode: JSON_EXCEPTION"
                        }
                        Error.IO_EXCEPTION -> {
                            "Error $errCode: IO_EXCEPTION"
                        }
                        Error.NO_NETWORK -> {
                            "Error $errCode: NO_NETWORK"
                        }
                        Error.SOCKET_TIMEOUT_EXCEPTION -> {
                            "Error $errCode: SOCKET_TIMEOUT_EXCEPTION"
                        }
                        Error.INVALID_KEY -> {
                            "Error $errCode: INVALID_KEY, please check the key in AndroidManifest.xml"
                        }
                        Error.UNSTABLE_NETWORK -> {
                            "Error $errCode: UNSTABLE_NETWORK"
                        }
                        else -> {
                            "Error $errCode"
                        }
                    }
//                    finishScanViewInit(false, errorMessage)
                    //由于TrustLook SERVER_NOT_AVAILABLE 暂时这样解决
                    scanSuccess = true
                }

                override fun onScanCanceled() {
                    ToastTool.showToast(
                        getString(R.string.start_quick_scan_text), this@TrustLookActivity, 1
                    )
                }

                override fun onScanInterrupt() {}

                override fun onScanFinished(resultList: MutableList<AppInfo>?) {
                    "startQuickScan onScanFinished " + resultList?.size + " APPs".print(tag = Constants.TAG)
                    scanSuccessList = resultList
                    scanSuccess = true
                }
            })
        }
    }

    /**
     * 手机apk扫描
     */
    private fun folderScanStart(startTime: Long) {

        cloudScanClient?.startFolderScan(trustViewModel.apkFilePathList,
            object : CloudScanListener() {
                override fun onScanStarted() {
                    "startFolderScan onScanStarted".print(tag = Constants.TAG)
                }

                override fun onScanProgress(curr: Int, total: Int, result: AppInfo?) {
                    "startFolderScan onScanProgress $curr/$total".print(tag = Constants.TAG)
                    bindingView.tvScanContent.text = result?.apkPath
                }

                override fun onScanError(errCode: Int) {
                    "FolderScan onScanError $errCode".print(tag = Constants.TAG)

                    val errorMessage = when (errCode) {
                        Error.HOST_NOT_DEFINED -> {
                            "Error $errCode: HOST_NOT_DEFINED"
                        }
                        Error.INVALID_INPUT -> {
                            "Error  $errCode: INVALID_INPUT, no samples to scan"
                        }
                        Error.SERVER_NOT_AVAILABLE -> {
                            "Error $errCode: SERVER_NOT_AVAILABLE, please check the key in AndroidManifest.xml"
                        }
                        Error.JSON_EXCEPTION -> {
                            "Error $errCode: JSON_EXCEPTION"
                        }
                        Error.IO_EXCEPTION -> {
                            "Error $errCode: IO_EXCEPTION"
                        }
                        Error.NO_NETWORK -> {
                            "Error $errCode: NO_NETWORK"
                        }
                        Error.SOCKET_TIMEOUT_EXCEPTION -> {
                            "Error $errCode: SOCKET_TIMEOUT_EXCEPTION"
                        }
                        Error.INVALID_KEY -> {
                            "Error $errCode: INVALID_KEY, please check the key in AndroidManifest.xml"
                        }
                        Error.UNSTABLE_NETWORK -> {
                            "Error $errCode: UNSTABLE_NETWORK"
                        }
                        else -> {
                            "Error $errCode"
                        }
                    }
                }

                override fun onScanCanceled() {
                    ToastTool.showToast(
                        getString(R.string.start_folder_scan_text), this@TrustLookActivity, 1
                    )
                }

                override fun onScanInterrupt() {}

                override fun onScanFinished(resultList: MutableList<AppInfo>?) {
                    "startQuickScan onScanFinished " + resultList?.size + " APPs".print(tag = Constants.TAG)
                    scanSuccess = true
                    scanSuccessList = resultList
                }
            })
    }

    private fun loadingSuccess() {
        trustViewModel.launchUI {
            AnalyticsUtils.appPageBuriedPoint(
                eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                screenName = AnalyticsUtils.SECURITY_END
            )

            if (AppBean.itemCategory == "Permanent") {
                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                    contentType = AppBean.contentType,
                    itemCategory = AppBean.itemCategory
                )
            } else {
                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                    contentType = AppBean.contentType,
                    itemCategory = AppBean.itemCategory,
                    itemId = AppBean.pushEmbeddingPoint
                )
            }

            intent.getStringExtra("pageEmbeddingPoint")?.let {
                AnalyticsUtils.appFunctionBuriedPoint(
                    eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                    contentType = "Virus",
                    itemCategory = "Page",
                    it
                )
            }

            dangerList = scanSuccessList?.let { trustViewModel.updateDataList(it) }
            bindingView.tvFunctionSuccessText.text = when (dangerList?.size) {
                0 -> {
                    getString(R.string.security_success_text)
                }
                1 -> {
                    getString(R.string.security_fail_one_text)
                }
                else -> {
                    scanSuccessList?.size.toString() + getString(R.string.security_fail_many_text)
                }
            }

            bindingView.clAnimLayout.setVisibilityAnimation(View.GONE, 1000)
            delay(1000)
            bindingView.llSuccessViewBag.visibility = View.VISIBLE
            bindingView.dhvCleanSuccess.startAnim(500)
            delay(500)
            bindingView.dhvCleanSuccess.stopAnim()

            val moveAnimTitle: Animation = AnimationUtils.loadAnimation(
                this@TrustLookActivity, R.anim.move_anim_down_to_up
            ).apply {
                duration = 650
            }
            bindingView.tvFunctionSuccessTitle.visibility = View.VISIBLE
            bindingView.tvFunctionSuccessTitle.startAnimation(moveAnimTitle)
            delay(650)


            val moveAnimSuccess: Animation = AnimationUtils.loadAnimation(
                this@TrustLookActivity, R.anim.move_anim_down_to_up
            ).apply {
                duration = 650
            }
            bindingView.tvFunctionSuccessText.visibility = View.VISIBLE
            bindingView.tvFunctionSuccessText.startAnimation(moveAnimSuccess)
            delay(650)


            showInterstitialAd(this@TrustLookActivity) {
                trustViewModel.launchUI {
                    val intent =
                        Intent(this@TrustLookActivity, FunctionSuccessActivity::class.java).apply {
                            putExtra("functionType", GlobalInfo.securityFunction)
                        }
                    dangerList?.let {
                        intent.putExtra("dangerList", it)
                    }
                    if (getTodayDateStr() != AppBean.startSecurityToday) {
                        AppBean.startSecurityToday = getTodayDateStr()
                    }
                    initSystemResidentNotification()
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    private fun addLoadingLogic() {
        trustViewModel.asyncSimpleAsync {
            var loading = true

            var loadingValue = 0

            var maxTime = 0

            var loadInterstitialAdB = false

            var loadInterstitialAdBTime = 0

            var loadNativeAdB = false

            var loadNativeAdBTime = 0

            while (loading) {
                val loadingSuccess = if (appClick) {
                    AdUtils.getNativeAdObjectSuccess() && scanSuccess
                } else {
                    AdUtils.getAdObjectSuccess() && scanSuccess
                }

                if (loadingSuccess || maxTime > 10000) {
                    loading = false
                    trustViewModel.trustLoadingValue.value = "100%"
                    loadingSuccess()

                } else {
                    if (loadingValue <= 100) {
                        trustViewModel.trustLoadingValue.value = "$loadingValue%"
                        loadingValue++
                    }

                    if (AdUtils.interstitialErrorMessage != null && AdUtils.interstitialErrorMessage?.isNotEmpty() == true) {
                        if (!loadInterstitialAdB) {
                            AdUtils.loadInterstitialAd(
                                this@TrustLookActivity, GlobalInfo.ADMOB_SECURITY_ID_INTERSTITIAL_b
                            )
                            loadInterstitialAdBTime = maxTime
                            loadInterstitialAdB = true
                        } else {
                            if (maxTime > 8000 || (maxTime - loadInterstitialAdBTime > 6000 && AdUtils.interstitialErrorMessage != null)) {
                                if (scanSuccess) {
                                    loading = false
                                    trustViewModel.trustLoadingValue.value = "100%"
                                    loadingSuccess()
                                    AdUtils.interstitialErrorMessage = null
                                }
                            }
                        }
                    }

                    if (AdUtils.nativeErrorMessage != null && AdUtils.nativeErrorMessage?.isNotEmpty() == true) {
                        if (!loadNativeAdB) {
                            AdUtils.loadNativeAd(
                                this@TrustLookActivity, GlobalInfo.ADMOB_SECURITY_ID_NATIVE_b
                            )
                            loadNativeAdB = true
                            loadNativeAdBTime = maxTime
                        } else {
                            if (maxTime > 30000 || (maxTime - loadNativeAdBTime > 6000 && AdUtils.nativeErrorMessage != null)) {
                                if (scanSuccess) {
                                    loading = false
                                    trustViewModel.trustLoadingValue.value = "100%"
                                    loadingSuccess()
                                    AdUtils.interstitialErrorMessage = null
                                }
                            }
                        }
                    }
                    maxTime += 10
                    delay(10)
                }
            }
        }.run { }
    }

}
package com.hyperstudio.hyper.file.unit

import java.text.SimpleDateFormat
import java.util.*


/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/6/21 16:07
 */

fun getYYYYMMDDTime(): String {
    val currentTime = System.currentTimeMillis()
    val formatter = SimpleDateFormat("yyyy-MM-dd")
    val date = Date(currentTime)
    return formatter.format(date).toString()
}
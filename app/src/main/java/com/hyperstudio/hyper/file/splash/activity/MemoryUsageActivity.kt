package com.hyperstudio.hyper.file.splash.activity

import android.content.Intent
import android.view.View
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.databinding.ActivityMemoryUsageBinding
import com.hyperstudio.hyper.file.main.activity.MainActivity
import com.hyperstudio.hyper.file.splash.viewmodel.MemoryUsageViewModel
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import com.hyperstudio.hyper.file.unit.startCleanActivity
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Name:MemoryUsageActivity
 * @Description: 初次启动内存占用量页面
 * @Author: even@leanktech.com
 * @Date: 2021/11/10 16:27
 */
class MemoryUsageActivity : BaseActivity<ActivityMemoryUsageBinding>() {

    private val memoryUsageViewModel by viewModel<MemoryUsageViewModel>()

    override val showToolBar: Boolean
        get() = false

    override fun getLayoutId() = R.layout.activity_memory_usage

    override fun initView() {
        bindingView.vm = memoryUsageViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
    }

    override fun loadData(isRefresh: Boolean) {

        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
            screenName = AnalyticsUtils.RAM_SCAN_RESULT
        )

        memoryUsageViewModel.startMemoryFun(this)

        initProgressBar()

    }

    /**
     * 运用取步长 防止精度缺失
     */
    private fun initProgressBar(){
        //拿到总运行内存分成100份 1份的步长
        val memoryStepLength = memoryUsageViewModel.totalBytesUnit / 100
        //拿到已用运行内存空间占到100份中的几份
        val usedMemoryLength = memoryUsageViewModel.usedBytesUnit / memoryStepLength

        memoryUsageViewModel.launchUI {
            for(limit in 0..usedMemoryLength.toInt()){
                bindingView.cpbMemoryUsageLoading.setProgress(limit)
                delay(20)
            }
        }

        bindingView.tvMemoryUsedNumber.text = ("$usedMemoryLength%")
        bindingView.tvHintNumber.text = (("$usedMemoryLength%"))
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        v?.let {
            AppTool.singleClick(v) {
                when (v.id) {
                    R.id.tv_speed_btn -> {
                        startCleanActivity(this, GlobalInfo.boostFunction, true)

                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                            screenName = AnalyticsUtils.RAM_SCAN_BEGIN
                        )

                        AppBean.checkMemoryUsage = true
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.RAM_SCAN_RESULT,
                            itemName = AnalyticsUtils.RAM_SCAN_RESULT_BUTTON
                        )
                        finish()
                    }
                    R.id.iv_close_speed -> {
                        startActivity(Intent(this, MainActivity::class.java))
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.RAM_SCAN_RESULT,
                            itemName = AnalyticsUtils.RAM_SCAN_RESULT_SKIP
                        )
                        finish()
                    }
                }
            }
        }
    }

}
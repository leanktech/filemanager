package com.hyperstudio.hyper.file.complete.model

import android.graphics.drawable.Drawable
import androidx.databinding.BaseObservable
import java.io.Serializable

/**
 * @Name: CleanFootItemBean
 * @Description: 危险app item
 * @Author: even@leanktech.com
 * @Date: 2021/8/26 14:27
 */
class DangerAppItem: BaseObservable(), Serializable {
    var appIcon: Drawable ?= null
    var appName: String = ""
    var appCategory: String = ""
    var appSummary: String = ""
}
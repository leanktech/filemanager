package com.hyperstudio.hyper.file.trustlook.dialog

import android.text.method.LinkMovementMethod
import android.view.Gravity
import android.view.View
import androidx.fragment.app.FragmentManager
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.fragment.BaseDialogFragment
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.databinding.AgreeTrustDialogBinding
import com.hyperstudio.hyper.file.unit.AnalyticsUtils

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/5/12 10:45
 */
class AgreeTrustDialogFragment(private var callBack: () -> Unit) :
    BaseDialogFragment<AgreeTrustDialogBinding>() {

    /** 位置 */
    override val gravity = Gravity.CENTER

    override var dialogWidth: Int = (AppTool.getScreenWidth() * 0.9).toInt()

    override var dimAmount: Float = 0.7f

    override fun getLayoutId() = R.layout.agree_trust_dialog

    //规避Dialog init报错
    override fun show(manager: FragmentManager, tag: String?) {
        if(manager.isStateSaved){
            return
        }
        super.show(manager, tag)
    }

    override fun initView() {
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        bindingView.tvTrustHint.movementMethod = LinkMovementMethod.getInstance()

        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
            screenName = AnalyticsUtils.SECURITY_DIALOG_ACCESS
        )
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_trust_agree -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                    screenName = AnalyticsUtils.SECURITY_DIALOG_ACCESS,
                    itemName = AnalyticsUtils.DIALOG_SECURITY_ACCESS_AGREE
                )
                callBack()
                dismiss()
            }
            R.id.tv_trust_disagree -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                    screenName = AnalyticsUtils.SECURITY_DIALOG_ACCESS,
                    itemName = AnalyticsUtils.DIALOG_SECURITY_ACCESS_CANCEL
                )
                dismiss()
            }
        }
    }

}
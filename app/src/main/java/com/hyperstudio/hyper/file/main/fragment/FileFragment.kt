package com.hyperstudio.hyper.file.main.fragment

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter.AnimationType
import com.hyperstudio.hyper.file.BR
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.adapter.SimpleAdapter
import com.hyperstudio.hyper.file.base_lib.base.ItemClickPresenter
import com.hyperstudio.hyper.file.base_lib.fragment.BaseFragment
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.base_lib.tool.file.*
import com.hyperstudio.hyper.file.base_lib.tool.param.*
import com.hyperstudio.hyper.file.databinding.FragFileBinding
import com.hyperstudio.hyper.file.main.model.FileListItemBean
import com.hyperstudio.hyper.file.main.viewmodel.FileViewModel
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import com.hyperstudio.hyper.file.unit.SplashDataObject
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException
import java.util.*


/**
 * @Name: FileFragment
 * @Description: File模块
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */

class FileFragment : BaseFragment<FragFileBinding>(),
    ItemClickPresenter<FileListItemBean> {

    private val fileViewModel by viewModel<FileViewModel>()

    /**
     * 文件适配器
     */
    private val mAdapter by lazy {
        SimpleAdapter(
            R.layout.file_list_item,
            fileViewModel.fileList,
            BR.presenter,
            BR.item
        ).apply {
            itemPresenter = this@FileFragment
        }
    }

    override fun getLayoutId() = R.layout.frag_file

    override fun initView() {
        bindingView.vm = fileViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        mAdapter.setAnimationWithDefault(AnimationType.SlideInLeft)
        mAdapter.isAnimationFirstOnly = false
        bindingView.rvFolderList.apply {
            layoutManager = LinearLayoutManager(this@FileFragment.context)
            adapter = mAdapter
        }

        AnalyticsUtils.openSystemResidentBoost(
            AnalyticsUtils.APP_VIEW_EVENT_NAME,
            AnalyticsUtils.FILES
        )

    }

    override fun loadData(isRefresh: Boolean) {
        fileViewModel.launchUI {
            SplashDataObject.baseFileText?.let { fileViewModel.fileText.value = it }
            SplashDataObject.fileList?.let { fileViewModel.fileList.addAll(it) }
            SplashDataObject.baseFileText = null
            SplashDataObject.fileList = null
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        v?.let {
            AppTool.singleClick(v) {
                when (v.id) {
                    //返回上一层的view实现
                    R.id.tv_folder_now -> {
                        fileViewModel.launchUI {
                            showLoading()
                            try {
                                val fileText = bindingView.tvFolderNow.text.toString()
                                if (fileText != "/storage/emulated/0") {
                                    val folder: String =
                                        getParentPath(fileText) ?: ""
                                    fileViewModel.loadFolderList(folder) {
                                        dismissLoading()
                                    }
                                } else {
                                    dismissLoading()
                                }
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onItemClick(v: View?, item: FileListItemBean) {
        when (v?.id) {
            //进入子文件夹的view实现
            R.id.cl_file_item -> {
                fileViewModel.launchUI {
                    showLoading()
                    when(item.fName?.trim()){
                        "Android" -> {
                            AnalyticsUtils.appPageBuriedPoint(
                                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                screenName = AnalyticsUtils.FILES,
                                itemName = AnalyticsUtils.ANDROID_FILES
                            )
                        }
                        "DCIM" -> {
                            AnalyticsUtils.appPageBuriedPoint(
                                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                screenName = AnalyticsUtils.FILES,
                                itemName = AnalyticsUtils.DCIM_FILES
                            )
                        }
                        "Download" -> {
                            AnalyticsUtils.appPageBuriedPoint(
                                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                screenName = AnalyticsUtils.FILES,
                                itemName = AnalyticsUtils.DOWNLOAD_FILES
                            )
                        }
                        "Movies" -> {
                            AnalyticsUtils.appPageBuriedPoint(
                                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                screenName = AnalyticsUtils.FILES,
                                itemName = AnalyticsUtils.MOVIES_FILES
                            )
                        }
                        "Music" -> {
                            AnalyticsUtils.appPageBuriedPoint(
                                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                screenName = AnalyticsUtils.FILES,
                                itemName = AnalyticsUtils.MUSIC_FILES
                            )
                        }
                        "Pictures" -> {
                            AnalyticsUtils.appPageBuriedPoint(
                                eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                                screenName = AnalyticsUtils.FILES,
                                itemName = AnalyticsUtils.PICTURES_FILES
                            )
                        }
                    }
                    try {
                        if (item.fileIsDir == true) {
                            fileViewModel.launchUI {
                                fileViewModel.loadFolderList(item.fPath.toString()) {
                                }
                            }
                        }
                        dismissLoading()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

}
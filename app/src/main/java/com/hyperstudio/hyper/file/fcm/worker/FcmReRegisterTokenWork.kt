package com.hyperstudio.hyper.file.fcm.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.print
import com.hyperstudio.hyper.file.fcm.stopReRegisterTokenWork
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * @Date :2022/4/11
 * @Description:
 * @Author: jason@leanktech.com
 */
class FcmReRegisterTokenWork(private val appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {

    override fun doWork(): Result {
        registerToken()
        return Result.success()
    }

    private fun registerToken() {
        CoroutineScope(Dispatchers.Main).launch {
            ActivityMgr.currentActivity()?.let {
                val makeGooglePlayServicesAvailable =
                    GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(it)
                "当前google play server是否可用${makeGooglePlayServicesAvailable.isSuccessful}".print("FcmWork")
                if (makeGooglePlayServicesAvailable.isSuccessful) {
                    FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            AppBean.fcmIsRunning = false
                            "Fetching FCM registration token failed ${task.exception}".print()
                            return@OnCompleteListener
                        }
                        // Get new FCM registration token
                        val token = task.result
                        "当前注册->$token".print("FcmWork")
                        AppBean.fcmToken = token.toString()
                        AppBean.fcmIsRunning = true
                        stopReRegisterTokenWork()
                    })
                } else {
                    stopReRegisterTokenWork()
                }
            }
        }
    }
}
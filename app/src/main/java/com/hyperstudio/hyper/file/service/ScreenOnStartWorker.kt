package com.hyperstudio.hyper.file.service

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hyperstudio.hyper.file.base_lib.tool.printError
import com.hyperstudio.hyper.file.unit.startAliveService


/**
 * @Description:
 * author: Rhett
 * time: 2021/12/23
 */
class ScreenOnStartWorker (val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
        try {
            startAliveService(context)
        }catch (e: Exception){
            e.printStackTrace()
            e.toString().printError("启动亮屏监听服务")
        }
        return Result.success()
    }

}
package com.hyperstudio.hyper.file.trustlook.model

import com.chad.library.adapter.base.entity.MultiItemEntity

/**
 * @Name: TrustLookListBean
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/3 17:30
 */
class TrustLookListBean(override var itemType: Int) : MultiItemEntity {
    companion object {
        const val head = 1
        const val foot = 2
    }
    var trustLookHeadBean: TrustLookHeader ?= null
    var trustLookFootBean: TrustLookFoot ?= null
}
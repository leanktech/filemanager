package com.hyperstudio.hyper.file.fcm.service

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.hyperstudio.hyper.file.base_lib.tool.AppBean
import com.hyperstudio.hyper.file.base_lib.tool.print
import com.hyperstudio.hyper.file.fcm.showFcmNotification

/**
 * @Date :2022/4/11
 * @Description: fcm 接受通知
 * @Author: jason@leanktech.com
 */
class RegFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.isNotEmpty()) {
            //if ( /* Check if data needs to be processed by long running job */true) {
            //    // For long-running tasks (10 seconds or more) use WorkManager.
            //    scheduleJob()
            //} else {
            //    // Handle message within 10 seconds
            //    handleNow()
            //}
        }
        if (remoteMessage.notification != null) {
            remoteMessage.notification?.let {
                val title = it.title
                val body = it.body
                showFcmNotification(this,messageTitle = title, messageBody = body)
            }
        }
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()

    }

    override fun onNewToken(token: String) {
        sendRegistrationToServer(token)
    }

    private fun scheduleJob() {
//        val work = OneTimeWorkRequest.Builder(FcmWorker::class.java)
//            .build()
//        WorkManager.getInstance(this).beginWith(work).enqueue()
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        "Short lived task is done.".print()
    }

    private fun sendRegistrationToServer(token: String) {
        "当前打印toke 为$token".print("FcmWork")
        AppBean.fcmToken = token
    }
}
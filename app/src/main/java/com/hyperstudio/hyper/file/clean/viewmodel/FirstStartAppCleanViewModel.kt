package com.hyperstudio.hyper.file.clean.viewmodel

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Build
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.init
import kotlinx.coroutines.delay
import java.util.*

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/5/9 16:12
 */
class FirstStartAppCleanViewModel : BaseViewMode() {

    var firstCleanLoadingIcon = init(0)

    private var activityManager: ActivityManager? = null

    /**
     * 获取其他app以及kill掉的方法
     */
    @SuppressLint("DefaultLocale")
    fun cleanAnotherApp(callback: () -> Unit) {
        launchUI {
            activityManager = ActivityMgr.getContext()
                .getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                val appTasks = activityManager?.getRunningTasks(20)
                appTasks?.forEach {
                    it.topActivity?.packageName?.let { packageName ->
                        activityManager?.killBackgroundProcesses(packageName)
                    }
                    delay(300)
                }
            } else {
                val pm = ActivityMgr.getContext().packageManager
                // 查询所有已经安装的应用程序
                val listApplications: List<ApplicationInfo> =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        pm.getInstalledApplications(PackageManager.MATCH_UNINSTALLED_PACKAGES)
                    } else {
                        pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES)
                    }

                val packageNameList: MutableList<String> = mutableListOf()
                listApplications.forEach {
                    if (!it.packageName.contains("android") &&
                        !it.packageName.contains("system") &&
                        !it.packageName.contains("services") &&
                        !it.packageName.contains("com.sec") &&
                        !it.packageName.contains(Build.BRAND.toLowerCase(Locale.getDefault())) &&
                        !it.packageName.equals(ActivityMgr.getContext().packageName) &&
                        !it.packageName.contains("miui") &&
                        !it.packageName.contains("qualcomm") &&
                        !it.packageName.contains("vendor")
                    ) {
                        packageNameList.add(it.packageName)
                    }
                }

                packageNameList.forEach {
                    //捕获异常 有些app由于没有定义这样的类，或忘了在AndroidManifest中注册 会报NameNotFoundException
                    try {
                        activityManager?.killBackgroundProcesses(it)
                    } catch (e: Exception) {
                    }
                }
            }
            callback()
        }
    }

}
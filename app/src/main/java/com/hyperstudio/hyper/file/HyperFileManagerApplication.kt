package com.hyperstudio.hyper.file

import android.content.Context
import android.os.Environment
import androidx.multidex.MultiDexApplication
import com.alibaba.android.arouter.launcher.ARouter
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.getkeepsafe.relinker.ReLinker
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.hyperstudio.hyper.file.admob.AppStartupManager
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.tencent.mmkv.MMKV
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger

/**
 * application
 */
class HyperFileManagerApplication : MultiDexApplication() {

    val appsflyer_develop_key = "WjBHvsSe6ShFcnmgikRoBU"
    var isHasUploadInstallReferrerData = false

    override fun onCreate() {
        super.onCreate()
        initActivityManager()
        initKoin()
        initARouter()
        initWrapMMKV(this)
        initAppStartUpManager()
        initAppsFlyer()
    }

    private fun initAppsFlyer() {
        FirebaseApp.initializeApp(this)

        val firebaseAnalytics = Firebase.analytics

        if (!isHasUploadInstallReferrerData) {
            val conversionListener = object : AppsFlyerConversionListener {
                override fun onAppOpenAttribution(p0: MutableMap<String, String>?) {
                }

                override fun onConversionDataSuccess(p0: MutableMap<String, Any>?) {
                    p0?.let {
                        var isFirstLaunch = false
                        if (it.containsKey("is_first_launch")) {
                            isFirstLaunch = it.getValue("is_first_launch") as Boolean
                        }
                        isHasUploadInstallReferrerData = true
                        if (it.containsKey("media_source") && it.containsKey("af_status") && it.containsKey(
                                "campaign"
                            )
                        ) {
                            val mediaSource = it.getValue("media_source") as String
                            val afStatus = it.getValue("af_status") as String
                            val campaign = it.getValue("campaign") as String

                            if (afStatus.equals("Non-organic", true)) {
                                if (!mediaSource.equals(
                                        "google",
                                        true
                                    ) && !mediaSource.equals("googleadwords_int", true)
                                ) {
                                    if (isFirstLaunch) {
                                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.CAMPAIGN_DETAILS) {
                                            param(FirebaseAnalytics.Param.SOURCE, mediaSource)
                                            param(FirebaseAnalytics.Param.MEDIUM, "cpc")
                                            param(FirebaseAnalytics.Param.CAMPAIGN, campaign)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                override fun onConversionDataFail(p0: String?) {
                }

                override fun onAttributionFailure(p0: String?) {
                }
            }
//            AppsFlyerLib.getInstance().waitForCustomerUserId(true)
            AppsFlyerLib.getInstance().init(appsflyer_develop_key, conversionListener, this)
            AppsFlyerLib.getInstance().start(this)
        }
    }

    /**
     * 初始化MMKV
     * @param context Context
     */
    private fun initWrapMMKV(context: Context) {
        try {
            initMMKV(context)
        } catch (e: UnsatisfiedLinkError) {
            initMMKV(context) { s: String? -> ReLinker.loadLibrary(context, s) }
        }
    }

    private fun initMMKV(context: Context, loader: MMKV.LibLoader? = null) {
        try {
            val root = context.filesDir.absolutePath + "/mmkv"
            MMKV.initialize(root, loader)
        } catch (e: NullPointerException) {
            var cacheDir = context.cacheDir
            if (cacheDir == null) {
                cacheDir = Environment.getRootDirectory()
            }
            MMKV.initialize("$cacheDir/mmkv", loader)
        }
    }

    /**
     * 初始化Activity管理器
     */
    private fun initActivityManager() {
        ActivityMgr.init(this)
    }

    /**
     * 初始化Koin
     */
    private fun initKoin() {
        startKoin(this, appModule, logger = AndroidLogger())
    }

    private fun initARouter() {
        // 打印日志
        ARouter.openLog()
        // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
        ARouter.openDebug()
        // 尽可能早，推荐在Application中初始化
        ARouter.init(this)
    }

    private fun initAppStartUpManager() {
        //更新应用的启动manager 用于冷启动热启动操作
        AppStartupManager.init(this)
    }

}
package com.hyperstudio.hyper.file

import com.hyperstudio.hyper.file.clean.viewmodel.*
import com.hyperstudio.hyper.file.complete.viewmodel.FunctionSuccessViewModel
import com.hyperstudio.hyper.file.fileinfo.viewmodel.FileInfoViewModel
import com.hyperstudio.hyper.file.main.viewmodel.FileViewModel
import com.hyperstudio.hyper.file.main.viewmodel.MainViewModel
import com.hyperstudio.hyper.file.main.viewmodel.TodoViewModel
import com.hyperstudio.hyper.file.main.viewmodel.ToolsViewModel
import com.hyperstudio.hyper.file.splash.viewmodel.MemoryUsageViewModel
import com.hyperstudio.hyper.file.splash.viewmodel.SplashViewModel
import com.hyperstudio.hyper.file.trustlook.viewmodel.TrustLookViewModel
import com.hyperstudio.hyper.file.widget.viewmodel.AddWidgetViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * Koin配置文件
 * @author mazeyu
 * @email mazeyu8665@dingtalk.com
 * @date 2021/3/3
 * @since 1.0.0
 * @version 1.0.0
 */
/**
 * viewModel模块
 */
val viewModelModule = module {
    viewModel { MainViewModel() }
    viewModel { TodoViewModel() }
    viewModel { ToolsViewModel() }
    viewModel { FileViewModel() }
    viewModel { SplashViewModel() }
    viewModel { TrustLookViewModel() }
    viewModel { CleanCacheViewModel() }
    viewModel { FileInfoViewModel() }
    viewModel { FunctionSuccessViewModel() }
    viewModel { MemoryUsageViewModel() }
    viewModel { TargetedCleanViewModel() }
    viewModel { AddWidgetViewModel() }
    viewModel { FirstStartAppCleanViewModel() }
    viewModel { CleanAppViewModel() }
}

/**
 * 本地数据模块
 */
val localModule = module {

}

/**
 * 远程数据模块
 */
val remoteModule = module {

}

/**
 * 数据仓库模块
 */
val repoModule = module {

}

/**
 * 当需要构建你的ViewModel对象的时候，就会在这个容器里进行检索
 */
val appModule = listOf(viewModelModule, localModule, repoModule, remoteModule)
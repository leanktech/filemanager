package com.hyperstudio.hyper.file.splash.viewmodel

import android.app.ActivityManager
import android.content.Context
import android.text.format.Formatter
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.init
import com.hyperstudio.hyper.file.base_lib.tool.isNumeric
import java.io.BufferedReader
import java.io.FileReader
import java.lang.Exception
import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * @Name:MemoryUsageViewModel
 * @Description: view model
 * @Author: even@leanktech.com
 * @Date: 2021/11/10 16:30
 */
class MemoryUsageViewModel : BaseViewMode() {

    var usedMemory = init("")

    var availMemory = init("")

    var totalBytesUnit = 1000L
    private var availBytesUnit = 400L
    var usedBytesUnit = 0L

    fun startMemoryFun(context: Context){
        launchUI {
            getTotalMemory()
            getAvailMemory(context)
            getUsageMemory(context)
        }
    }

    /**
     * 获取android总运行内存大小
     * @param context
     */
    private fun getTotalMemory() {
        val str1 = "/proc/meminfo" // 系统内存信息文件
        val str2: String
        val arrayOfString: Array<String>
        try {
            val localFileReader = FileReader(str1)
            val localBufferedReader = BufferedReader(localFileReader, 8192)
            str2 = localBufferedReader.readLine() // 读取meminfo第一行，系统总内存大小
            arrayOfString = str2.split("\\s+").toTypedArray()
            // 获得系统总内存，单位是KB
            val p: Pattern = Pattern.compile("\\d+")
            val m: Matcher = p.matcher(arrayOfString[0])
            m.find()
            val i = m.group().toInt()
            //int值乘以1024转换为long类型 单位是B
            val initialMemory = i.toLong() * 1024
            localBufferedReader.close()
            if (initialMemory > 0 && isNumeric(initialMemory.toString())) {
                totalBytesUnit = initialMemory
            }
        } catch (e: Exception) {
        }
    }


    /**
     * 获取android当前可用运行内存大小
     * @param context
     */
    private fun getAvailMemory(context: Context) {
        try {
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val mi = ActivityManager.MemoryInfo()
            am.getMemoryInfo(mi)
            // mi.availMem; 当前系统的可用内存
            if (mi.availMem > 0 && isNumeric(mi.availMem.toString())) {
                availBytesUnit = mi.availMem
                availMemory.value = Formatter.formatFileSize(context, availBytesUnit)
            }else{
                availMemory.value = "4GB"
            }
        } catch (e: Exception) {

        }
    }

    /**
     * 获取android当前已用运行内存大小
     * @param context
     */
    private fun getUsageMemory(context: Context){
        try {
            usedBytesUnit = totalBytesUnit - availBytesUnit
            if(usedBytesUnit == 600L){
                usedMemory.value = "6GB"
            }else{
                usedMemory.value = Formatter.formatFileSize(context, usedBytesUnit)
            }
        }catch (e: Exception){

        }
    }

}
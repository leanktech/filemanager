package com.hyperstudio.hyper.file.clean.activity

import android.content.Intent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.admob.showInterstitialAd
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.clean.viewmodel.CleanAppViewModel
import com.hyperstudio.hyper.file.complete.activity.FunctionSuccessActivity
import com.hyperstudio.hyper.file.databinding.CoolerCleanBinding
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel

class CoolerCleanActivity : BaseActivity<CoolerCleanBinding>() {

    private val coolerCleanViewModel by viewModel<CleanAppViewModel>()

    //page埋点
    private var pageEmbeddingPoint: String? = null

    override fun getLayoutId() = R.layout.cooler_clean

    override fun initView() {
        bindingView.vm = coolerCleanViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        setToolBar(leftImage = R.mipmap.icon_hyper_back)
        toolBar?.setLeftTxt(GlobalInfo.coolerFunction)
        toolBar?.setToolbarColor(R.color.color_theme)
        toolBar?.setLeftTextSize(16F)
        bindingView.tvFunctionSuccessText.text = getString(R.string.cooler_success_text)
        //设置顶部导航栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.color_theme).init()
    }

    override fun loadData(isRefresh: Boolean) {
        coolerCleanViewModel.launchUI {
            pageEmbeddingPoint = intent.getStringExtra("pageEmbeddingPoint")

            //load Ad
            initAd()

            //第一部分埋点相关
            partOnePoint()

            //开始自定义闪烁动画
            startAnimation()

            //开始Loading动画
            AnalyticsUtils.appPageBuriedPoint(
                eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                screenName = AnalyticsUtils.COOLER_LOADING
            )
            repeat(101) {
                bindingView.cpbCoolerScanAppLoading.setProgress(it)
                delay(10)
            }

            startFun {
                coolerCleanViewModel.launchUI {
                    //结尾部分埋点相关
                    partEndPoint()

                    bindingView.clCoolerCleanAppBag.setVisibilityAnimation(View.GONE, 500)
                    delay(500)
                    bindingView.llSuccessViewBag.visibility = View.VISIBLE
                    bindingView.dhvCleanSuccess.startAnim(500)
                    delay(500)
                    bindingView.dhvCleanSuccess.stopAnim()

                    val moveAnimTitle: Animation = AnimationUtils.loadAnimation(
                        this@CoolerCleanActivity, R.anim.move_anim_down_to_up
                    ).apply {
                        duration = 650
                    }
                    bindingView.tvFunctionSuccessTitle.visibility = View.VISIBLE
                    bindingView.tvFunctionSuccessTitle.startAnimation(moveAnimTitle)
                    delay(650)

                    val moveAnimSuccess: Animation = AnimationUtils.loadAnimation(
                        this@CoolerCleanActivity, R.anim.move_anim_down_to_up
                    ).apply {
                        duration = 650
                    }
                    bindingView.tvFunctionSuccessText.visibility = View.VISIBLE
                    bindingView.tvFunctionSuccessText.startAnimation(moveAnimSuccess)
                    delay(650)

                    showInterstitialAd(this@CoolerCleanActivity) {
                        startActivity(
                            Intent(
                                this@CoolerCleanActivity, FunctionSuccessActivity::class.java
                            ).apply {
                                putExtra("functionType", GlobalInfo.coolerFunction)
                            })
                        finish()
                    }
                }
            }
        }
    }

    private fun initAd() {
        AdUtils.loadAdObject(
            this, GlobalInfo.ADMOB_COOLER_ID_INTERSTITIAL_h, GlobalInfo.ADMOB_COOLER_ID_NATIVE_h
        )
    }

    private fun partOnePoint() {
        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME, screenName = AnalyticsUtils.CPU_BEGIN
        )

        pageEmbeddingPoint?.let {
            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                contentType = "CPU",
                itemCategory = "Page",
                it
            )
        }

        if (getTodayDateStr() != AppBean.startCoolerToday) {
            AppBean.startCoolerToday = getTodayDateStr()
        }
    }

    private fun startAnimation() {
        bindingView.ivCoolerAnimOne.startFlick(800)
        bindingView.ivCoolerAnimTwo.startFlick(900)
        bindingView.ivCoolerAnimThree.startFlick(1000)
        bindingView.ivCoolerAnimFour.startFlick(1100)
        bindingView.ivCoolerAnimFive.startFlick(1200)
    }

    private fun startFun(callBack: () -> Unit) {
        coolerCleanViewModel.launchUI {

            bindingView.clCoolerLoadingBag.setVisibilityAnimation(View.GONE, 500)
            delay(500)
            bindingView.clCoolerCleanAppBag.setVisibilityAnimation(View.VISIBLE, 500)
            bindingView.ivRoundBg.rotateAnim()

            //clean another app
            AnalyticsUtils.appPageBuriedPoint(
                eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                screenName = AnalyticsUtils.COOLER_OPTIMIZE
            )
            var scanAppSuccess = false
            coolerCleanViewModel.getAnotherAppList {
                scanAppSuccess = true
            }

            coolerCleanViewModel.asyncSimpleAsync {
                var loading = true

                var maxTime = 0

                var loadInterstitialAdB = false

                var loadInterstitialAdBTime = 0

                var loadNativeAdB = false

                var loadNativeAdBTime = 0

                val appClick = intent.getBooleanExtra("appClick", false)

                while (loading) {

                    val loadingSuccess = if (appClick) {
                        AdUtils.getNativeAdObjectSuccess() && scanAppSuccess
                    } else {
                        AdUtils.getAdObjectSuccess() && scanAppSuccess
                    }

                    if (loadingSuccess || maxTime > 20000) {
                        loading = false
                        callBack()
                        //ad过多警告 所以app内点击功能模块不显示int广告了
                        if (!appClick) {

                            if (AdUtils.interstitialErrorMessage != null && AdUtils.interstitialErrorMessage?.isNotEmpty() == true) {
                                if (!loadInterstitialAdB) {

                                    AdUtils.loadInterstitialAd(
                                        this@CoolerCleanActivity,
                                        GlobalInfo.ADMOB_BATTERY_ID_INTERSTITIAL_b
                                    )

                                    loadInterstitialAdBTime = maxTime
                                    loadInterstitialAdB = true
                                } else {
                                    if (maxTime > 30000 || (maxTime - loadInterstitialAdBTime > 6000 && AdUtils.interstitialErrorMessage != null)) {
                                        if (scanAppSuccess) {
                                            loading = false
                                            callBack()
                                            AdUtils.interstitialErrorMessage = null
                                        }
                                    }
                                }
                            }
                        }

                        if (AdUtils.nativeErrorMessage != null && AdUtils.nativeErrorMessage?.isNotEmpty() == true) {
                            if (!loadNativeAdB) {

                                AdUtils.loadNativeAd(
                                    this@CoolerCleanActivity,
                                    GlobalInfo.ADMOB_BATTERY_ID_NATIVE_b
                                )

                                loadNativeAdB = true
                                loadNativeAdBTime = maxTime
                            } else {
                                if (maxTime > 30000 || (maxTime - loadNativeAdBTime > 6000 && AdUtils.nativeErrorMessage != null)) {
                                    if (scanAppSuccess) {
                                        loading = false
                                        callBack()
                                        AdUtils.nativeErrorMessage = null
                                    }
                                }
                            }
                        }
                    } else {
                        delay(10)
                        maxTime += 10
                    }
                }
            }.run { }

        }
    }

    private fun partEndPoint() {
        if (AppBean.itemCategory == "Permanent") {
            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                contentType = AppBean.contentType,
                itemCategory = AppBean.itemCategory
            )
        } else {
            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                contentType = AppBean.contentType,
                itemCategory = AppBean.itemCategory,
                itemId = AppBean.pushEmbeddingPoint
            )
        }

        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME, screenName = AnalyticsUtils.CPU_END
        )

        pageEmbeddingPoint?.let {
            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                contentType = "CPU",
                itemCategory = "Page",
                it
            )
        }
    }
}
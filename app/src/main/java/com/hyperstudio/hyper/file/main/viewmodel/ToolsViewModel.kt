package com.hyperstudio.hyper.file.main.viewmodel

import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr

/**
 * @Name: WorkViewModel
 * @Description: Work Vm
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */

class ToolsViewModel : BaseViewMode() {
    val mHeadIntro = ActivityMgr.getContext().getString(R.string.tools_head_clean_intro)
    val mHeadBtnText = ActivityMgr.getContext().getString(R.string.clean_now_large_text)

    val mSaverBtnText = ActivityMgr.getContext().getString(R.string.battery_text)

    val mCpuBtnText = ActivityMgr.getContext().getString(R.string.cooler_text)

    val mSafeBtnText = ActivityMgr.getContext().getString(R.string.security_text)

    val mCleanBtnText = ActivityMgr.getContext().getString(R.string.clean_text)

    val mCategoryImageText = ActivityMgr.getContext().getString(R.string.image_text)

    val mCategoryVideoText = ActivityMgr.getContext().getString(R.string.video_text)

    val mCategoryMusicText = ActivityMgr.getContext().getString(R.string.music_text)

    val mCategoryTxtText = ActivityMgr.getContext().getString(R.string.text)

    val mCategoryDownloadText = ActivityMgr.getContext().getString(R.string.download_text)
}
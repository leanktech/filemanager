package com.hyperstudio.hyper.file.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.fcm.worker.FcmReRegisterTokenWork
import com.hyperstudio.hyper.file.splash.activity.SplashActivity
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import java.util.concurrent.TimeUnit

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/5/16 17:13
 */

fun showFcmNotification(
    context: Context, messageTitle: String? = "",
    messageBody: String? = ""
) {
    //渠道先关
    val channelId = "Fcm Push" //应用频道Id唯一值， 长度若太长可能会被截断，
    val channelName = "Hyper Fcm Service" //最长40个字符，太长会被截断
    //通知对象
    val manager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Notification.Builder(context, channelId)
    } else {
        Notification.Builder(context)
    }
    //跳转意图
    val intent = Intent(context, SplashActivity::class.java)
    AnalyticsUtils.appFunctionBuriedPoint(
        eventName = AnalyticsUtils.PUSH_CLICK,
        itemCategory = AnalyticsUtils.PUSH_FCM
    )
    val pi = PendingIntent.getActivity(context, 1, intent, PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_CANCEL_CURRENT)
    //设置通知参数
    builder.setContentTitle(messageTitle)
    builder.setContentText(messageBody)
    builder.setSmallIcon(R.mipmap.notification_icon)
        .setLargeIcon(
            BitmapFactory.decodeResource(
                context.resources,
                R.mipmap.app_icon
            )
        )
        .setTicker(context.getString(R.string.app_name))
        .setWhen(System.currentTimeMillis())
        .setAutoCancel(true)
    builder.setContentIntent(pi)
    //Android 8.0 以上需包添加渠道
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val notificationChannel = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_DEFAULT
        )
        notificationChannel.setBypassDnd(true)
        notificationChannel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
        manager.createNotificationChannel(notificationChannel)
    }
    //show 通知
    manager.notify(100, builder.build())

}

fun starReRegisterTokenWork(context: Context) {
    val appBoostPushPeriodicWork =
        PeriodicWorkRequest.Builder(FcmReRegisterTokenWork::class.java, 30, TimeUnit.SECONDS)
            .addTag("RegisterFcmToken")
            .build()
    WorkManager.getInstance(context).enqueue(
        appBoostPushPeriodicWork
    )
}

/**
 * 停止注册 fcm toke work
 *
 */
fun stopReRegisterTokenWork() {
    //停止work
    WorkManager.getInstance().cancelAllWorkByTag("RegisterFcmToken")
}
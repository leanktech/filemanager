package com.hyperstudio.hyper.file.admob

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.OnPaidEventListener
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdView
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo

/**
 * @Name: AdPublicFun
 * @Description: 加载广告相关工具
 * @Author: even@leanktech.com
 * @Date: 2021/10/20 14:51
 */

/**
 * Populates a [NativeAdView] object with data from a given [NativeAd].
 *
 * @param nativeAd 广告对象
 * @param context 用于获取广告view
 */
fun initNativeAdView(nativeAd: NativeAd?, context: Context): NativeAdView? {

    if (nativeAd == null) {
        return null
    }

    val nativeAdView =
        LayoutInflater.from(context).inflate(R.layout.native_ad, null) as? NativeAdView

    return nativeAdView?.apply {
        // Set the media view. Media content will be automatically populated in the media view once
        // nativeAdView.setNativeAd() is called.
        mediaView = findViewById(R.id.ad_media)
        mediaView.setImageScaleType(ImageView.ScaleType.CENTER_INSIDE)

        // Set other ad assets.
        headlineView = findViewById(R.id.ad_headline)
        bodyView = findViewById(R.id.ad_body)
        callToActionView = findViewById(R.id.ad_call_to_action)
        iconView = findViewById(R.id.ad_app_icon)
        priceView = findViewById(R.id.ad_price)
//        starRatingView = findViewById(R.id.ad_stars)
        storeView = findViewById(R.id.ad_store)
        advertiserView = findViewById(R.id.ad_advertiser)

        // The headline is guaranteed to be in every NativeAd.
        (headlineView as? TextView)?.text = nativeAd.headline

        // These assets aren't guaranteed to be in every NativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.body == null) {
            bodyView?.visibility = View.GONE
        } else {
            bodyView?.visibility = View.VISIBLE
            (bodyView as TextView).text = nativeAd.body
        }
        if (nativeAd.callToAction == null) {
            callToActionView?.visibility = View.GONE
        } else {
            callToActionView?.visibility = View.VISIBLE
            (callToActionView as TextView).text = nativeAd.callToAction
        }
        if (nativeAd.icon == null) {
            iconView?.visibility = View.GONE
        } else {
            (iconView as ImageView).setImageDrawable(
                nativeAd.icon?.drawable
            )
            iconView?.visibility = View.VISIBLE
        }
//        if (nativeAd.price == null) {
//            priceView?.visibility = View.GONE
//        } else {
//            priceView?.visibility = View.VISIBLE
//            (priceView as TextView).text = nativeAd.price
//        }
//        if (nativeAd.store == null) {
//            storeView?.visibility = View.GONE
//        } else {
//            storeView?.visibility = View.VISIBLE
//            (storeView as TextView).text = nativeAd.store
//        }
//        if (nativeAd.starRating == null || nativeAd.starRating ?: 0.0 < 3) {
//            starRatingView?.visibility = View.GONE
//        } else {
//            (starRatingView as RatingBar).rating = nativeAd.starRating?.toFloat() ?: 0F
//            starRatingView?.visibility = View.VISIBLE
//        }
//        if (nativeAd.advertiser == null) {
//            advertiserView?.visibility = View.GONE
//        } else {
//            (advertiserView as TextView).text = nativeAd.advertiser
//            advertiserView?.visibility = View.VISIBLE
//        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad. The SDK will populate the nativeAdView's MediaView
        // with the media content from this native ad.
        setNativeAd(nativeAd)

        AdUtils.nativeAd = null
        AdUtils.nativeErrorMessage = null
    }
}

fun showInterstitialAd(context: Context, callBack: (String) -> Unit) {
    if (AdUtils.interstitialAd == null) {
        callBack("null")
    } else {
        AdUtils.interstitialAd?.let {
            it.fullScreenContentCallback =
                object : FullScreenContentCallback() {
                    //手动关闭广告的回调
                    override fun onAdDismissedFullScreenContent() {
                        super.onAdDismissedFullScreenContent()
                        callBack(GlobalInfo.AD_DISMISSED)
                        AdUtils.interstitialAd = null
                    }

                    //show广告 报错的回调
                    override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                        super.onAdFailedToShowFullScreenContent(p0)
                        callBack(GlobalInfo.AD_FAILED_SHOW)
                        AdUtils.interstitialAd = null
                    }

                }

            it.onPaidEventListener =
                OnPaidEventListener { adValue ->
                    Log.d(
                        "MyApp", String.format(
                            "impression-level ad revenue %d microcents in currency %s of precision %s%n occurred for ad unit %s from ad network %s.",
                            adValue.valueMicros,
                            adValue.currencyCode,
                            adValue.precisionType,
                            it.adUnitId,
                            it.responseInfo.mediationAdapterClassName
                        )
                    )
                }
            (context as? Activity)?.let { act ->
                it.show(act)
            }
        }
    }
}


fun showAppOpenAd(context: Context, callBack: (String) -> Unit) {
    AdUtils.appOpenAd?.let { appOpenAd ->
        appOpenAd.fullScreenContentCallback =
            object : FullScreenContentCallback() {
                override fun onAdDismissedFullScreenContent() {
                    callBack(GlobalInfo.AD_DISMISSED)
                    AdUtils.appOpenAd = null
                }

                override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                    callBack(GlobalInfo.AD_FAILED_SHOW)
                    AdUtils.appOpenAd = null
                }

                override fun onAdShowedFullScreenContent() {
                    callBack(GlobalInfo.AD_SHOWED_FULL)
                    AdUtils.appOpenAd = null
                }
            }
        (context as? Activity)?.let { act ->
            appOpenAd.show(act)
        }
    }
}

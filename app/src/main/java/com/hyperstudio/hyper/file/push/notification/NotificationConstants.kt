package com.hyperstudio.hyper.file.push.notification

/**
 * @Description:
 * author: Even
 * time: 2022/2/18
 */
object NotificationConstants {

    const val INTERVAL_NOTIFY_TIME = 60 * 60 * 1000L
    const val INTERVAL_CLEAN_TIME = 4 * 60 * 60 * 1000L
    const val INTERVAL_BOOST_TIME = 2 * 60 * 60 * 1000L
    const val INTERVAL_ANTIVIRUS_TIME = 10 * 60 * 60 * 1000L
    const val INTERVAL_COOL_CPU_TIME = 1 * 60 * 60 * 1000L
    const val securityPushIntervalTime = 4 * 60 * 60 * 1000L
    const val INTERVAL_PACKAGE_PUSH_TIME = 1 * 5 * 60 * 1000L
    const val INTERVAL_CHARGING_PROTECT_TIME = 5 * 60 * 1000L

}
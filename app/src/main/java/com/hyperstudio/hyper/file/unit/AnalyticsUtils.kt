package com.hyperstudio.hyper.file.unit

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.isNotNullEmpty

/**
 * @Description: 埋点工具类
 * author: Rhett
 * time: 2021/12/21
 */
object AnalyticsUtils {

    //AppClick event name
    const val APP_CLICK_EVENT_NAME = "AppClick"

    //AppView event name
    const val APP_VIEW_EVENT_NAME = "AppView"

    //Clean Begin event name
    const val CLEAN_BEGIN_EVENT_NAME = "Clean_Begin"

    //Clean End event name
    const val CLEAN_END_EVENT_NAME = "Clean_End"

    //首次启动页 event name
    const val FIRST_START_UP_EVENT_NAME = "FirstStartup"

    //首次启动页 event name
    const val START_UP_EVENT_NAME = "StartUp"

    //首次启动页-开始 screen name (AppClick)
    const val FIRST_START_UP_PAGE_SCREEN_NAME = "FirstStartupPage"

    //曝光-启动页 event name
    const val START_UP = "Startup"

    /**
     * Phone Boost
     */
    //曝光
    //内存扫描开始页 screen name
    const val RAM_SCAN_BEGIN = "RamScan_Begin"

    //内存占用页 screen name
    const val RAM_SCAN_RESULT = "RamScan_Result"

    //PhoneBoost进行页
    const val PHONE_BOOST_BEGIN = "PhoneBoost_Begin"

    //PhoneBoost结束页
    const val PHONE_BOOST_END = "PhoneBoost_End"

    //PhoneBoost结束后列表页
    const val PHONE_BOOST_END_LIST = "PhoneBoost_EndList"

    //点击
    //内存占用页-Boost Now
    const val RAM_SCAN_RESULT_BUTTON = "Button"

    //内存占用页-Skip
    const val RAM_SCAN_RESULT_SKIP = "Skip"

    //Boost Loading开始
    const val PHONE_BOOST_LOADING = "PhoneBoost_Begin"

    //Boost 优化开始
    const val PHONE_BOOST_OPTIMIZE = "PhoneBoost_Optimize"

    /**
     * Clean Junk
     */
    //曝光
    //CleanJunk进行页
    const val CLEAN_JUNK_BEGIN = "CleanJunk_Begin"

    //CleanJunk结束页
    const val CLEAN_JUNK_END = "CleanJunk_End"

    //CleanJunk结束后列表页
    const val CLEAN_JUNK_END_LIST = "CleanJunk_EndList"


    /**
     * Battery Saver
     */
    //曝光
    //Battery进行页
    const val BATTERY_BEGIN = "Battery_Begin"

    //Battery结束页
    const val BATTERY_END = "Battery_End"

    //Battery结束后列表页
    const val BATTERY_END_LIST = "Battery_EndList"

    //Battery Loading开始
    const val BATTERY_LOADING = "Battery_Begin"

    //Battery 优化开始
    const val BATTERY_OPTIMIZE = "Battery_Optimize"

    /**
     * CPU Cooler
     */
    //曝光
    //CPU进行页
    const val CPU_BEGIN = "CPU_Begin"

    //CPU结束页
    const val CPU_END = "CPU_End"

    //CPU结束页
    const val CPU_END_LIST = "CPU_EndList"

    //Cooler Loading开始
    const val COOLER_LOADING = "CPU_Begin"

    //Cooler 优化开始
    const val COOLER_OPTIMIZE = "CPU_Optimize"

    /**
     * Phone Security
     */
    //曝光
    //提示弹窗
    const val SECURITY_DIALOG_ACCESS = "Dialog_Security_Access"

    //Security进行页
    const val SECURITY_BEGIN = "Security_Begin"

    //Security结束页
    const val SECURITY_END = "Security_End"

    //Security结束后列表页
    const val SECURITY_END_LIST = "Security_EndList"

    //点击
    //提示弹窗-同意
    const val DIALOG_SECURITY_ACCESS_AGREE = "Agree"

    //提示弹窗-取消
    const val DIALOG_SECURITY_ACCESS_CANCEL = "Cancel"

    /**
     * 首页tab1-Todo页面
     */
    //曝光
    //首页tab1
    const val HOME = "Home"

    //点击
    //大圆圈
    const val HOME_CLEAN_NOW_CIRCLE = "CleanNow_Circle"

    //CleanNow按钮
    const val HOME_CLEAN_NOW_BUTTON = "CleanNow_Button"

    //Boost按钮
    const val HOME_BOOST = "Boost"

    //Battery Saver按钮
    const val HOME_BATTERY = "Battery"

    //CPU Cooler按钮
    const val HOME_CPU = "CPU"

    //Clean按钮
    const val HOME_CLEAN = "Clean"

    //Phone Security按钮
    const val HOME_VIRUS = "Virus"

    //Widget按钮
    const val HOME_WIDGET = "Widget"

    /**
     * 首页tab2-Tools页面
     */
    //曝光
    //首页tab2
    const val TOOLS = "Tools"

    //点击
    //BoostNow按钮
    const val TOOLS_BOOST_NOW_BUTTON = "BoostNow_Button"

    //BoostNow按钮
    const val TOOLS_CLEAN_NOW_BUTTON = "CleanNow_Button"

    //Clean
    const val TOOLS_CLEAN = "Clean"

    //Battery
    const val TOOLS_BATTERY = "Battery"

    //Cooler
    const val TOOLS_CPU = "CPU"

    //Security
    const val TOOLS_VIRUS = "Virus"

    //Image
    const val TOOLS_IMAGE = "Image"

    //Video
    const val TOOLS_VIDEO = "Video"

    //Music
    const val TOOLS_MUSIC = "Music"

    //Text
    const val TOOLS_TEXT = "Text"

    //Download
    const val TOOLS_DOWNLOAD = "Download"

    //WhatsApp
    const val TOOLS_WHATS_APP = "WhatsApp"

    //Google Play
    const val TOOLS_GOOGLE_PLAY = "GooglePlay"

    //YouTube
    const val TOOLS_YOUTUBE = "YouTube"

    //Tiktok
    const val TOOLS_TIKTOK = "TikTok"

    /**
     * 首页tab3-Files
     */
    //曝光
    //首页tab3
    const val FILES = "Files"
    //授权弹窗
    const val CLEAN_JUNK_DIALOG_ACCESS = "Dialog_Access"

    //点击
    //Android
    const val ANDROID_FILES = "AndroidFiles"

    //DCIM
    const val DCIM_FILES = "DCIMFiles"

    //Download
    const val DOWNLOAD_FILES = "DownloadFiles"

    //Movies
    const val MOVIES_FILES = "MoviesFiles"

    //Music
    const val MUSIC_FILES = "MusicFiles"

    //Pictures
    const val PICTURES_FILES = "PicturesFiles"

    //授权弹窗-同意
    const val DIALOG_ACCESS_ALLOW = "Allow"

    //授权弹窗-不同意
    const val DIALOG_ACCESS_DO_NOT_ALLOW = "Dont_Allow"

    /**
     * Widget
     */
    //曝光
    //Widget页
    const val WIDGET = "Widget"

    //首次引导添加快捷方式的对话框
    const val DIALOG_WIDGET = "Dialog_Widget"

    //点击
    //4*2Widget
    const val WIDGET_42 = "4*2"

    //4*1_1Widget
    const val WIDGET_411 = "4*1_1"

    //4*1_2Widget
    const val WIDGET_412 = "4*1_2"

    //手动添加教程
    const val WIDGET_TUTORIAL = "Tutorial"

    //首次引导添加快捷方式的对话框-添加
    const val DIALOG_WIDGET_ADD = "Add"

    //首次引导添加快捷方式的对话框-取消
    const val DIALOG_WIDGET_CANCEL = "Cancel"

    const val TOOLS_VALUE = "Tools"
    const val TODO_VALUE = "To_Do"
    const val END_LIST_VALUE = "EndList"

    /**
     * 功能点击埋点
     */

    /**
     * Card点击
     */
    //点击
    //PhoneBoost通知卡片
    const val CARD_PHONE_BOOST = "PhoneBoost"

    //CleanJunk通知卡片
    const val CARD_CLEAN_JUNK = "CleanJunk"

    //Battery通知卡片
    const val CARD_BATTERY = "Battery"

    //CPU通知卡片
    const val CARD_CPU = "CPU"

    //Security通知卡片
    const val CARD_SECURITY = "Virus"

    //Install通知卡片
    const val CARD_INSTALL = "Install"

    //Uninstall通知卡片
    const val CARD_UNINSTALL = "Uninstall"

    //Charge通知卡片
    const val CARD_CHARGE = "Charge"


    //FCM Push进入主页
    //通过Push进入Home
    const val PUSH_CLICK = "Push_Click"

    const val PUSH_FCM = "FCM"

    /**
     * 声明分析对象
     */
    private val mFirebaseAnalytics by lazy {
        FirebaseAnalytics.getInstance(ActivityMgr.getContext())
    }

    /**
     * 点击常驻通知的选项
     */
    fun openSystemResidentBoost(key: String, value: String) {
        val bundle = Bundle().apply {
            putString(key, value)
        }
        mFirebaseAnalytics.logEvent("notification", bundle)
    }

    private const val SCREEN_NAME = "SCREEN_NAME"

    private const val ITEM_NAME = "ITEM_NAME"

    fun appPageBuriedPoint(eventName: String, screenName: String? = null, itemName: String? = null) {
        val bundle = Bundle().apply {
            if (screenName.isNotNullEmpty() || itemName.isNotNullEmpty()) {
                screenName?.let {
                    putString(SCREEN_NAME, it)
                }
                itemName?.let {
                    putString(ITEM_NAME, it)
                }
            }
        }
        mFirebaseAnalytics.logEvent(eventName, bundle)
    }

    private const val CONTENT_TYPE = "CONTENT_TYPE"

    private const val ITEM_CATEGORY = "ITEM_CATEGORY"

    private const val ITEM_ID = "ITEM_ID"

    fun appFunctionBuriedPoint(eventName: String, contentType: String? = null, itemCategory: String? = null, itemId: String? = null) {
        val bundle = Bundle().apply {
            if (contentType.isNotNullEmpty() || itemCategory.isNotNullEmpty() || itemId.isNotNullEmpty()) {
                contentType?.let {
                    putString(CONTENT_TYPE, it)
                }
                itemCategory?.let {
                    putString(ITEM_CATEGORY, it)
                }
                itemId?.let {
                    putString(ITEM_ID, it)
                }
            }
        }
        mFirebaseAnalytics.logEvent(eventName, bundle)
    }

}
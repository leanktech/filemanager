package com.hyperstudio.hyper.file.clean.activity

import android.content.Intent
import android.view.KeyEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.gyf.immersionbar.ImmersionBar
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.admob.showInterstitialAd
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.clean.viewmodel.CleanAppViewModel
import com.hyperstudio.hyper.file.complete.activity.FunctionSuccessActivity
import com.hyperstudio.hyper.file.databinding.BoostCleanBinding
import com.hyperstudio.hyper.file.main.activity.MainActivity
import com.hyperstudio.hyper.file.unit.AnalyticsUtils
import kotlinx.coroutines.delay
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/10/13 11:10
 */
class BoostCleanActivity : BaseActivity<BoostCleanBinding>() {

    private val boostCleanViewModel by viewModel<CleanAppViewModel>()

    //是否为MemoryUsage页面跳转进入的Boost
    private var memoryUsage = false

    //是否为通知点击进入的Boost
    var notificationBoostTurn = false

    //page埋点
    private var pageEmbeddingPoint: String? = null

    override fun getLayoutId() = R.layout.boost_clean

    override fun onLeftClick(view: View) {
        super.onLeftClick(view)
        if (memoryUsage) {
            turnMainActivity()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_DOWN && memoryUsage) {
            turnMainActivity()
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun turnMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun initView() {
        bindingView.vm = boostCleanViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        setToolBar(leftImage = R.mipmap.icon_hyper_back)
        toolBar?.setLeftTxt(GlobalInfo.boostFunction)
        toolBar?.setToolbarColor(R.color.color_theme)
        toolBar?.setLeftTextSize(16F)
        bindingView.tvFunctionSuccessText.text = getString(R.string.phone_boost_text)
        //设置顶部导航栏颜色
        ImmersionBar.with(this).statusBarColor(R.color.color_theme).init()
    }

    override fun loadData(isRefresh: Boolean) {
        boostCleanViewModel.launchUI {
            memoryUsage = intent.getBooleanExtra("memoryUsage", false)
            pageEmbeddingPoint = intent.getStringExtra("pageEmbeddingPoint")

            //load Ad
            initAd()

            //第一部分埋点相关
            partOnePoint()

            //开始自定义闪烁动画
            startAnimation()

            //开始Loading动画
            AnalyticsUtils.appPageBuriedPoint(
                eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                screenName = AnalyticsUtils.PHONE_BOOST_LOADING
            )
            repeat(101) {
                bindingView.cpbBoostScanAppLoading.setProgress(it)
                delay(10)
            }

            startFun {
                boostCleanViewModel.launchUI {
                    //结尾部分埋点相关
                    partEndPoint()

                    bindingView.clBoostCleanAppBag.setVisibilityAnimation(View.GONE, 500)
                    delay(500)
                    bindingView.llSuccessViewBag.visibility = View.VISIBLE
                    bindingView.dhvCleanSuccess.startAnim(500)
                    delay(500)
                    bindingView.dhvCleanSuccess.stopAnim()

                    val moveAnimTitle: Animation = AnimationUtils.loadAnimation(
                        this@BoostCleanActivity,
                        R.anim.move_anim_down_to_up
                    ).apply {
                        duration = 650
                    }
                    bindingView.tvFunctionSuccessTitle.visibility = View.VISIBLE
                    bindingView.tvFunctionSuccessTitle.startAnimation(moveAnimTitle)
                    delay(650)

                    val moveAnimSuccess: Animation = AnimationUtils.loadAnimation(
                        this@BoostCleanActivity,
                        R.anim.move_anim_down_to_up
                    ).apply {
                        duration = 650
                    }
                    bindingView.tvFunctionSuccessText.visibility = View.VISIBLE
                    bindingView.tvFunctionSuccessText.startAnimation(moveAnimSuccess)
                    delay(650)

                    showInterstitialAd(this@BoostCleanActivity) {
                        startActivity(
                            Intent(
                                this@BoostCleanActivity,
                                FunctionSuccessActivity::class.java
                            ).apply {
                                putExtra("functionType", GlobalInfo.boostFunction)
                            }
                        )
                        finish()
                    }
                }
            }
        }
    }

    private fun initAd() {
        if (!memoryUsage) {
            if (AppBean.boostNotificationTurn) {
                AdUtils.loadAdObject(
                    this,
                    GlobalInfo.ADMOB_NOTIFICATION_BOOST_ID_INTERSTITIAL_h,
                    GlobalInfo.ADMOB_NOTIFICATION_BOOST_ID_NATIVE_h
                )
                notificationBoostTurn = true
                //如果是通知跳转 判断完要置为默认False 否则会影响其他需要判断通知跳转的逻辑
                AppBean.boostNotificationTurn = false
            } else {
                AdUtils.loadAdObject(
                    this,
                    GlobalInfo.ADMOB_BOOST_ID_INTERSTITIAL_h,
                    GlobalInfo.ADMOB_BOOST_ID_NATIVE_h
                )
                notificationBoostTurn = false
            }
        }
    }

    private fun partOnePoint() {
        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
            screenName = AnalyticsUtils.PHONE_BOOST_BEGIN
        )

        pageEmbeddingPoint?.let {
            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                contentType = "PhoneBoost",
                itemCategory = "Page",
                it
            )
        }

        if (getTodayDateStr() != AppBean.startBoostToday) {
            AppBean.startBoostToday = getTodayDateStr()
        }
    }

    private fun startAnimation() {
        bindingView.ivBoostAnimOne.startFlick(800)
        bindingView.ivBoostAnimTwo.startFlick(900)
        bindingView.ivBoostAnimThree.startFlick(1000)
        bindingView.ivBoostAnimFour.startFlick(1100)
        bindingView.ivBoostAnimFive.startFlick(1200)
    }

    private fun startFun(callBack: () -> Unit) {
        boostCleanViewModel.launchUI {

            bindingView.clBoostLoadingBag.setVisibilityAnimation(View.GONE, 500)
            delay(500)
            bindingView.clBoostCleanAppBag.setVisibilityAnimation(View.VISIBLE, 500)
            bindingView.ivRoundBg.rotateAnim()

            //clean another app
            AnalyticsUtils.appPageBuriedPoint(
                eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
                screenName = AnalyticsUtils.PHONE_BOOST_OPTIMIZE
            )
            var scanAppSuccess = false
            boostCleanViewModel.getAnotherAppList {
                scanAppSuccess = true
            }

            boostCleanViewModel.asyncSimpleAsync {
                var loading = true

                var maxTime = 0

                var loadInterstitialAdB = false

                var loadInterstitialAdBTime = 0

                var loadNativeAdB = false

                var loadNativeAdBTime = 0

                val appClick = intent.getBooleanExtra("appClick", false)

                while (loading) {

                    val loadingSuccess =
                        if (appClick) {
                            AdUtils.getNativeAdObjectSuccess() && scanAppSuccess
                        } else {
                            AdUtils.getAdObjectSuccess() && scanAppSuccess
                        }

                    if (memoryUsage) {
                        if ((AdUtils.getIntAdObjectSuccess() && scanAppSuccess) || maxTime > 8000) {
                            loading = false
                            callBack()
                        } else {
                            delay(10)
                            maxTime += 10
                        }
                    }

                        if (loadingSuccess || maxTime > 20000) {
                            loading = false
                            callBack()
                            //ad过多警告 所以app内点击功能模块不显示int广告了
                            if (!appClick) {

                                if (AdUtils.interstitialErrorMessage != null && AdUtils.interstitialErrorMessage?.isNotEmpty() == true) {
                                    if (!loadInterstitialAdB) {

                                        if (notificationBoostTurn) {
                                            AdUtils.loadInterstitialAd(
                                                this@BoostCleanActivity,
                                                GlobalInfo.ADMOB_NOTIFICATION_BOOST_ID_INTERSTITIAL_b
                                            )
                                        } else {
                                            AdUtils.loadInterstitialAd(
                                                this@BoostCleanActivity,
                                                GlobalInfo.ADMOB_BOOST_ID_INTERSTITIAL_b
                                            )
                                        }

                                        loadInterstitialAdBTime = maxTime
                                        loadInterstitialAdB = true
                                    } else {
                                        if (maxTime > 30000 || (maxTime - loadInterstitialAdBTime > 6000 && AdUtils.interstitialErrorMessage != null)) {
                                            if (scanAppSuccess) {
                                                loading = false
                                                callBack()
                                                AdUtils.interstitialErrorMessage = null
                                            }
                                        }
                                    }
                                }
                            }

                            if (AdUtils.nativeErrorMessage != null && AdUtils.nativeErrorMessage?.isNotEmpty() == true) {
                                if (!loadNativeAdB) {
                                    if (notificationBoostTurn) {
                                        AdUtils.loadNativeAd(
                                            this@BoostCleanActivity,
                                            GlobalInfo.ADMOB_NOTIFICATION_BOOST_ID_NATIVE_b
                                        )
                                    } else {
                                        AdUtils.loadNativeAd(
                                            this@BoostCleanActivity,
                                            GlobalInfo.ADMOB_BOOST_ID_NATIVE_b
                                        )
                                    }
                                    loadNativeAdB = true
                                    loadNativeAdBTime = maxTime
                                } else {
                                    if (maxTime > 30000 || (maxTime - loadNativeAdBTime > 6000 && AdUtils.nativeErrorMessage != null)) {
                                        if (scanAppSuccess) {
                                            loading = false
                                            callBack()
                                            AdUtils.nativeErrorMessage = null
                                        }
                                    }
                                }
                            }
                        }else{
                            delay(10)
                            maxTime += 10
                        }
                }
            }.run { }

        }
    }

    private fun partEndPoint() {
        if (AppBean.itemCategory == "Permanent") {
            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                contentType = AppBean.contentType,
                itemCategory = AppBean.itemCategory
            )
        } else {
            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                contentType = AppBean.contentType,
                itemCategory = AppBean.itemCategory,
                itemId = AppBean.pushEmbeddingPoint
            )
        }

        AnalyticsUtils.appPageBuriedPoint(
            eventName = AnalyticsUtils.APP_VIEW_EVENT_NAME,
            screenName = AnalyticsUtils.PHONE_BOOST_END
        )

        pageEmbeddingPoint?.let {
            AnalyticsUtils.appFunctionBuriedPoint(
                eventName = AnalyticsUtils.CLEAN_END_EVENT_NAME,
                contentType = "PhoneBoost",
                itemCategory = "Page",
                it
            )
        }
    }

}
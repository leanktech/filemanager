package com.hyperstudio.hyper.file.trustlook.model

import androidx.databinding.BaseObservable
import java.io.Serializable

/**
 * @Name: TrustLookHeader
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/3 17:34
 */
class TrustLookHeader: BaseObservable(), Serializable {
    var headTotalFile: String = ""
    var headInvalid: String = ""
    var headMalware: String = ""
    var headPua: String = ""
    var headBenign: String = ""
    var headMillis: String = ""
}
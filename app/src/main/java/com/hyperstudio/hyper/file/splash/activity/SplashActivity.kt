package com.hyperstudio.hyper.file.splash.activity

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Paint
import android.os.BatteryManager
import android.os.Build
import android.util.Base64
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.hyperstudio.hyper.file.BuildConfig
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.admob.AppStartupManager
import com.hyperstudio.hyper.file.base_lib.activity.BaseActivity
import com.hyperstudio.hyper.file.base_lib.tool.*
import com.hyperstudio.hyper.file.broadcast.AppAddRemoveBroadcastReceiver
import com.hyperstudio.hyper.file.broadcast.ScreenOnBroadcastReceiver
import com.hyperstudio.hyper.file.clean.activity.FirstStartAppCleanActivity
import com.hyperstudio.hyper.file.databinding.ActivitySplashBinding
import com.hyperstudio.hyper.file.fcm.starReRegisterTokenWork
import com.hyperstudio.hyper.file.h5.H5Activity
import com.hyperstudio.hyper.file.main.activity.MainActivity
import com.hyperstudio.hyper.file.push.worker.*
import com.hyperstudio.hyper.file.service.ScreenOnStartWorker
import com.hyperstudio.hyper.file.splash.viewmodel.SplashViewModel
import com.hyperstudio.hyper.file.unit.*
import com.lzf.easyfloat.EasyFloat
import com.lzf.easyfloat.enums.ShowPattern
import com.lzf.easyfloat.enums.SidePattern
import com.orhanobut.logger.LogLevel
import com.orhanobut.logger.Logger
import kotlinx.coroutines.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeUnit


/**
 * @Name: SplashActivity
 * @Description: 初始占位页
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 16:18
 */
class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    private val splashViewModel by viewModel<SplashViewModel>()

    private val mBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, intent: Intent?) {
            if (intent == null) {
                return
            }
            val action = intent.action
            if (Intent.ACTION_BATTERY_CHANGED == action) {
                // 电池温度
                AppBean.temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1).let {
                    if (it > 100) {
                        (it / 10).toString()
                    } else {
                        50.toString()
                    }
                }
            }
        }
    }

    override val showToolBar: Boolean
        get() = false

    override fun getLayoutId() = R.layout.activity_splash

    override fun initView() {
        bindingView.vm = splashViewModel
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
        bindingView.ab = AppBean

        bindingView.tvPrivacyPolicy.paint.flags = Paint.UNDERLINE_TEXT_FLAG
        bindingView.tvUserAgreement.paint.flags = Paint.UNDERLINE_TEXT_FLAG

        bindingView.ivStartCheckBtn.isSelected = true
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun loadData(isRefresh: Boolean) {
        splashViewModel.launchUI {

            AnalyticsUtils.appPageBuriedPoint(eventName = AnalyticsUtils.START_UP)

            AppBean.pushEmbeddingPoint = intent.getStringExtra("embeddingPoint") ?: ""

            AppBean.itemCategory = intent.getStringExtra("itemCategory") ?: ""

            AppBean.contentType = intent.getStringExtra("contentType") ?: ""

            intent.getStringExtra("embeddingPoint")?.let { embeddingPoint ->
                if (AppBean.itemCategory == "Permanent") {
                    AnalyticsUtils.appFunctionBuriedPoint(
                        eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                        contentType = AppBean.contentType,
                        itemCategory = AppBean.itemCategory
                    )
                } else {
                    AnalyticsUtils.appFunctionBuriedPoint(
                        eventName = AnalyticsUtils.CLEAN_BEGIN_EVENT_NAME,
                        contentType = AppBean.contentType,
                        itemCategory = AppBean.itemCategory,
                        itemId = embeddingPoint
                    )
                }
            }


            AppBean.appStartUpType = intent.getStringExtra("appStartUp") ?: GlobalInfo.COLD_BOOT_TAG

            AppBean.residentNotificationChoiceType = intent.getStringExtra("functionType") ?: ""

            AppBean.boostNotificationTurn = intent.getBooleanExtra("notificationBoostTurn", false)

            registerReceiver(
                mBroadcastReceiver,
                IntentFilter(Intent.ACTION_BATTERY_CHANGED)
            )

            AppBean.memoryNumber = SplashDataObject.getMemoryUsedPercentage(this@SplashActivity)

            initComponent()

            //加载广告
            if (AppBean.residentNotificationChoiceType.isEmpty() || AppBean.firstStartApp) {
                initAd()
            }

            if (AppBean.startAppFirstTime) {
                AnalyticsUtils.appPageBuriedPoint(
                    AnalyticsUtils.FIRST_START_UP_EVENT_NAME
                )
                AppBean.startAppFirstTime = false
            }else{
                AnalyticsUtils.appPageBuriedPoint(
                    AnalyticsUtils.START_UP_EVENT_NAME
                )
            }

            if (!AppBean.firstAgreeAgreement) {
                bindingView.rlAgreement.visibility = View.VISIBLE
            } else {
                bindingView.clAnimLayout.visibility = View.VISIBLE
                initNotification()
                //init亮屏监控推送通知相关
                initScreenOnMonitor()
                getPermissionForApp(true)
            }

            //获取advId
//        determineAdvertisingInfo()
        }

    }

    /**
     * init亮屏监控推送通知相关
     * */
    private fun initScreenOnMonitor() {
        startAliveService(this)

        // 第二个参数是间隔时间，第三个参数是第二个参数的单位
        val appScreenOnMonitorWork =
            PeriodicWorkRequest.Builder(ScreenOnStartWorker::class.java, 15, TimeUnit.MINUTES)
                .build()

        WorkManager.getInstance(this@SplashActivity).enqueueUniquePeriodicWork(
            "ScreenOnMonitor",
            ExistingPeriodicWorkPolicy.KEEP,
            appScreenOnMonitorWork
        )
    }

    /**
     * init组件
     */
    private fun initComponent() {
        if (AppBean.appStartUpType == GlobalInfo.COLD_BOOT_TAG) {
            /**
             * initAdmob
             */
            MobileAds.initialize(
                applicationContext
            ) {
                "MobileAds init success$it".print()
            }

            //更新应用的启动manager 用于冷启动热启动操作
            AppStartupManager.init(application)

            /**
             * 初始化日志
             */
            if (BuildConfig.DEBUG) {
                Logger.init(this.packageName).methodOffset(2).methodCount(1).hideThreadInfo()
            } else {
                Logger.init().logLevel(LogLevel.NONE)
            }
        }
        initFcmRegisterToken()
    }

    /**
     * 注册fcm token
     */
    private fun initFcmRegisterToken() {
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                "Fetching FCM registration token failed ${task.exception}".print()
                starReRegisterTokenWork(applicationContext)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            val token = task.result
            "当前注册->$token".print("FcmWork")
            AppBean.fcmToken = token.toString()
            AppBean.fcmIsRunning = true
        })
    }

    private fun startHomeActivity() {
//        if (AppBean.firstStartApp) {
////            showInterstitialAd(this@SplashActivity) {
//            AppBean.firstStartAppTime = System.currentTimeMillis()
//            startActivity(Intent(this@SplashActivity, FirstStartAppCleanActivity::class.java))
//            finish()
////            }
//        } else {
//            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
//            finish()
//        }
        if (AppBean.firstStartApp) {
            AppBean.firstStartAppTime = System.currentTimeMillis()
        }
        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        finish()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_privacy_policy -> {
                val intent = Intent(this, H5Activity::class.java).apply {
                    putExtra("h5url", "https://hyper-filemanager.web.app/privacy.html")
                }
                startActivity(intent)
            }
            R.id.tv_user_agreement -> {
                val intent = Intent(this, H5Activity::class.java).apply {
                    putExtra("h5url", "https://hyper-filemanager.web.app/terms.html")
                }
                startActivity(intent)
            }
            R.id.iv_start_check_btn -> {
                bindingView.ivStartCheckBtn.isSelected = !bindingView.ivStartCheckBtn.isSelected
            }
            R.id.btn_start -> {
                if (bindingView.ivStartCheckBtn.isSelected) {
                    if (AppBean.firstStartApp) {
                        AnalyticsUtils.appPageBuriedPoint(
                            eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                            screenName = AnalyticsUtils.FIRST_START_UP_PAGE_SCREEN_NAME
                        )
                    }
                    AppBean.firstAgreeAgreement = true
//                    getPermissionForApp(false)
                    initNotification()
                    //init亮屏监控推送通知相关
                    initScreenOnMonitor()
                    splashViewModel.asyncSimpleAsync {
                        //加载文件列表
                        SplashDataObject.loadFolderList()
                    }.run { }
                    startHomeActivity()
                } else {
                    getString(R.string.splash_agree_hint).showToast()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getPermissionForApp(successStartApp: Boolean) {
        splashViewModel.launchUI {

            // 权限检测
//                        if (!PermissionUtils.checkPermission(this@SplashActivity)) {
//                            // 权限申请，参数2为权限回调接口
//                            PermissionUtils.requestPermission(this@SplashActivity,
//                                object : OnPermissionResult {
//                                    override fun permissionResult(isOpen: Boolean) {
//                                        if (isOpen) {
//                                            createEasyFloat()
//                                        }
//                                    }
//
//                                })
//                        } else {
//                            createEasyFloat()
//                        }
            if (!successStartApp) {
                bindingView.rlAgreement.setVisibilityAnimation(View.GONE, 500)
                delay(500)
            }
            bindingView.clAnimLayout.setVisibilityAnimation(View.VISIBLE, 500)

            splashViewModel.asyncSimpleAsync {
                //加载文件列表
                SplashDataObject.loadFolderList()
            }.run { }

            var loading = true
            var loadingValue = 0
            var maxTime = 0
            while (loading) {
                val showSuccess =
                    showAd() && SplashDataObject.baseFileText?.isNotEmpty() == true && SplashDataObject.fileList?.isNotEmpty() == true
                if (showSuccess || maxTime > 8000) {
                    loading = false
                    bindingView.pbLoading.progress = 1200
                    bindingView.tvLoadingLimit.text = "100%"
                    startHomeActivity()
                } else {
                    val loadingLimit = loadingValue / 12
                    if (loadingLimit in 1..100) {
                        bindingView.tvLoadingLimit.text = "$loadingLimit%"
                    }
                    delay(10)
                    maxTime += 10
                    bindingView.pbLoading.progress = loadingValue++
                }
            }
        }
    }

    private fun initAd() {
        splashViewModel.asyncSimpleAsync {
            if (AppBean.appStartUpType == GlobalInfo.HOT_START_TAG) {
                AdUtils.loadAppOpenAd(this@SplashActivity, GlobalInfo.ADMOB_HOT_START_ID)
            } else {
                if (AppBean.firstStartApp) {
                    AdUtils.loadInterstitialAd(
                        this@SplashActivity,
                        GlobalInfo.FIRST_START_ADMOB_COLD_BOOT_ID
                    )
                } else {
                    AdUtils.loadInterstitialAd(this@SplashActivity, GlobalInfo.ADMOB_COLD_BOOT_ID)
                }
            }
            AdUtils.loadBannerAd(
                this@SplashActivity,
                GlobalInfo.BANNER_TODO_ID_BANNER,
                AdSize.MEDIUM_RECTANGLE
            )
        }.run { }
    }

    private fun showAd(): Boolean {
        if (AppBean.residentNotificationChoiceType.isNotEmpty()) {
            return true
        }
        when (AppBean.appStartUpType) {
            GlobalInfo.HOT_START_TAG -> {
                return AdUtils.appOpenAd != null && AdUtils.bannerAd != null
            }
            GlobalInfo.COLD_BOOT_TAG -> {
                return AdUtils.interstitialAd != null && AdUtils.bannerAd != null
            }
        }
        return false
    }

    private fun initNotification() {
        /**
         * initNotification
         */
        initWorkManager(GlobalInfo.NOTIFICATION_SYSTEM_RESIDENT)

        initSystemResidentNotificationChannel()
        initFunctionPushNotificationChannel(this@SplashActivity)
//        initWorkManager(GlobalInfo.boostFunction)
//        initWorkManager(GlobalInfo.cleanFunction)
//        initWorkManager(GlobalInfo.securityFunction)
//        initWorkManager(GlobalInfo.coolerFunction)
//        initWorkManager(GlobalInfo.NOTIFICATION_TARGETED_CARD)
    }

    //创建workManager管理后台任务
    private fun initWorkManager(type: String) {
        when (type) {
            GlobalInfo.NOTIFICATION_SYSTEM_RESIDENT -> {
                // 第二个参数是间隔时间，第三个参数是第二个参数的单位
                val appSystemPushPeriodicWork =
                    PeriodicWorkRequest.Builder(
                        SystemNotificationPushWorker::class.java,
                        15,
                        TimeUnit.MINUTES
                    )
                        .build()

                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                    GlobalInfo.NOTIFICATION_SYSTEM_RESIDENT,
                    ExistingPeriodicWorkPolicy.KEEP,
                    appSystemPushPeriodicWork
                )
            }
//            GlobalInfo.boostFunction -> {
//                val appBoostPushPeriodicWork =
//                    PeriodicWorkRequest.Builder(
//                        BoostDailyPushWorker::class.java,
//                        12,
//                        TimeUnit.HOURS
//                    )
//                        .build()
//
//                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
//                    GlobalInfo.boostPushWorkName,
//                    ExistingPeriodicWorkPolicy.KEEP,
//                    appBoostPushPeriodicWork
//                )
//            }

            GlobalInfo.cleanFunction -> {
                val appCleanPushPeriodicWork =
                    PeriodicWorkRequest.Builder(
                        CleanDailyPushWorker::class.java,
                        11,
                        TimeUnit.HOURS
                    )
                        .build()

                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                    GlobalInfo.cleanPushWorkName,
                    ExistingPeriodicWorkPolicy.KEEP,
                    appCleanPushPeriodicWork
                )
            }

//            GlobalInfo.saverFunction -> {
//                val appBatteryPushPeriodicWork =
//                    PeriodicWorkRequest.Builder(
//                        BatteryDailyPushWorker::class.java,
//                        14,
//                        TimeUnit.HOURS
//                    ).build()
//
//                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
//                    GlobalInfo.batteryPushWorkName,
//                    ExistingPeriodicWorkPolicy.KEEP,
//                    appBatteryPushPeriodicWork
//                )
//            }

//            GlobalInfo.coolerFunction -> {
//                val appCoolerPushPeriodicWork =
//                    PeriodicWorkRequest.Builder(
//                        CoolerDailyPushWorker::class.java,
//                        15,
//                        TimeUnit.HOURS
//                    ).build()
//
//                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
//                    GlobalInfo.coolerPushWorkName,
//                    ExistingPeriodicWorkPolicy.KEEP,
//                    appCoolerPushPeriodicWork
//                )
//            }

            GlobalInfo.securityFunction -> {
                val appSecurityPushPeriodicWork =
                    PeriodicWorkRequest.Builder(
                        SecurityDailyPushWorker::class.java,
                        16,
                        TimeUnit.HOURS
                    ).build()

                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                    GlobalInfo.securityPushWorkName,
                    ExistingPeriodicWorkPolicy.KEEP,
                    appSecurityPushPeriodicWork
                )
            }

            GlobalInfo.NOTIFICATION_TARGETED_CARD -> {
                val appTargetedPushPeriodicWork =
                    PeriodicWorkRequest.Builder(
                        TargetedDailyPushWorker::class.java,
                        10,
                        TimeUnit.HOURS
                    )
                        .build()

                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                    GlobalInfo.NOTIFICATION_TARGETED_CARD,
                    ExistingPeriodicWorkPolicy.KEEP,
                    appTargetedPushPeriodicWork
                )
            }
        }

    }

    private fun createEasyFloat() {
        EasyFloat.with(this)
            .setLayout(R.layout.boost_easy_float) {
                val closeView = it.findViewById<ImageView>(R.id.iv_boost_close_easy_float)
                val openView = it.findViewById<LinearLayout>(R.id.ll_open_easy_float)
                closeView.setOnClickListener {
                    closeView.visibility = View.GONE
                    openView.visibility = View.VISIBLE
                }
                openView.setOnClickListener {
                    openView.visibility = View.GONE
                    closeView.visibility = View.VISIBLE

                    val boostIntent =
                        Intent(ActivityMgr.getContext(), SplashActivity::class.java)
                    boostIntent.flags =
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    boostIntent.putExtra("functionType", GlobalInfo.boostFunction)
                    startActivity(boostIntent)
                }
            }
            // 设置浮窗显示类型
            .setShowPattern(ShowPattern.BACKGROUND)
            // 设置吸附方式
            .setSidePattern(SidePattern.RIGHT)
            // 设置浮窗的标签，用于区分多个浮窗
            .setTag("boostFloat")
            // 设置浮窗是否可拖拽
            .setDragEnable(true)
            // 设置浮窗的对齐方式和坐标偏移量
            .setGravity(Gravity.END or Gravity.CENTER_VERTICAL, 0, 200)
            .registerCallback {
                touchEvent { _, _ -> }
                drag { view, _ ->
                    val closeView = view.findViewById<ImageView>(R.id.iv_boost_close_easy_float)
                    val openView = view.findViewById<LinearLayout>(R.id.ll_open_easy_float)
                    openView.visibility = View.GONE
                    closeView.visibility = View.VISIBLE
                }
            }
            .show()
    }

    private fun determineAdvertisingInfo() {
        splashViewModel.viewModelScope.launch(Dispatchers.IO) {
            var adInfo: AdvertisingIdClient.Info? = null
            try {
                adInfo = AdvertisingIdClient.getAdvertisingIdInfo(this@SplashActivity)
            } catch (e: IOException) {
                // Unrecoverable error connecting to Google Play services (e.g.,
                // the old version of the service doesn't support getting AdvertisingId).
            } catch (e: GooglePlayServicesNotAvailableException) {
                // Google Play services is not available entirely.
            }
            if (adInfo != null) {
                val id: String = adInfo.id
                val isLAT: Boolean = adInfo.isLimitAdTrackingEnabled
                "AdvertisingId id=$id\nisLat=$isLAT".print()
            }
        }
    }

    /**
     * 注意运行的时候，app需要正式的签名
     * @param context
     */
    fun getKeyHash(context: Context): String? {
        try {
            var info: PackageInfo? = null
            info = context.getPackageManager()
                .getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                var messageDigest: MessageDigest? = null
                messageDigest = MessageDigest.getInstance("SHA")
                messageDigest.update(signature.toByteArray())
                return Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT)
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            unregisterReceiver(mBroadcastReceiver)
            if (ScreenOnBroadcastReceiver.screenOnBroadcastReceiver != null) {
                unregisterReceiver(ScreenOnBroadcastReceiver.screenOnBroadcastReceiver)
            }
            if (AppAddRemoveBroadcastReceiver.appAddRemoveBroadcastReceiver != null) {
                unregisterReceiver(AppAddRemoveBroadcastReceiver.appAddRemoveBroadcastReceiver)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}
package com.hyperstudio.hyper.file.complete.viewmodel

import android.content.pm.PackageManager
import androidx.databinding.ObservableArrayList
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.admob.AdUtils
import com.hyperstudio.hyper.file.base_lib.base.BaseViewMode
import com.hyperstudio.hyper.file.base_lib.bundlebean.DangerAppBundleBean
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.GlobalInfo
import com.hyperstudio.hyper.file.complete.model.DangerAppItem
import com.hyperstudio.hyper.file.complete.model.FunctionSuccessBean
import com.hyperstudio.hyper.file.main.model.TodoItemBean

/**
 * @Name:FunctionSuccess
 * @Description: FunctionSuccessViewModel
 * @Author: even@leanktech.com
 * @Date: 2021/10/21 16:45
 */
class FunctionSuccessViewModel : BaseViewMode() {

    var functionSuccessList = ObservableArrayList<FunctionSuccessBean>()

    /**
     * 加载todo list数据
     */
    fun getTodoListData(dangerList: MutableList<*>?, functionText: String) {
        functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.head).apply {
            functionTypeText = functionText
        })

        dangerList?.forEach { item ->
            if (item is DangerAppBundleBean) {
                try {
                    functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.dangerApp).apply {
                        functionDangerAppItem = DangerAppItem().apply {
                            //app icon
                            val aInfo = ActivityMgr.getContext().packageManager.getApplicationInfo(
                                item.appPackageName,
                                PackageManager.GET_META_DATA
                            )
                            val icon = ActivityMgr.getContext().packageManager.getApplicationIcon(aInfo)
                            appIcon = icon

                            //app name
                            appName = item.appName

                            //app category
                            appCategory = item.appCategory

                            //app summary
                            appSummary = item.appSummary
                        }
                    })
                }catch (e: Exception){
                    functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.dangerApp).apply {
                        functionDangerAppItem = DangerAppItem().apply {
                            //app name
                            appName = item.appName

                            //app category
                            appCategory = item.appCategory

                            //app summary
                            appSummary = item.appSummary
                        }
                    })
                }
            }
        }

        AdUtils.nativeAd?.let{
            functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.ad))
        }

//        functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.foot).apply {
//            functionSuccessItem = TodoItemBean().apply {
//                itemName = GlobalInfo.boostFunction
//                itemImage = R.mipmap.icon_boost
//                itemIntro = ActivityMgr.getContext().getString(R.string.boost_item_intro)
//                itemButtonText = ActivityMgr.getContext().getString(R.string.boost_item_button_text)
//            }
//        })
//        functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.foot).apply {
//            functionSuccessItem = TodoItemBean().apply {
//                itemName = GlobalInfo.saverFunction
//                itemImage = R.mipmap.icon_battery
//                itemIntro = ActivityMgr.getContext().getString(R.string.saver_item_intro)
//                itemButtonText = ActivityMgr.getContext().getString(R.string.saver_item_button_text)
//            }
//        })
//        functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.foot).apply {
//            functionSuccessItem = TodoItemBean().apply {
//                itemName = GlobalInfo.coolerFunction
//                itemImage = R.mipmap.icon_cpu
//                itemIntro = ActivityMgr.getContext().getString(R.string.cooler_item_intro)
//                itemButtonText = ActivityMgr.getContext().getString(R.string.cooler_item_button_text)
//            }
//        })
        functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.foot).apply {
            functionSuccessItem = TodoItemBean().apply {
                itemName = GlobalInfo.cleanFunction
                itemImage = R.mipmap.icon_clean
                itemIntro = ActivityMgr.getContext().getString(R.string.clean_item_intro)
                itemButtonText = ActivityMgr.getContext().getString(R.string.clean_item_button_text)
            }
        })
        functionSuccessList.add(FunctionSuccessBean(FunctionSuccessBean.foot).apply {
            functionSuccessItem = TodoItemBean().apply {
                itemName = GlobalInfo.securityFunction
                itemImage = R.mipmap.icon_safe
                itemIntro = ActivityMgr.getContext().getString(R.string.security_item_intro)
                itemButtonText = ActivityMgr.getContext().getString(R.string.security_item_button_text)
            }
        })
    }
}
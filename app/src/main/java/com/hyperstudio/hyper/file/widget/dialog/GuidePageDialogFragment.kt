package com.hyperstudio.hyper.file.widget.dialog

import android.view.View
import android.view.ViewGroup
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.fragment.BaseDialogFragment
import com.hyperstudio.hyper.file.databinding.GuidePageDialogBinding

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/4/29 17:39
 */
class GuidePageDialogFragment: BaseDialogFragment<GuidePageDialogBinding>()  {

    override var dialogHeight: Int = ViewGroup.LayoutParams.MATCH_PARENT

    override fun getLayoutId() = R.layout.guide_page_dialog

    override fun initView() {
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){
            R.id.ll_widget_guide_back -> {
                dismiss()
            }
        }
    }

}
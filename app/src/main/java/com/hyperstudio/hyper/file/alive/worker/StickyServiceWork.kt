package com.hyperstudio.hyper.file.alive.worker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hyperstudio.hyper.file.base_lib.tool.printError
import com.hyperstudio.hyper.file.unit.startAliveService

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/9/5 18:00
 */
class StickyServiceWork(val context: Context, workerParams: WorkerParameters) :
Worker(context, workerParams) {
    override fun doWork(): Result {
        try {
            startAliveService(context)
        }catch (e: Exception){
            e.printStackTrace()
            e.toString().printError("启动亮屏监听服务")
        }
        return Result.success()
    }

}
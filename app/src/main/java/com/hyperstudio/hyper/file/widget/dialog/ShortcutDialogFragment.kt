package com.hyperstudio.hyper.file.widget.dialog

import android.view.Gravity
import android.view.View
import androidx.fragment.app.FragmentManager
import com.hyperstudio.hyper.file.R
import com.hyperstudio.hyper.file.base_lib.fragment.BaseDialogFragment
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.databinding.ShortCutDialogBinding
import com.hyperstudio.hyper.file.unit.AnalyticsUtils

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/5/25 16:19
 */
class ShortcutDialogFragment(var callBack: (Boolean) -> Unit) :
    BaseDialogFragment<ShortCutDialogBinding>() {

    /** 位置 */
    override val gravity = Gravity.CENTER

    override var dialogWidth: Int = (AppTool.getScreenWidth() * 0.9).toInt()

    override var dimAmount: Float = 0.7f

    override fun getLayoutId() = R.layout.short_cut_dialog

    //规避Dialog init报错
    override fun show(manager: FragmentManager, tag: String?) {
        if(manager.isStateSaved){
            return
        }
        super.show(manager, tag)
    }

    override fun initView() {
        bindingView.lifecycleOwner = this
        bindingView.presenter = this
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.tv_screen_add -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                    screenName = AnalyticsUtils.DIALOG_WIDGET,
                    itemName = AnalyticsUtils.DIALOG_WIDGET_ADD
                )
                callBack(true)
                dismiss()
            }
            R.id.tv_screen_cancel -> {
                AnalyticsUtils.appPageBuriedPoint(
                    eventName = AnalyticsUtils.APP_CLICK_EVENT_NAME,
                    screenName = AnalyticsUtils.DIALOG_WIDGET,
                    itemName = AnalyticsUtils.DIALOG_WIDGET_CANCEL
                )
                callBack(false)
                dismiss()
            }
        }
    }

}
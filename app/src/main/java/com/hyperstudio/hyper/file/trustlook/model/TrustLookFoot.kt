package com.hyperstudio.hyper.file.trustlook.model

import androidx.databinding.BaseObservable
import java.io.Serializable

/**
 * @Name: TrustLookFoot
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/3 17:34
 */
class TrustLookFoot: BaseObservable(), Serializable {
    var footAppName: String = ""
    var footMD5: String = ""
    var footPkgName: String = ""
    var footPkgScore: String = ""
    var footVirusFamily: String = ""
    var footVirusCategory: String = ""
    var footSum: String = ""
    var footLocalRes: String = ""
}
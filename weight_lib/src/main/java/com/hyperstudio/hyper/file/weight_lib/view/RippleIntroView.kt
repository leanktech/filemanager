package com.hyperstudio.hyper.file.weight_lib.view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import com.hyperstudio.hyper.file.weight_lib.R

/**
 * @Name: RippleIntroView
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/8/25 15:47
 */
class RippleIntroView(context: Context, attrs: AttributeSet) : RelativeLayout(context, attrs),
    Runnable {

    /**最大动画范围数 数越大动画范围越广*/
    var mMaxRadius = 90

    /**动画绘制圆的间距*/
    var mInterval = 40
    var count = 0

    var mCacheBitmap: Bitmap? = null
    var mRipplePaint = Paint()
    var mCirclePaint = Paint()

    init {
        mRipplePaint.isAntiAlias = true
        mRipplePaint.isDither = true
        mRipplePaint.style = Paint.Style.STROKE
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mRipplePaint.color = context.getColor(R.color.color_3d88f2)
        }
        mRipplePaint.strokeWidth = 12f

        mCirclePaint.isAntiAlias = true
        mCirclePaint.isDither = true
        mCirclePaint.style = Paint.Style.FILL
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mCirclePaint.color = context.getColor(R.color.color_3d88f2)
        }
    }

    override fun run() {
        //把run对象的引用从队列里拿出来，这样，他就不会执行了，但 run 没有销毁
        removeCallbacks(this)
        count += 2
        count %= mInterval
        invalidate()//重绘
    }

    /**
     * view大小变化时系统调用
     * @param w
     * @param h
     * @param oldw
     * @param oldh
     */
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (mCacheBitmap != null) {
            mCacheBitmap!!.recycle()
            mCacheBitmap = null
        }
    }

    override fun onDraw(canvas: Canvas) {
        //获取图片view
        val mPlusChild: View = getChildAt(0) ?: return

        //获取图片大小
        val pw: Int = mPlusChild.width
        val ph: Int = mPlusChild.height
        if (pw == 0 || ph == 0) return

        //加号图片中心点坐标
        val px: Float = mPlusChild.x + pw / 2
        val py: Float = mPlusChild.y + ph / 2
        val rw = pw / 2
        if (mCacheBitmap == null) {
            mCacheBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            val cv = Canvas(mCacheBitmap!!)
            super.onDraw(cv)
            //0~255，数值越小越透明
            mRipplePaint.alpha = 200
        }

        //绘制背景图片
        canvas.drawBitmap(mCacheBitmap!!, 0F, 0F, mCirclePaint)

        //保存画布当前的状态
        val save: Int = canvas.save()
        var step = count
        while (step <= mMaxRadius) {
            //step越大越靠外就越透明
            mRipplePaint.alpha = 200 * (mMaxRadius - step) / mMaxRadius
            canvas.drawCircle(px, py, (rw + step).toFloat(), mRipplePaint)
            step += mInterval
        }
        //恢复Canvas的状态
        canvas.restoreToCount(save)
        //延迟40毫秒后开始绘制  (动画速度)
        postDelayed(this, 40)
    }

    /**
     * 销毁view时调用，收尾工作
     */
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        if (mCacheBitmap != null) {
            mCacheBitmap!!.recycle()
            mCacheBitmap = null
        }
    }

}
package com.hyperstudio.hyper.file.weight_lib.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.hyperstudio.hyper.file.weight_lib.R;
import com.hyperstudio.hyper.file.weight_lib.view.base.CustomizeViewBase;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 动态打钩动画效果
 * @Description: author: Rhett
 * time: 2022/9/14
 */
public class DrawHookView extends CustomizeViewBase {

    private Paint mPaint;

    //绘制圆弧的进度值
    private int progress = 0;
    //线1的x轴
    private int line1_x = 0;
    //线1的y轴
    private int line1_y = 0;
    //线2的x轴
    private int line2_x = 0;
    //线2的y轴
    private int line2_y = 0;
    //线1是否绘制完毕
    private boolean line1Done = false;

    public DrawHookView(@NotNull Context context, @NotNull AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        progress += 5;

        /**
         * 绘制圆弧
         */
        Paint paint = new Paint();
        //设置画笔颜色
        paint.setColor(getResources().getColor(R.color.toolbar_background));
        //设置圆弧的宽度
        paint.setStrokeWidth(30);
        //设置圆弧为空心
        paint.setStyle(Paint.Style.STROKE);
        //消除锯齿
        paint.setAntiAlias(true);
        //Paint.Cap.BUTT(无线帽)，Paint.Cap.ROUND(圆形线帽)，Paint.Cap.SQUARE(矩形线帽)
        paint.setStrokeCap(Paint.Cap.ROUND);

        //获取圆心的x坐标
        int center = getWidth() / 2;
        int center1 = center - getWidth() / 5;
        //圆弧半径
        int radius = getWidth() / 2 - 30;

        //定义的圆弧的形状和大小的界限
        RectF rectF = new RectF(center - radius - 1, center - radius - 1 ,center + radius + 1, center + radius + 1);

        //根据进度画圆弧
        canvas.drawArc(rectF, 235, -360 * progress / 100, false, paint);

        /**
         * 绘制对勾
         */
        //先等圆弧画完，才话对勾
        if(progress >= 100) {
            if(line1_x < radius / 3) {
                line1_x += 6;
                line1_y += 6;
            }
            //画第一根线
            canvas.drawLine(center1, center, center1 + line1_x, center + line1_y, paint);

            if (!line1Done && line1_x >= radius / 3) {
                line1Done = true;
                //初始化线2的stopX和stopY
                line2_x = line1_x;
                line2_y = line1_y;
            }

            //画第二根线
            canvas.drawLine(center1 + line1_x + 1, center + line1_y, center1 + line2_x, center + line2_y, paint);

            if (line1_x >= radius / 3 && line2_x <= radius) {
                line2_x += 6;
                line2_y -= 6;
            }
        }

        //每隔10毫秒界面刷新
        postInvalidateDelayed(10);
    }

    @Override
    protected void initPaint() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(dip2px(2));
    }

    @Override
    protected void onAnimationUpdate(@Nullable ValueAnimator valueAnimator) {
    }

    @Override
    protected void onAnimationRepeat(@Nullable Animator animation) {

    }

    @Override
    protected int onStopAnim() {
        return 0;
    }

    @Override
    protected int setAnimRepeatMode() {
        return ValueAnimator.RESTART;
    }

    @Override
    protected int setAnimRepeatCount() {
        return ValueAnimator.RESTART;
    }

    @Override
    protected void animIsRunning() {

    }
}
package com.hyperstudio.hyper.file.weight_lib.view.base

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2022/9/21 17:35
 */
abstract class CustomizeViewBase(context: Context, attrs: AttributeSet): View(context, attrs) {

    fun startAnim() {
        stopAnim()
        startViewAnim(0f, 1f, 500)
    }

    fun startAnim(time: Int) {
        stopAnim()
        startViewAnim(0f, 1f, time.toLong())
    }


    fun stopAnim() {
        if (valueAnimator != null) {
            clearAnimation()
            valueAnimator!!.repeatCount = 0
            valueAnimator!!.cancel()
            valueAnimator!!.end()
            if (onStopAnim() == 0) {
                valueAnimator!!.repeatCount = 0
                valueAnimator!!.cancel()
                valueAnimator!!.end()
            }
        }
    }

    var valueAnimator: ValueAnimator? = null

    private fun startViewAnim(startF: Float, endF: Float, time: Long): ValueAnimator? {
        valueAnimator = ValueAnimator.ofFloat(startF, endF)
        valueAnimator?.duration = time
        valueAnimator?.interpolator = LinearInterpolator()
        valueAnimator?.repeatCount = setAnimRepeatCount()
        if (ValueAnimator.RESTART == setAnimRepeatMode()) {
            valueAnimator?.repeatMode = ValueAnimator.RESTART
        } else if (ValueAnimator.REVERSE == setAnimRepeatMode()) {
            valueAnimator?.repeatMode = ValueAnimator.REVERSE
        }
        valueAnimator?.addUpdateListener { valueAnimator ->
            onAnimationUpdate(
                valueAnimator
            )
        }
        valueAnimator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
            }

            override fun onAnimationStart(animation: Animator) {
                super.onAnimationStart(animation)
            }

            override fun onAnimationRepeat(animation: Animator) {
                super.onAnimationRepeat(animation)
                this@CustomizeViewBase.onAnimationRepeat(animation)
            }
        })
        if (valueAnimator?.isRunning == false) {
            animIsRunning()
            valueAnimator?.start()
        }
        return valueAnimator
    }


    protected abstract fun initPaint()

    protected abstract fun onAnimationUpdate(valueAnimator: ValueAnimator?)

    protected abstract fun onAnimationRepeat(animation: Animator?)

    protected abstract fun onStopAnim(): Int

    protected abstract fun setAnimRepeatMode(): Int

    protected abstract fun setAnimRepeatCount(): Int

    protected abstract fun animIsRunning()


    fun dip2px(dpValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    fun getFontLength(paint: Paint, str: String): Float {
        val rect = Rect()
        paint.getTextBounds(str, 0, str.length, rect)
        return rect.width().toFloat()
    }

    fun getFontHeight(paint: Paint, str: String): Float {
        val rect = Rect()
        paint.getTextBounds(str, 0, str.length, rect)
        return rect.height().toFloat()
    }

    fun getFontHeight(paint: Paint): Float {
        val fm = paint.fontMetrics
        return fm.descent - fm.ascent
    }
}
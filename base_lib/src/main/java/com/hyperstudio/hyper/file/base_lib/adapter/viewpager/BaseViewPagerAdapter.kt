package com.hyperstudio.hyper.file.base_lib.adapter.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

/**
 * @Name: BaseViewPager2Adapter
 * @Description: vp2 base adapter
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */


class BaseViewPagerAdapter(fragmentActivity: FragmentActivity, var fragments: MutableList<Fragment>) : FragmentStateAdapter(fragmentActivity) {

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }

    override fun getItemCount(): Int {
        return fragments.size
    }

}
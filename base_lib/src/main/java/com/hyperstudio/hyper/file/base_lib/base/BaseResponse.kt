package com.hyperstudio.hyper.file.base_lib.base


interface BaseResponse<T> {
    fun code(): Int
    fun msg(): String
    fun data(): T
    fun isSuccess(): Boolean
}
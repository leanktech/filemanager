package com.hyperstudio.hyper.file.base_lib.tool

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/11/11 17:50
 */

fun isNumeric(str: String?): Boolean {
    val pattern: Pattern = Pattern.compile("^[1-9]\\d{0,}\\.{0,1}\\d{0,}\$|^[0]\\.\\d{0,}[1-9]\$")
    val isNum: Matcher = pattern.matcher(str)
    return isNum.matches()
}
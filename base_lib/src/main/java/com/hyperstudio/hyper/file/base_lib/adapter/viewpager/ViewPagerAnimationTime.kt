package com.hyperstudio.hyper.file.base_lib.adapter.viewpager

import android.view.View
import androidx.viewpager2.widget.ViewPager2

/**
 * @Name: ViewPagerAnimationTime
 * @Description: ViewPager2 切换动画
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */

/**ViewPager缩放动画
 * 默认时间0.75*/
class ViewPagerScale(var animationTime: Float = 0.75f) : ViewPager2.PageTransformer {

    override fun transformPage(view: View, position: Float) {
        val pageWidth = view.width

        when {
            position < -1 -> { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.alpha = 0f
            }
            position <= 0 -> { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.alpha = 1F
                view.translationX = 0f
                view.scaleX = 1f
                view.scaleY = 1f
            }
            position <= 1 -> { // (0,1]
                // Fade the page out.
                view.alpha = 1 - position

                // Counteract the default slide transition
                view.translationX = pageWidth * -position

                // Scale the page down (between MIN_SCALE and 1)
                val scaleFactor: Float = (animationTime
                        + (1 - animationTime) * (1 - kotlin.math.abs(position)))
                view.scaleX = scaleFactor
                view.scaleY = scaleFactor
            }
            else -> { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.alpha = 0f
            }
        }

    }
}

/**ViewPager叠加动画
 * 默认时间1*/
class ViewPagerOverlay(var animationTime: Float = 1f) : ViewPager2.PageTransformer {

    override fun transformPage(view: View, position: Float) {
        val pageWidth = view.width

        when {
            position < -1 -> { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.alpha = 0f
            }
            position <= 0 -> { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.alpha = 1F
                view.translationX = 0f
                view.scaleX = 1f
                view.scaleY = 1f
            }
            position <= 1 -> { // (0,1]
                // Fade the page out.
                view.alpha = 1 - position

                // Counteract the default slide transition
                view.translationX = pageWidth * -position

                // Scale the page down (between MIN_SCALE and 1)
                val scaleFactor: Float = (animationTime
                        + (1 - animationTime) * (1 - kotlin.math.abs(position)))
                view.scaleX = scaleFactor
                view.scaleY = scaleFactor
            }
            else -> { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.alpha = 0f
            }
        }

    }

}
package com.hyperstudio.hyper.file.base_lib.adapter.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * @Name: BaseFragmentPageAdapter
 * @Description: vp base adapter
 * @Author: even@leanktech.com
 * @Date: 2021/8/24 15:58
 */

class BaseFragmentPageAdapter(var mList: MutableList<Fragment>,var titleList: MutableList<String>, fm: FragmentManager,behavior:Int = BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) : FragmentPagerAdapter(fm,behavior){
    override fun getItem(position: Int): Fragment {
        return mList[position]
    }

    override fun getCount(): Int {
        return mList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }

}
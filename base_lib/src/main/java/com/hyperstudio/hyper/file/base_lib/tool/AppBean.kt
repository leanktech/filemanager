package com.hyperstudio.hyper.file.base_lib.tool

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.hyperstudio.hyper.file.base_lib.BR
import com.hyperstudio.hyper.file.base_lib.tool.keyvalue.MmkvKeyValueMgr
import java.io.Serializable

/**
 * @Name: AccountBean
 * @Description: 本地持久化
 * @Author: Even@leanktech.com
 * @Date: 2021/9/18 15:50
 */
object AppBean : BaseObservable(), Serializable {

    /**
     * 是否第一次进入app（记录首次启动逻辑）
     */
    @set:Bindable
    var firstStartApp: Boolean = MmkvKeyValueMgr.get("firstStartApp", true) ?: true
        @Bindable get
        set(firstStartApp) {
            field = firstStartApp
            firstStartApp.let { MmkvKeyValueMgr.put("firstStartApp", it) }
            notifyPropertyChanged(BR.firstStartApp)
        }

    /**
     * 是否第一次进入app（记录首次启动逻辑）
     */
    @set:Bindable
    var startAppFirstTime: Boolean = MmkvKeyValueMgr.get("startAppFirstTime", true) ?: true
        @Bindable get
        set(startAppFirstTime) {
            field = startAppFirstTime
            startAppFirstTime.let { MmkvKeyValueMgr.put("startAppFirstTime", it) }
            notifyPropertyChanged(BR.startAppFirstTime)
        }

    /**
     * 是否同意协议
     */
    @set:Bindable
    var firstAgreeAgreement: Boolean = MmkvKeyValueMgr.get("firstAgreeAgreement", false) ?: true
        @Bindable get
        set(firstAgreeAgreement) {
            field = firstAgreeAgreement
            firstAgreeAgreement.let { MmkvKeyValueMgr.put("firstAgreeAgreement", it) }
            notifyPropertyChanged(BR.firstAgreeAgreement)
        }

    /**
     * 常驻通知点击type类型
     */
    @set:Bindable
    var residentNotificationChoiceType: String = MmkvKeyValueMgr.get("residentNotificationChoiceType", "") ?: ""
        @Bindable get
        set(residentNotificationChoiceType) {
            field = residentNotificationChoiceType
            residentNotificationChoiceType.let { MmkvKeyValueMgr.put("residentNotificationChoiceType", it) }
            notifyPropertyChanged(BR.residentNotificationChoiceType)
        }

    /**
     * app启动方式
     */
    @set:Bindable
    var appStartUpType: String = MmkvKeyValueMgr.get("appStartUpType", "") ?: ""
        @Bindable get
        set(appStartUpType) {
            field = appStartUpType
            appStartUpType.let { MmkvKeyValueMgr.put("appStartUpType", it) }
            notifyPropertyChanged(BR.appStartUpType)
        }


    /**
     * clean当天执行与否
     */
    @set:Bindable
    var startBoostToday: String = MmkvKeyValueMgr.get("startBoostToday", "") ?: ""
        @Bindable get
        set(startBoostToday) {
            field = startBoostToday
            startBoostToday.let { MmkvKeyValueMgr.put("startBoostToday", it) }
            notifyPropertyChanged(BR.startBoostToday)
        }


    /**
     * battery当天执行与否
     */
    @set:Bindable
    var startBatteryToday: String = MmkvKeyValueMgr.get("startBatteryToday", "") ?: ""
        @Bindable get
        set(startBatteryToday) {
            field = startBatteryToday
            startBatteryToday.let { MmkvKeyValueMgr.put("startBatteryToday", it) }
            notifyPropertyChanged(BR.startBatteryToday)
        }

    /**
     * cooler当天执行与否
     */
    @set:Bindable
    var startCoolerToday: String = MmkvKeyValueMgr.get("startCoolerToday", "") ?: ""
        @Bindable get
        set(startCoolerToday) {
            field = startCoolerToday
            startCoolerToday.let { MmkvKeyValueMgr.put("startCoolerToday", it) }
            notifyPropertyChanged(BR.startCoolerToday)
        }

    /**
     * security当天执行与否
     */
    @set:Bindable
    var startSecurityToday: String = MmkvKeyValueMgr.get("startSecurityToday", "") ?: ""
        @Bindable get
        set(startSecurityToday) {
            field = startSecurityToday
            startSecurityToday.let { MmkvKeyValueMgr.put("startSecurityToday", it) }
            notifyPropertyChanged(BR.startSecurityToday)
        }

    /**
     * 内存占用量 用于常驻通知boost显示
     */
    @set:Bindable
    var memoryNumber: String = MmkvKeyValueMgr.get("memoryNumber", "55") ?: ""
        @Bindable get
        set(memoryNumber) {
            field = memoryNumber
            memoryNumber.let { MmkvKeyValueMgr.put("memoryNumber", it) }
            notifyPropertyChanged(BR.memoryNumber)
        }

    /**
     * 电池温度 用于常驻通知cooler显示
     */
    @set:Bindable
    var temperature: String = MmkvKeyValueMgr.get("temperature", "33") ?: ""
        @Bindable get
        set(temperature) {
            field = temperature
            temperature.let { MmkvKeyValueMgr.put("temperature", it) }
            notifyPropertyChanged(BR.temperature)
        }

    /**
     * Cooler通知推送的时间记录
     */
    @set:Bindable
    var lastCoolCpuNotifyTime: Long = MmkvKeyValueMgr.get("lastCoolCpuNotifyTime", 0L) ?: 0L
        @Bindable get
        set(lastCoolCpuNotifyTime) {
            field = lastCoolCpuNotifyTime
            lastCoolCpuNotifyTime.let { MmkvKeyValueMgr.put("lastCoolCpuNotifyTime", it) }
            notifyPropertyChanged(BR.lastCoolCpuNotifyTime)
        }

    /**
     * 电池电量 用于常驻通知battery显示
     */
    @set:Bindable
    var electricQuantity: String = MmkvKeyValueMgr.get("electricQuantity", "100") ?: ""
        @Bindable get
        set(electricQuantity) {
            field = electricQuantity
            electricQuantity.let { MmkvKeyValueMgr.put("electricQuantity", it) }
            notifyPropertyChanged(BR.electricQuantity)
        }

    /**
     * Battery通知推送的时间记录
     */
    @set:Bindable
    var powerConnectedNotificationTime: Long = MmkvKeyValueMgr.get("powerConnectedNotificationTime", 0L) ?: 0L
        @Bindable get
        set(powerConnectedNotificationTime) {
            field = powerConnectedNotificationTime
            powerConnectedNotificationTime.let { MmkvKeyValueMgr.put("powerConnectedNotificationTime", it) }
            notifyPropertyChanged(BR.powerConnectedNotificationTime)
        }

    /**
     * 初次启动app记录当前时间 用于功能执行完成弹出打分判断是否过三天
     */
    @set:Bindable
    var firstStartAppTime: Long = MmkvKeyValueMgr.get("firstStartAppTime", 0L) ?: 0L
        @Bindable get
        set(firstStartAppTime) {
            field = firstStartAppTime
            firstStartAppTime.let { MmkvKeyValueMgr.put("firstStartAppTime", it) }
            notifyPropertyChanged(BR.firstStartAppTime)
        }

    /**
     * 记录亮屏通知的时间 用于执行一小时内只能弹一次亮屏通知
     */
    @set:Bindable
    var screenOnNotificationTime: Long = MmkvKeyValueMgr.get("screenOnNotificationTime", 0L) ?: 0L
        @Bindable get
        set(screenOnNotificationTime) {
            field = screenOnNotificationTime
            screenOnNotificationTime.let { MmkvKeyValueMgr.put("screenOnNotificationTime", it) }
            notifyPropertyChanged(BR.screenOnNotificationTime)
        }

    /**
     * 是否同意协议
     */
    @set:Bindable
    var showBatteryNotification: Boolean = MmkvKeyValueMgr.get("showBatteryNotification", false) ?: false
        @Bindable get
        set(showBatteryNotification) {
            field = showBatteryNotification
            showBatteryNotification.let { MmkvKeyValueMgr.put("showBatteryNotification", it) }
            notifyPropertyChanged(BR.showBatteryNotification)
        }

    /**
     * 开锁通知整体推送时间，限制一小时
     */
    @set:Bindable
    var lastNotificationTime: Long = MmkvKeyValueMgr.get("lastNotificationTime", 0L) ?: 0L
        @Bindable get
        set(lastNotificationTime) {
            field = lastNotificationTime
            lastNotificationTime.let { MmkvKeyValueMgr.put("lastNotificationTime", it) }
            notifyPropertyChanged(BR.lastNotificationTime)
        }

    /**
     * Security通知推送的时间记录
     */
    @set:Bindable
    var lastAntivirusNotifyTime: Long = MmkvKeyValueMgr.get("lastAntivirusNotifyTime", 0L) ?: 0L
        @Bindable get
        set(lastAntivirusNotifyTime) {
            field = lastAntivirusNotifyTime
            lastAntivirusNotifyTime.let { MmkvKeyValueMgr.put("lastAntivirusNotifyTime", it) }
            notifyPropertyChanged(BR.lastAntivirusNotifyTime)
        }

    /**
     * Clean通知推送的时间记录
     */
    @set:Bindable
    var lastCleanNotifyTime: Long = MmkvKeyValueMgr.get("lastCleanNotifyTime", 0L) ?: 0L
        @Bindable get
        set(lastCleanNotifyTime) {
            field = lastCleanNotifyTime
            lastCleanNotifyTime.let { MmkvKeyValueMgr.put("lastCleanNotifyTime", it) }
            notifyPropertyChanged(BR.lastCleanNotifyTime)
        }

    /**
     * 亮屏Boost通知推送的时间记录
     */
    @set:Bindable
    var lastBoostNotifyTime: Long = MmkvKeyValueMgr.get("lastBoostNotifyTime", 0L) ?: 0L
        @Bindable get
        set(lastBoostNotifyTime) {
            field = lastBoostNotifyTime
            lastBoostNotifyTime.let { MmkvKeyValueMgr.put("lastBoostNotifyTime", it) }
            notifyPropertyChanged(BR.lastBoostNotifyTime)
        }

    /**
     * TikTok App是否存在
     */
    @set:Bindable
    var tiktokAlive: Boolean = MmkvKeyValueMgr.get("tiktokAlive", false) ?: false
        @Bindable get
        set(tiktokAlive) {
            field = tiktokAlive
            tiktokAlive.let { MmkvKeyValueMgr.put("tiktokAlive", it) }
            notifyPropertyChanged(BR.tiktokAlive)
        }

    /**
     * WhatsApp App是否存在
     */
    @set:Bindable
    var whatsAppAlive: Boolean = MmkvKeyValueMgr.get("whatsAppAlive", false) ?: false
        @Bindable get
        set(whatsAppAlive) {
            field = whatsAppAlive
            whatsAppAlive.let { MmkvKeyValueMgr.put("whatsAppAlive", it) }
            notifyPropertyChanged(BR.whatsAppAlive)
        }

    /**
     * GooglePlay App是否存在
     */
    @set:Bindable
    var googlePlayAlive: Boolean = MmkvKeyValueMgr.get("googlePlayAlive", false) ?: false
        @Bindable get
        set(googlePlayAlive) {
            field = googlePlayAlive
            googlePlayAlive.let { MmkvKeyValueMgr.put("googlePlayAlive", it) }
            notifyPropertyChanged(BR.googlePlayAlive)
        }

    /**
     * YouTube App是否存在
     */
    @set:Bindable
    var youTubeAlive: Boolean = MmkvKeyValueMgr.get("youTubeAlive", false) ?: false
        @Bindable get
        set(youTubeAlive) {
            field = youTubeAlive
            youTubeAlive.let { MmkvKeyValueMgr.put("youTubeAlive", it) }
            notifyPropertyChanged(BR.youTubeAlive)
        }

    /**
     * YouTube App是否存在
     */
    @set:Bindable
    var lineAlive: Boolean = MmkvKeyValueMgr.get("lineAlive", false) ?: false
        @Bindable get
        set(lineAlive) {
            field = lineAlive
            lineAlive.let { MmkvKeyValueMgr.put("lineAlive", it) }
            notifyPropertyChanged(BR.lineAlive)
        }

    /**
     * Targeted推送的哪个app
     */
    @set:Bindable
    var targetedPushType: Int = MmkvKeyValueMgr.get("targetedPushType", 0) ?: 0
        @Bindable get
        set(targetedPushType) {
            field = targetedPushType
            targetedPushType.let { MmkvKeyValueMgr.put("targetedPushType", it) }
            notifyPropertyChanged(BR.targetedPushType)
        }

    /**
     * 安装通知推送的时间记录
     */
    @set:Bindable
    var lastAddPackageNotifyTime: Long = MmkvKeyValueMgr.get("lastInstallNotifyTime", 0L) ?: 0L
        @Bindable get
        set(lastInstallNotifyTime) {
            field = lastInstallNotifyTime
            lastInstallNotifyTime.let { MmkvKeyValueMgr.put("lastInstallNotifyTime", it) }
            notifyPropertyChanged(BR.lastAddPackageNotifyTime)
        }

    /**
     * 卸载通知推送的时间记录
     */
    @set:Bindable
    var lastRemovePackageNotifyTime: Long = MmkvKeyValueMgr.get("lastRemovePackageNotifyTime", 0L) ?: 0L
        @Bindable get
        set(lastRemovePackageNotifyTime) {
            field = lastRemovePackageNotifyTime
            lastRemovePackageNotifyTime.let { MmkvKeyValueMgr.put("lastRemovePackageNotifyTime", it) }
            notifyPropertyChanged(BR.lastRemovePackageNotifyTime)
        }

    /**
     * fcm token
     */
    @set:Bindable
    var fcmToken: String = MmkvKeyValueMgr.get("fcmToken", "") ?: ""
        @Bindable get
        set(fcmToken) {
            field = fcmToken
            fcmToken.let { MmkvKeyValueMgr.put("fcmToken", it) }
            notifyPropertyChanged(BR.fcmToken)
        }

    /**
     * fcm token
     */
    @set:Bindable
    var fcmIsRunning: Boolean = MmkvKeyValueMgr.get("fcmIsRunning", false) ?: false
        @Bindable get
        set(fcmIsRunning) {
            field = fcmIsRunning
            fcmIsRunning.let { MmkvKeyValueMgr.put("fcmIsRunning", it) }
            notifyPropertyChanged(BR.fcmIsRunning)
        }

    /**
     * agree trustLook
     */
    @set:Bindable
    var agreeTrustLook: Boolean = MmkvKeyValueMgr.get("agreeTrustLook", false) ?: false
        @Bindable get
        set(agreeTrustLook) {
            field = agreeTrustLook
            agreeTrustLook.let { MmkvKeyValueMgr.put("agreeTrustLook", it) }
            notifyPropertyChanged(BR.agreeTrustLook)
        }

    /**
     * notification boost turn
     */
    @set:Bindable
    var boostNotificationTurn: Boolean = MmkvKeyValueMgr.get("boostNotificationTurn", false) ?: false
        @Bindable get
        set(boostNotificationTurn) {
            field = boostNotificationTurn
            boostNotificationTurn.let { MmkvKeyValueMgr.put("boostNotificationTurn", it) }
            notifyPropertyChanged(BR.boostNotificationTurn)
        }

    /**
     * 上一次推送插电通知的时间
     */
    @set:Bindable
    var lastPushPowerTime: Long = MmkvKeyValueMgr.get("lastPushPowerTime", 0L) ?: 0L
        @Bindable get
        set(lastPushPowerTime) {
            field = lastPushPowerTime
            lastPushPowerTime.let { MmkvKeyValueMgr.put("lastPushPowerTime", it) }
            notifyPropertyChanged(BR.lastPushPowerTime)
        }

    /**
     * 手机亮屏的时间计算
     */
    @set:Bindable
    var screenOnTimer: Long = MmkvKeyValueMgr.get("screenOnTimer", -1L) ?: -1L
        @Bindable get
        set(screenOnTimer) {
            field = screenOnTimer
            screenOnTimer.let { MmkvKeyValueMgr.put("screenOnTimer", it) }
            notifyPropertyChanged(BR.screenOnTimer)
        }

    /**
     * 当日Cool的次数，每日最多2次
     */
    @set:Bindable
    var dayCoolCount: String = MmkvKeyValueMgr.get("dayCoolCount", "") ?: ""
        @Bindable get
        set(dayCoolCount) {
            field = dayCoolCount
            dayCoolCount.let { MmkvKeyValueMgr.put("dayCoolCount", it) }
            notifyPropertyChanged(BR.dayCoolCount)
        }

    /**
     * 当日安装的次数，每日最多2次
     */
    @set:Bindable
    var addCount: String = MmkvKeyValueMgr.get("addCount", "") ?: ""
        @Bindable get
        set(addCount) {
            field = addCount
            addCount.let { MmkvKeyValueMgr.put("addCount", it) }
            notifyPropertyChanged(BR.addCount)
        }

    /**
     * 当日卸载的次数，每日最多2次
     */
    @set:Bindable
    var removeCount: String = MmkvKeyValueMgr.get("removeCount", "") ?: ""
        @Bindable get
        set(removeCount) {
            field = removeCount
            removeCount.let { MmkvKeyValueMgr.put("removeCount", it) }
            notifyPropertyChanged(BR.removeCount)
        }

    /**
     * 上一次Security通知显示的时间（4小时弹一次）
     */
    @set:Bindable
    var lastSecurityNotifyTime: Long = MmkvKeyValueMgr.get("lastSecurityNotifyTime", System.currentTimeMillis()) ?: System.currentTimeMillis()
        @Bindable get
        set(lastSecurityNotifyTime) {
            field = lastSecurityNotifyTime
            lastSecurityNotifyTime.let { MmkvKeyValueMgr.put("lastSecurityNotifyTime", it) }
            notifyPropertyChanged(BR.lastSecurityNotifyTime)
        }

    /**
     * 当日Security推送的次数，每日最多1次
     */
    @set:Bindable
    var securityNotifyCount: String = MmkvKeyValueMgr.get("securityNotifyCount", "") ?: ""
        @Bindable get
        set(securityNotifyCount) {
            field = securityNotifyCount
            securityNotifyCount.let { MmkvKeyValueMgr.put("securityNotifyCount", it) }
            notifyPropertyChanged(BR.securityNotifyCount)
        }

    /**
     * notification boost turn
     */
    @set:Bindable
    var checkMemoryUsage: Boolean = MmkvKeyValueMgr.get("checkMemoryUsage", false) ?: false
        @Bindable get
        set(checkMemoryUsage) {
            field = checkMemoryUsage
            checkMemoryUsage.let { MmkvKeyValueMgr.put("checkMemoryUsage", it) }
            notifyPropertyChanged(BR.checkMemoryUsage)
        }

    /**
     * 通知点击埋点保存 做功能结束之后的埋点判断
     */
    @set:Bindable
    var pushEmbeddingPoint: String = MmkvKeyValueMgr.get("pushEmbeddingPoint", "") ?: ""
        @Bindable get
        set(pushEmbeddingPoint) {
            field = pushEmbeddingPoint
            pushEmbeddingPoint.let { MmkvKeyValueMgr.put("pushEmbeddingPoint", it) }
            notifyPropertyChanged(BR.pushEmbeddingPoint)
        }

    @set:Bindable
    var itemCategory: String = MmkvKeyValueMgr.get("itemCategory", "") ?: ""
        @Bindable get
        set(itemCategory) {
            field = itemCategory
            itemCategory.let { MmkvKeyValueMgr.put("itemCategory", it) }
            notifyPropertyChanged(BR.itemCategory)
        }

    @set:Bindable
    var contentType: String = MmkvKeyValueMgr.get("contentType", "") ?: ""
        @Bindable get
        set(contentType) {
            field = contentType
            contentType.let { MmkvKeyValueMgr.put("contentType", it) }
            notifyPropertyChanged(BR.contentType)
        }

}


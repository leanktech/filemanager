package com.hyperstudio.hyper.file.base_lib.adapter.`interface`

import androidx.databinding.ViewDataBinding
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder

/**
 * @Name: itemConvert
 * @Description: 适配器convert回调
 * @Author: even@leanktech.com
 * @Date: 2021/8/27 13:49
 */
interface ItemConvert {
    /**
     * 装饰item
     * @param holder ViewHolder
     * @param item dataItem
     */
    //多布局适配器需要实现的方法
    fun multiConvert(holder: BaseDataBindingHolder<ViewDataBinding>?, item: MultiItemEntity){}
    //普通适配器需要实现的方法
    fun simpleConvert(holder: BaseDataBindingHolder<ViewDataBinding>?, item: Any?){}

}
package com.hyperstudio.hyper.file.base_lib.base

import android.view.View

/**
 * item点击处理
 * @author Even
 * @email even@leanktech.com
 * @date 2021/8/27
 */
interface  ItemClickPresenter<in Any> {
    /**
     * item点击处理
     * @param v 点击View
     * @param item 数据对象
     */
    fun onItemClick(v: View?=null, item:Any)
}
package com.hyperstudio.hyper.file.base_lib.view

import android.view.Gravity
import com.hyperstudio.hyper.file.base_lib.R
import com.hyperstudio.hyper.file.base_lib.databinding.LoadingDialogBinding
import com.hyperstudio.hyper.file.base_lib.fragment.BaseDialogFragment
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppTool
import com.hyperstudio.hyper.file.base_lib.tool.StatusBarTool

/**
 * @Name: LoadingDialogFragment
 * @Description: LoadingView
 * @Author: even@leanktech.com
 * @Date: 2021/9/17 16:48
 */
class LoadingDialogFragment : BaseDialogFragment<LoadingDialogBinding>() {

    /** 高度 */
    override var dialogHeight: Int = AppTool.showContentHeight -
            StatusBarTool.getStatusBarHeight(
                context ?: ActivityMgr.currentActivity()!!
            )
//
//    /** dialog 弹出样式 */
//    override var animStyle: Int = R.style.dialogAlphaAnim

    /** 位置 */
    override val gravity = Gravity.CENTER

    override fun getLayoutId() = R.layout.loading_dialog

    override fun initView() {
        bindingView.lifecycleOwner = this

        bindingView.avBsfLoading.show()
    }

}
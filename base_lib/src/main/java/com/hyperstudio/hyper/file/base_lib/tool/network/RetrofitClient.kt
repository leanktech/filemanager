package com.hyperstudio.hyper.file.base_lib.tool.network

import androidx.databinding.library.BuildConfig
import com.hyperstudio.hyper.file.base_lib.tool.AppConfig
import com.hyperstudio.hyper.file.base_lib.tool.AppConfig.Companion.FORCE_RELEASE
import com.hyperstudio.hyper.file.base_lib.tool.network.interceptor.LoggingInterceptor
import com.hyperstudio.hyper.file.base_lib.tool.print
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {

    fun getDefaultRetrofit(): Retrofit =
        Retrofit.Builder()
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(if ((!BuildConfig.DEBUG) || FORCE_RELEASE) {
                AppConfig.BASE_URL_FOR_RELEASE
            } else {
                AppConfig.BASE_URL_FOR_TEST
            })
            .build()

    fun getRetrofit(client: OkHttpClient, baseUrl: String): Retrofit =
        Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()

    fun getOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(20L, TimeUnit.SECONDS)//连接超时时间
            .writeTimeout(60L, TimeUnit.SECONDS)//写入超时时间
            .connectionPool(ConnectionPool(8, 15, TimeUnit.SECONDS))//连接池
            .addInterceptor(LoggingInterceptor(logger = { message -> message.print("-NET-") }).apply { level = LoggingInterceptor.Level.BODY })//日志拦截器
            .build()
}
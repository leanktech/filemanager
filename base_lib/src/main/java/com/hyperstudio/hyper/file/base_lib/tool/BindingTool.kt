package com.hyperstudio.hyper.file.base_lib.tool

import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import java.util.concurrent.TimeUnit

/**
 * 加载图片资源文件（一般用于动态加载）
 * @param v ImageView
 * @param imgRes 资源文件
 */
@BindingAdapter(value = ["imgRes"])
fun setImgResValue(v: ImageView, imgRes: Int?) {
    if (imgRes != null) {
        v.setImageResource(imgRes)
    }
}

@BindingAdapter(value = ["imgDrawable"])
fun setImgDrawable(v: ImageView, imgDrawable: Drawable?) {
    if (imgDrawable != null) {
        v.setImageDrawable(imgDrawable)
    }
}

@BindingAdapter(value = ["bgColor"])
fun setViewBgColorValue(v: View, color: Int?) {
    if (color != null) {
        (v.background as? GradientDrawable)?.setColor(
            ContextCompat.getColor(
                ActivityMgr.getContext(),
                color
            )
        )
    }
}

@BindingAdapter(value = ["formatScanTime"])
fun setFormatScanTime(v: TextView, millis: String?) {
    millis?.let { time ->
        v.text = if (time.toInt() < 1000) {
            "00:00:01"
        } else {
            String.format(
                "%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(time.toLong()),
                TimeUnit.MILLISECONDS.toMinutes(time.toLong()) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time.toLong())),
                TimeUnit.MILLISECONDS.toSeconds(time.toLong()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time.toLong()))
            )
        }
    }
}

@BindingAdapter(value = ["requisiteForVisible"])
fun setVisibleByRequisiteValue(v: View, requisiteVisible: Any?) {
    if (requisiteVisible == null) {
        v.visibility = View.GONE
        return
    }
    v.visibility = when (requisiteVisible) {
        is String -> {
            if (requisiteVisible.isEmpty()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
        is List<*> -> {
            if (requisiteVisible.isEmpty()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
        is Boolean -> {
            if (requisiteVisible == true) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
        else -> View.VISIBLE
    }
}
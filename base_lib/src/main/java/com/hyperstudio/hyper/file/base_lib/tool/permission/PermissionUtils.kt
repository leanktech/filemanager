package com.hyperstudio.hyper.file.base_lib.tool.permission

import android.content.Context
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.XXPermissions
import com.hyperstudio.hyper.file.base_lib.R
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.ToastTool

/**
 * @Name: PermissionUtils
 * @Description: 权限相关工具方法
 * @Author: even@leanktech.com
 * @Date: 2021/9/2 13:16
 */

/**
 * 授予单个权限
 * @param permissionSingle 单个权限
 * @param permissionName 权限名称
 * @param callBack 需要返回的回调
 */
fun getPermissionSingle(
    context: Context,
    permissionSingle: String,
    permissionName: String,
    callBack: PermissionCallback
) {
    //判断权限有没有开启 如果开启直接执行成功回调
    if (XXPermissions.isGranted(context, permissionSingle)) {
        callBack.build.onGrantedAllSuccess?.invoke()
    } else {
        XXPermissions.with(context)
            .permission(permissionSingle)
            .request(object : OnPermissionCallback {
                override fun onGranted(permissions: MutableList<String>?, all: Boolean) {
                    if (all) {
//                        ToastTool.showToast(permissionName + ActivityMgr.getContext().getString(R.string.grant_success_text), context, 1)
                        callBack.build.onGrantedAllSuccess?.invoke()
                    } else {
                        ToastTool.showToast(permissionName + ActivityMgr.getContext().getString(R.string.grant_fail_text), context, 1)
                        callBack.build.onGrantedHaveError?.invoke()
                    }
                }

                override fun onDenied(permissions: MutableList<String>?, never: Boolean) {
                    if (never) {
                        ToastTool.showToast(ActivityMgr.getContext().getString(R.string.grant_denied_never_text), context, 1)
                        callBack.build.onDeniedNever?.invoke()
                    } else {
                        ToastTool.showToast(permissionName + ActivityMgr.getContext().getString(R.string.grant_denied_text), context, 1)
                        callBack.build.onDenied?.invoke()
                    }
                }
            })
    }
}

/**
 * 授予多个权限
 * @param permissionList 权限列表
 * @param permissionGroup 权限组别 例:permission.Group
 * @param callBack 需要返回的回调
 */
fun getPermissionMultiple(
    context: Context,
    permissionList: MutableList<String>?,
    permissionGroup: Array<out String>? = null,
    callBack: PermissionCallback
) {
    val xxPermission = XXPermissions.with(context)

    //判断permissionList和pmGroupSuccess权限有没有全部开启 如果开启直接执行成功回调
    val pmListSuccess = if(permissionList.isNullOrEmpty()){
        false
    }else{
        XXPermissions.isGranted(context, permissionList)
    }
    val pmGroupSuccess = if(permissionGroup.isNullOrEmpty()){
        false
    }else{
        XXPermissions.isGranted(context, permissionGroup)
    }

    if (pmListSuccess && pmGroupSuccess) {
        callBack.build.onGrantedAllSuccess?.invoke()
    } else {
        if (!pmListSuccess) {
            permissionList?.forEach { pmText ->
                xxPermission.permission(pmText)
            }
        }
        if (!pmGroupSuccess) {
            permissionGroup?.let { pmGroup ->
                xxPermission.permission(pmGroup)
            }
        }

        xxPermission.request(object : OnPermissionCallback {
            override fun onGranted(permissions: MutableList<String>?, all: Boolean) {
                if (all) {
//                    ToastTool.showToast(ActivityMgr.getContext().getString(R.string.permission_text) + ActivityMgr.getContext().getString(R.string.grant_success_text), context, 1)
                    callBack.build.onGrantedAllSuccess?.invoke()
                } else {
                    ToastTool.showToast(ActivityMgr.getContext().getString(R.string.permission_text) + ActivityMgr.getContext().getString(R.string.grant_fail_text), context, 1)
                    callBack.build.onGrantedHaveError?.invoke()
                }
            }

            override fun onDenied(permissions: MutableList<String>?, never: Boolean) {
                if (never) {
                    ToastTool.showToast(ActivityMgr.getContext().getString(R.string.grant_denied_never_text), context, 1)
                    callBack.build.onDeniedNever?.invoke()
                } else {
                    ToastTool.showToast(ActivityMgr.getContext().getString(R.string.permission_text) + ActivityMgr.getContext().getString(R.string.grant_denied_text), context, 1)
                    callBack.build.onDenied?.invoke()
                }
            }
        })
    }
}

/**
 * 赋值权限回调对象
 */
fun setPermissionCallBacks(callBack: PermissionCallback.Builder.() -> Unit): PermissionCallback {
    return PermissionCallback().apply { setCallBack(callBack) }
}
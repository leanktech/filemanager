package com.hyperstudio.hyper.file.base_lib.tool

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/11/12 19:13
 */

@SuppressLint("SimpleDateFormat")
fun getTodayDateStr(): String {
    val sdf = SimpleDateFormat("yyMMdd")
    return sdf.format(Date())
}
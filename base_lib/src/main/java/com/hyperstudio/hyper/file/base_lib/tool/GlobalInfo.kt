package com.hyperstudio.hyper.file.base_lib.tool

/**
 * @Name: GlobalInfo
 * @Description: app常量
 * @Author: even@leanktech.com
 * @Date: 2021/10/13 16:51
 */
object GlobalInfo {

    /**
     * 创建workManager时候通过Type判断 如果有当前worker则不会再次创建
     */
    const val boostPushWorkName = "AppBoostPushWorkName"
    const val cleanPushWorkName = "AppCleanPushWorkName"
    const val batteryPushWorkName = "AppBatteryPushWorkName"
    const val coolerPushWorkName = "AppCoolerPushWorkName"
    const val securityPushWorkName = "AppSecurityPushWorkName"

    /**
     * 通知类型
     * SystemResident 常驻通知
     * PictureCard 卡片通知
     */
    const val NOTIFICATION_SYSTEM_RESIDENT = "SystemResident"
    const val NOTIFICATION_PICTURE_CARD = "PictureCard"
    const val NOTIFICATION_TARGETED_CARD = "TargetedCard"

    /**
     * 功能名
     */
    const val boostFunction = "Phone Boost"
    const val saverFunction = "Battery Saver"
    const val coolerFunction = "CPU Cooler"
    const val securityFunction = "Phone Security"
    const val cleanFunction = "Clean"
    const val imageFunction = "Image"
    const val videoFunction = "Video"
    const val musicFunction = "Music"
    const val textFunction = "Text"
    const val downloadFunction = "Download"
    const val moreFunction = "More Function"

    const val targetedTiktokFunction = "Tiktok"
    const val targetedWhatsAppFunction = "WhatsApp"
    const val targetedGooglePlayFunction = "GooglePlay"
    const val targetedYouTubeFunction = "YouTube"
    const val targetedLineFunction = "Line"

    const val tiktokPackageName = "com.zhiliaoapp.musically"
    const val whatsAppPackageName = "com.whatsapp"
    const val googlePlayPackageName = "com.android.vending"
    const val youTubePackageName = "com.google.android.youtube"
    const val linePackageName = "jp.naver.line.android"

    const val RC_INSTALL_APK = "rc_install_apk"

    /**
     * interstitial类型
     * 首次启动广告id
     */
    const val FIRST_START_ADMOB_COLD_BOOT_ID = "ca-app-pub-6515162185810818/4238395192"

    /**
     * interstitial类型
     * 首次扫描广告id
     */
    const val FIRST_START_CLEAN_COLD_BOOT_ID_h = "ca-app-pub-6515162185810818/9299150180"
    const val FIRST_START_CLEAN_COLD_BOOT_ID_b = "ca-app-pub-6515162185810818/4548403846"

    /**
     * interstitial类型
     * cold boot id(冷启动广告id)
     * 程序没在后台 启动需要show的广告id
     */
    const val COLD_BOOT_TAG = "coldBoot"
    const val ADMOB_COLD_BOOT_ID = "ca-app-pub-6515162185810818/4727112952"

    /**
     * app open类型
     * hot start id(热启动广告id)
     * 例如程序推到后台 再次启动 需要show的广告id
     */
    const val HOT_START_TAG = "hotStart"
    const val ADMOB_HOT_START_ID = "ca-app-pub-6515162185810818/8854672601"


    //int interstitial (x屏,即插屏,全屏广告)
    //nat native原生广告

    /**
     * clean id
     */
    //interstitial
    const val ADMOB_CLEAN_ID_INTERSTITIAL_h = "ca-app-pub-6515162185810818/2697631828"
    //interstitial
    const val ADMOB_CLEAN_ID_INTERSTITIAL_b = "ca-app-pub-6515162185810818/6819914782"
    //native
    const val ADMOB_CLEAN_ID_NATIVE_h = "ca-app-pub-6515162185810818/2122916753"
    //native
    const val ADMOB_CLEAN_ID_NATIVE_b = "ca-app-pub-6515162185810818/7833306068"

    /**
     * notification boost id
     */
    //interstitial
    const val ADMOB_NOTIFICATION_BOOST_ID_INTERSTITIAL_h = "ca-app-pub-6515162185810818/1031024767"
    //interstitial 备用
    const val ADMOB_NOTIFICATION_BOOST_ID_INTERSTITIAL_b = "ca-app-pub-6515162185810818/3339511372"
    //native
    const val ADMOB_NOTIFICATION_BOOST_ID_NATIVE_h = "ca-app-pub-6515162185810818/6273164038"
    //native 备用
    const val ADMOB_NOTIFICATION_BOOST_ID_NATIVE_b = "ca-app-pub-6515162185810818/3729661094"

    /**
     * boost id
     */
    //interstitial
    const val ADMOB_BOOST_ID_INTERSTITIAL_h = "ca-app-pub-6515162185810818/6992100058"
    //interstitial 备用
    const val ADMOB_BOOST_ID_INTERSTITIAL_b = "ca-app-pub-6515162185810818/2852749615"
    //native
    const val ADMOB_BOOST_ID_NATIVE_h = "ca-app-pub-6515162185810818/4192878026"
    //native 备用
    const val ADMOB_BOOST_ID_NATIVE_b = "ca-app-pub-6515162185810818/7925176537"

    /**
     * battery id
     */
    //interstitial
    const val ADMOB_BATTERY_ID_INTERSTITIAL_h = "ca-app-pub-6515162185810818/5876899594"
    //interstitial 备用
    const val ADMOB_BATTERY_ID_INTERSTITIAL_b = "ca-app-pub-6515162185810818/5138532992"
    //native
    const val ADMOB_BATTERY_ID_NATIVE_h = "ca-app-pub-6515162185810818/4751776065"
    //native 备用
    const val ADMOB_BATTERY_ID_NATIVE_b = "ca-app-pub-6515162185810818/6478209403"

    /**
     * cooler id
     */
    //interstitial
    const val ADMOB_COOLER_ID_INTERSTITIAL_h = "ca-app-pub-6515162185810818/5873286048"
    //interstitial 备用
    const val ADMOB_COOLER_ID_INTERSTITIAL_b = "ca-app-pub-6515162185810818/7203058164"
    //native
    const val ADMOB_COOLER_ID_NATIVE_h = "ca-app-pub-6515162185810818/6994796024"
    //native 备用
    const val ADMOB_COOLER_ID_NATIVE_b = "ca-app-pub-6515162185810818/2667860865"

    /**
     * security id
     */
    //interstitial
    const val ADMOB_SECURITY_ID_INTERSTITIAL_h = "ca-app-pub-6515162185810818/6611652646"
    //interstitial 备用
    const val ADMOB_SECURITY_ID_INTERSTITIAL_b = "ca-app-pub-6515162185810818/9855473360"
    //native
    const val ADMOB_SECURITY_ID_NATIVE_h = "ca-app-pub-6515162185810818/6289392472"
    //native 备用
    const val ADMOB_SECURITY_ID_NATIVE_b = "ca-app-pub-6515162185810818/8083839079"

    /**
     * targeted id
     */
    //interstitial
    const val ADMOB_TARGETED_ID_INTERSTITIAL_h = "ca-app-pub-6515162185810818/3819262258"
    //interstitial 备用
    const val ADMOB_TARGETED_ID_INTERSTITIAL_b = "ca-app-pub-6515162185810818/5663827354"
    //native
    const val ADMOB_TARGETED_ID_NATIVE_h = "ca-app-pub-6515162185810818/2382073417"
    //native
    const val ADMOB_TARGETED_ID_NATIVE_b = "ca-app-pub-6515162185810818/6547396126"

    /**
     * 首页banner id
     */
    const val BANNER_TODO_ID_BANNER = "ca-app-pub-6515162185810818/8724917615"

    /**
     * 手动关闭广告的回调tag
     */
    const val AD_DISMISSED = "onAdDismissed"

    /**
     * show广告 报错的回调tag
     */
    const val AD_FAILED_SHOW = "onAdFailedToShow"

    /**
     * show广告 报错的回调tag
     */
    const val AD_SHOWED_FULL = "onAdShowedFullScreenContent"

}
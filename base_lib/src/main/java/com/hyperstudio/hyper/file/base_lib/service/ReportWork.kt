package com.hyperstudio.hyper.file.base_lib.service

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.*

class ReportWork(context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        return@withContext try {
            Result.success()
        } catch (error: Throwable) {
            Result.failure()
        }
    }

}



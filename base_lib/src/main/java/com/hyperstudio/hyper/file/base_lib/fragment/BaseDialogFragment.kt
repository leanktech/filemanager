package com.hyperstudio.hyper.file.base_lib.fragment

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.hyperstudio.hyper.file.base_lib.R
import com.hyperstudio.hyper.file.base_lib.base.Presenter
import com.hyperstudio.hyper.file.base_lib.tool.ActivityMgr
import com.hyperstudio.hyper.file.base_lib.tool.AppTool.getScreenWidth


abstract class BaseDialogFragment<VB : ViewDataBinding> : DialogFragment(),
    Presenter {

    /**
     * ViewDataBinding
     */
    lateinit var bindingView: VB

    /**
     * 对话框宽
     */
    open var dialogWidth = try {
        getScreenWidth(context ?: ActivityMgr.currentActivity()!!)
    } catch (e: Exception) {
        0
    }

    /**
     * 对话框长
     */
    open var dialogHeight = ViewGroup.LayoutParams.WRAP_CONTENT

    /**
     * 浮层透明度
     */
    open var dimAmount = 0.0f

    /**
     * 对话框位置
     */
    open val gravity = Gravity.CENTER

    /**
     * 动画，默认为渐入动画
     */
    open var animStyle: Int = R.style.dialogAlphaAnim

    /**
     * 是否可取消
     */
    open var cancelAble = true

    /**
     * 点击外部是否可取消
     */
    open var outsideCancelAble = true

    var res = try {
        resources
    } catch (e: Exception) {
        (context ?: ActivityMgr.currentActivity() ?: ActivityMgr.getContext()).resources
    }

    /**
     * 获取布局文件
     * @return 布局文件
     */
    abstract fun getLayoutId(): Int

    /**
     * 初始化页面
     */
    abstract fun initView()

    /**
     * 点击事件
     * @param v 被点击的view
     */
    override fun onClick(v: View?) {}

    /**
     * 加载数据
     * @param isRefresh 是否第一次加载数据
     */
    override fun loadData(isRefresh: Boolean) {}

    /**
     * 退出
     */
    open fun exit() {
        dismiss()
    }

    override fun dismiss() {
        dismissAllowingStateLoss()
    }

    /**
     * 绑定DataBinding
     * @param inflater LayoutInflater
     * @param container 父view
     * @param savedInstanceState Bundle
     * @return 显示View
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        bindingView = DataBindingUtil.inflate(activity!!.layoutInflater, getLayoutId(), null, false)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//            dialog?.window!!.insetsController?.show(WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS)
//            dialog?.window!!.insetsController?.show(WindowInsetsController.APPEARANCE_LIGHT_NAVIGATION_BARS)
            dialog?.window!!.setDecorFitsSystemWindows(false)
            if (dialog != null && dialog?.window != null) {
                val insetsController = View(context).windowInsetsController
                if (insetsController != null) {
                    insetsController.show(WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS)
                    insetsController.show(WindowInsetsController.APPEARANCE_LIGHT_NAVIGATION_BARS)
                }
            }
        } else {
            dialog?.window!!.decorView.systemUiVisibility =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
                } else {
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                }
        }
        return bindingView.root
    }

    /**
     * 设置是否可取消以、动画以及显示位置
     * @param savedInstanceState Bundle
     * @return 对话框
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setCancelable(cancelAble)
        dialog.setCanceledOnTouchOutside(outsideCancelAble)
        val lp = dialog.window?.attributes
        lp?.gravity = gravity
        dialog.window?.attributes = lp
        dialog.window?.attributes?.windowAnimations = animStyle
        return dialog
    }

    override fun onStart() {
        super.onStart()
        val dm = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(dm)
        dialog!!.window!!.setLayout(dialogWidth, dialogHeight)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (dialogWidth == 0 || dialogHeight == 0) {
            dismiss()
            return
        }
        dialog?.window!!.attributes.dimAmount = dimAmount
        initView()
        loadData(true)
    }
}
package com.hyperstudio.hyper.file.base_lib.tool

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat

/**
 * @Name: NotificationUtil
 * @Description: 通知工具类
 * @Author: even@leanktech.com
 * @Date: 2021/10/12 18:33
 */

/**
 * 创建Boost重复推送拉活通知渠道
 */
fun initFunctionPushNotificationChannel(context: Context) {
    val notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager?
    //判断是否为8.0以上：Build.VERSION_CODES.O为26
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        //创建通知渠道ID
        val channelId = "functionPush"
        //创建通知渠道名称
        val channelName = "Hyper Service"
        //创建通知渠道重要性
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(channelId, channelName, importance)
        channel.setBypassDnd(true)
        channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
        notificationManager?.createNotificationChannel(channel)
    }
}

/**
 * 创建系统常驻拉活通知渠道
 */
fun initSystemResidentNotificationChannel() {
    val notificationManager = ActivityMgr.getContext().getSystemService(NOTIFICATION_SERVICE) as NotificationManager?
    //判断是否为8.0以上：Build.VERSION_CODES.O为26
   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        //创建通知渠道ID
        val channelId = "systemResident"
        //创建通知渠道名称
        val channelName = "Hyper System Resident"
        //创建通知渠道重要性
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelId, channelName, importance).apply {
            description = "systemResidentNotification"
            lockscreenVisibility = NotificationCompat.VISIBILITY_PRIVATE
            enableLights(false)
            enableVibration(false)
            setSound(null, null)
            lightColor = Color.RED
            setShowBadge(false)
        }
        notificationManager?.createNotificationChannel(channel)
    }
}

fun initKeepAliveNotificationChannel(){
    //判断是否为8.0以上：Build.VERSION_CODES.O为26
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val mNotificationManager = ActivityMgr.getContext().getSystemService(NOTIFICATION_SERVICE) as NotificationManager?
        //创建通知渠道ID
        val channelId = "serviceLive"
        //创建通知渠道名称
        val channelName = "Hyper Service Alive"
        //创建通知渠道重要性
        val importance = NotificationManager.IMPORTANCE_NONE
        val channel = NotificationChannel(channelId, channelName, importance)
        channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PRIVATE
        mNotificationManager?.createNotificationChannel(channel)
    }
}
package com.hyperstudio.hyper.file.base_lib.tool.param

/**
 * @Name: StaticParam
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/8/31 14:23
 */

/**获取文件相关*/
const val FILE_TYPE_FOLDER: String = "wFl2d"
const val FILE_INFO_NAME = "fName"
const val FILE_INFO_ISFOLDER = "fIsDir"
const val FILE_INFO_TYPE = "fFileType"
const val FILE_INFO_NUM_SONDIRS = "fSonDirs"
const val FILE_INFO_NUM_SONFILES = "fSonFiles"
const val FILE_INFO_PATH = "fPath"
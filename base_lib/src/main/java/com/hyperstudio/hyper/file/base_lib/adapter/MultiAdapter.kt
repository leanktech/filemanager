package com.hyperstudio.hyper.file.base_lib.adapter

import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.hyperstudio.hyper.file.base_lib.adapter.`interface`.ItemConvert
import com.hyperstudio.hyper.file.base_lib.base.ItemClickPresenter

/**
 * @Name: MultiAdapter
 * @Description: 多布局DataBinding二次封装
 * @Author: even@leanktech.com
 * @Date: 2021/8/25 13:57
 */
class MultiAdapter<T : MultiItemEntity>(list: ObservableList<T>, var present: Int, var itemId: Int) :
    BaseMultiItemQuickAdapter<T, BaseDataBindingHolder<ViewDataBinding>>(list) {

    //如果不需要刷新item 不需要实现
    var itemConvert: ItemConvert? = null

    var itemPresenter: ItemClickPresenter<T>? = null

    override fun onBindViewHolder(holder: BaseDataBindingHolder<ViewDataBinding>, position: Int) {
        val item = data[position]
        holder.dataBinding?.setVariable(itemId, item)
        holder.dataBinding?.setVariable(present, itemPresenter)
        holder.dataBinding?.executePendingBindings()
        itemConvert?.multiConvert(holder, item)
    }

    override fun convert(holder: BaseDataBindingHolder<ViewDataBinding>, item: T) {}

    /**
     * 初始化，添加ListChanged监听，处理通知事件
     */
    init {
        list.addOnListChangedCallback(object :
            ObservableList.OnListChangedCallback<ObservableList<T>>() {
            override fun onItemRangeMoved(
                sender: ObservableList<T>?,
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
                notifyItemMoved(fromPosition, toPosition)
            }

            override fun onItemRangeRemoved(
                sender: ObservableList<T>?,
                positionStart: Int,
                itemCount: Int
            ) {
                if (sender?.isNotEmpty() == true) {
                    notifyItemRangeRemoved(positionStart, itemCount)
                } else {
                    notifyDataSetChanged()
                }
            }

            override fun onItemRangeChanged(
                sender: ObservableList<T>?,
                positionStart: Int,
                itemCount: Int
            ) {
                notifyItemRangeChanged(positionStart, itemCount)
            }

            override fun onItemRangeInserted(
                sender: ObservableList<T>?,
                positionStart: Int,
                itemCount: Int
            ) {
                sender?.run {
                    notifyItemRangeInserted(positionStart, itemCount)
                }
            }

            override fun onChanged(sender: ObservableList<T>?) {
                notifyDataSetChanged()
            }

        })
    }

    fun addTypeToLayout(type: Int, layoutResId: Int){
        addItemType(type, layoutResId)
    }

}
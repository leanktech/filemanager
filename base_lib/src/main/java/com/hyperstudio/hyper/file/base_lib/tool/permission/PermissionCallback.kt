package com.hyperstudio.hyper.file.base_lib.tool.permission

/**
 * @Name: PermissionCallback
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/2 13:52
 */
class PermissionCallback {

    lateinit var build: Builder

    fun setCallBack(call: Builder.() -> Unit) {
        this.build = Builder().also(call)
    }

    inner class Builder {
        //授予全部权限成功
        internal var onGrantedAllSuccess: (() -> Unit)? = null

        //授予权限中有失败
        internal var onGrantedHaveError: (() -> Unit)? = null

        //被永久拒绝权限
        internal var onDeniedNever: ((() -> Unit))? = null

        //被否认权限
        internal var onDenied: ((() -> Unit))? = null

        fun setOnGrantedAllSuccess(callback: () -> Unit) {
            onGrantedAllSuccess = callback
        }

        fun setOnGrantedHaveError(callback: () -> Unit) {
            onGrantedHaveError = callback
        }

        fun setOnDeniedNever(callback: () -> Unit) {
            onDeniedNever = callback
        }

        fun setOnDenied(callback: () -> Unit) {
            onDenied = callback
        }
    }
}
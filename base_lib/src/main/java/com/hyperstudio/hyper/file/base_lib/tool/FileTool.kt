package com.hyperstudio.hyper.file.base_lib.tool

import android.content.Context
import java.io.BufferedReader
import java.io.InputStreamReader
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

/**
 * @Name: FileTool
 * @Description: 类作用描述
 * @Author: even@leanktech.com
 * @Date: 2021/9/26 18:53
 */

/**
 * 格式化文件长度
 * @param fileSize
 * @return String
 */
fun formatFileSizeToUnit(fileSize: Long): String {
    val df = DecimalFormat("#0.00") //表示小数点前至少一位,0也会显示,后保留两位
    //特别声明小数点分隔符为"."，不根据系统语言变化而变化
    val symbols = DecimalFormatSymbols()
    symbols.decimalSeparator = '.'
    df.decimalFormatSymbols = symbols
    var fileSizeString = ""
    fileSizeString = when {
        fileSize < 1024 -> {
            df.format(fileSize.toDouble()).toString() + "B"
        }
        fileSize < 1048576 -> {
            df.format(fileSize.toDouble() / 1024).toString() + "KB"
        }
        fileSize < 1073741824 -> {
            df.format(fileSize.toDouble() / 1048576).toString() + "MB"
        }
        else -> {
            df.format(fileSize.toDouble() / 1073741824).toString() + "G"
        }
    }
    return fileSizeString
}

/**
 * 获取json文件内容工具类
 * @param context Context
 * @param fileName String
 * @return String
 */
fun getAssetsFileText(context: Context, fileName:String):String{
    val strBuilder=StringBuilder()
    val assetManager=context.assets
    val bf = BufferedReader(InputStreamReader(assetManager.open(fileName)))
    bf.use { strBuilder.append(it.readLine())}
    bf.close()
    return strBuilder.toString()
}
package com.hyperstudio.hyper.file.base_lib.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.Cap
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import com.hyperstudio.hyper.file.base_lib.R


/**
 * @Name:
 * @Description: 类的作用
 * @Author: even@leanktech.com
 * @Date: 2021/11/26 16:00
 */
class CircleProgressBar(context: Context, attrs: AttributeSet) : View(context, attrs) {

    // 画圆环的画笔
    private var ringPaint = Paint()

    // 画背景得画笔
    private var backgroundPaint = Paint()

    // 圆环颜色
    private var ringColor = 0

    // 圆环背景颜色
    private var ringBgColor = 0

    // 半径
    private var radius = 0f

    // 圆环宽度
    private var strokeWidth = 0f

    // 总进度
    private var totalProgress = 100

    // 当前进度
    private var currentProgress = 0
    // 透明度   private int alpha = 25;

    init {
        initAttrs(context, attrs)
        initVariable()
    }

    private fun initAttrs(context: Context, attrs: AttributeSet) {
        val typeArray: TypedArray =
            context.theme.obtainStyledAttributes(attrs, R.styleable.CircleProgressbar, 0, 0)
        radius = typeArray.getDimension(R.styleable.CircleProgressbar_radius, 80f)
        totalProgress = typeArray.getInteger(R.styleable.CircleProgressbar_max, 100)
        strokeWidth = typeArray.getDimension(R.styleable.CircleProgressbar_progressStrokeWidth, 10f)
        ringColor = typeArray.getColor(R.styleable.CircleProgressbar_ringColor, 0xFF0000)
        ringBgColor = typeArray.getColor(R.styleable.CircleProgressbar_ringBgColor, 0xFF0000)
    }

    private fun initVariable() {
        ringPaint.isAntiAlias = true
        ringPaint.isDither = true
        ringPaint.color = ringColor
        ringPaint.style = Paint.Style.STROKE
        ringPaint.strokeCap = Cap.ROUND
        ringPaint.strokeWidth = strokeWidth

        backgroundPaint.isAntiAlias = true
        backgroundPaint.isDither = true
        backgroundPaint.color = ringBgColor
        backgroundPaint.style = Paint.Style.STROKE
        backgroundPaint.strokeCap = Cap.ROUND
        backgroundPaint.strokeWidth = strokeWidth

    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        val oval = RectF(
            width / 2 - radius,
            height / 2 - radius,
            width / 2 + radius,
            height / 2 + radius
        )
        canvas.drawOval(oval, backgroundPaint)
        if (currentProgress >= 0) {
            ringPaint.let {
                canvas.drawArc(oval, 0f, 0f, false, it)
                canvas.drawArc(
                    oval,
                    -90f,
                    currentProgress.toFloat() / totalProgress * 360,
                    false,
                    it
                )
            }
        }

    }

    fun setProgress(progress: Int) {
        currentProgress = progress
        postInvalidate()
    }
}
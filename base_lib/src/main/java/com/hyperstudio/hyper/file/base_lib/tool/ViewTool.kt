package com.hyperstudio.hyper.file.base_lib.tool

import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation

/**
 * @Name: ViewTool
 * @Description: View的动态调用扩展方法
 * @Author: even@leanktech.com
 * @Date: 2021/9/8 15:05
 */

/**
 * View渐隐渐现动画效果
 */
fun View?.setVisibilityAnimation(visible:Int, duration: Long){
    if( this == null || duration < 0 ){
        return
    }
    var animation: AlphaAnimation ?= null
    this.visibility = visible
    when(visible){
        View.VISIBLE -> {
            animation = AlphaAnimation(0.0f, 1.0f)
        }
        View.GONE, View.INVISIBLE -> {
            animation = AlphaAnimation(1.0f, 0.0f)
        }
    }
    animation?.duration = duration
    animation?.fillAfter = true
    this.startAnimation(animation)
}

fun View?.startFlick(startTime: Long = 1000){
    val alphaAnimation1 = AlphaAnimation(0.1f, 1.0f)
    alphaAnimation1.duration = startTime
    alphaAnimation1.repeatCount = Animation.INFINITE
    alphaAnimation1.repeatMode = Animation.RESTART
    this?.startAnimation(alphaAnimation1)
}

/**
 * 旋转动画(顺时针)
 * */
fun View?.rotateAnim(duration: Long = 600){
    val anim: Animation = RotateAnimation(
        0f,
        360f,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    anim.fillAfter = false // 设置保持动画最后的状态

    anim.duration = duration//600 // 设置动画时间

    anim.repeatCount = -1 //设置动画的循环次数

    //LinearInterpolator()匀速旋转，AccelerateInterpolator()每次旋转一圈后会停顿一下
    anim.interpolator = LinearInterpolator()//AccelerateInterpolator() // 设置插入器

    this?.startAnimation(anim)
}

/**
 * 旋转动画(逆时针)
 * */
fun View?.rotateReverseAnim(duration: Long = 600){
    val anim: Animation = RotateAnimation(
        0f,
        -360f,
        Animation.RELATIVE_TO_SELF,
        0.5f,
        Animation.RELATIVE_TO_SELF,
        0.5f
    )
    anim.fillAfter = false // 设置保持动画最后的状态

    anim.duration = duration//600 // 设置动画时间

    anim.repeatCount = -1 //设置动画的循环次数

    //LinearInterpolator()匀速旋转，AccelerateInterpolator()每次旋转一圈后会停顿一下
    anim.interpolator = LinearInterpolator()//AccelerateInterpolator() // 设置插入器

    this?.startAnimation(anim)
}

package com.hyperstudio.hyper.file.base_lib.adapter

import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.hyperstudio.hyper.file.base_lib.adapter.`interface`.ItemConvert
import com.hyperstudio.hyper.file.base_lib.base.ItemClickPresenter

class SimpleAdapter<T>(
    layoutResId: Int, val list: ObservableList<T>, var present: Int, var itemId: Int
) : BaseQuickAdapter<T, BaseDataBindingHolder<ViewDataBinding>>(layoutResId, list) {

    var itemConvert: ItemConvert? = null

    var itemPresenter: ItemClickPresenter<T>? = null

    override fun onBindViewHolder(holder: BaseDataBindingHolder<ViewDataBinding>, position: Int) {
        val item = data[position]
        holder.dataBinding?.setVariable(itemId, item)
        holder.dataBinding?.setVariable(present, itemPresenter)
        holder.dataBinding?.executePendingBindings()
        itemConvert?.simpleConvert(holder, item)
    }

    override fun convert(holder: BaseDataBindingHolder<ViewDataBinding>, item: T) {}

    /**
     * 初始化，添加ListChanged监听，处理通知事件
     */
    init {
        list.addOnListChangedCallback(object :
            ObservableList.OnListChangedCallback<ObservableList<T>>() {
            override fun onChanged(contributorViewModels: ObservableList<T>) {
                notifyDataSetChanged()
            }

            override fun onItemRangeChanged(
                contributorViewModels: ObservableList<T>,
                i: Int,
                i1: Int
            ) {
                notifyItemRangeChanged(i, i1)
            }

            override fun onItemRangeInserted(
                contributorViewModels: ObservableList<T>,
                i: Int,
                i1: Int
            ) {
                notifyItemRangeInserted(i, i1)
            }

            override fun onItemRangeMoved(
                contributorViewModels: ObservableList<T>,
                i: Int,
                i1: Int,
                i2: Int
            ) {
                notifyItemMoved(i, i1)
            }

            override fun onItemRangeRemoved(
                contributorViewModels: ObservableList<T>,
                i: Int,
                i1: Int
            ) {
                if (contributorViewModels.isEmpty()) {
                    notifyDataSetChanged()
                } else {
                    notifyItemRangeRemoved(i, i1)
                }
            }
        })
    }
}
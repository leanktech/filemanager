package com.hyperstudio.hyper.file.base_lib.bundlebean

import java.io.Serializable

/**
 * @Name:DangerAppBundleBean
 * @Description: 用于传给扫描完成的Bundle Serializable Bean
 * @Author: even@leanktech.com
 * @Date: 2021/10/25 17:21
 */
class DangerAppBundleBean: Serializable {
        var appPackageName: String = ""
        var appName: String = ""
        var appCategory: String = ""
        var appSummary: String = ""
}